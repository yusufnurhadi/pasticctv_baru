<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Atm extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('pdf');

        if (!$this->session->userdata('email_user')) {
            $this->session->set_flashdata('error', 'Anda harus login dahulu ');
            redirect();
            die();
        }
    }

    public function index($wilayah)
    {
        log_act($this->session->userdata('email_user'), 'Page ATM '.$wilayah, 0, 0, date('Y-m-d H:i:s'));
        //config pagination
        $config['base_url'] = base_url('atm/index/'.$wilayah.'/');
        $config['per_page'] = 10;
        $data['start'] = $this->uri->segment(4);

        //keyword
        if ($this->input->post('keyword')) {

            $keyword = $this->input->post('keyword');
            $change = $this->input->post('change');
            $this->session->set_userdata('key_atm', $keyword);
            $this->session->set_userdata('change_atm', $change);

            $config['total_rows'] = $this->m_atm->read_like([
                'atm_cabang.kd_wilayah' => $wilayah,
                $this->session->userdata('change_atm') => $this->session->userdata('key_atm'),
            ])->num_rows();
            $data['atm'] = $this->m_atm->read_like_pagination([
                'atm_cabang.kd_wilayah' => $wilayah,
                $this->session->userdata('change_atm') => $this->session->userdata('key_atm'),
            ], $config['per_page'], $data['start'])->result_array();

        } else {

            if ($this->session->userdata('key_atm')) {

                $config['total_rows'] = $this->m_atm->read_like([
                    'atm_cabang.kd_wilayah' => $wilayah,
                    $this->session->userdata('change_atm') => $this->session->userdata('key_atm'),
                ])->num_rows();
                $data['atm'] = $this->m_atm->read_like_pagination([
                    'atm_cabang.kd_wilayah' => $wilayah,
                    $this->session->userdata('change_atm') => $this->session->userdata('key_atm')
                ], $config['per_page'], $data['start'])->result_array();

            } else {

                $config['total_rows'] = $this->m_atm->read_like([
                    'atm_cabang.kd_wilayah' => $wilayah,
                ])->num_rows();
                $data['atm'] = $this->m_atm->read_like_pagination([
                    'atm_cabang.kd_wilayah' => $wilayah,
                ], $config['per_page'], $data['start'])->result_array();

            }

        }

        //inisialisasi
        $this->pagination->initialize($config);

        $data['total_rows'] = $config['total_rows'];
        $data['tittle_atm'] = "ATM ".$wilayah;
        $data['halaman'] = "atm";
        $this->load->view('index', $data);
    }

    public function refresh($wilayah)
    {
        log_act($this->session->userdata('email_user'), 'Refresh ATM '.$wilayah, 0, 0, date('Y-m-d H:i:s'));
        $this->session->unset_userdata('key_atm');
        redirect('atm/index/'.$wilayah);
    }

    public function hapus($id)
    {
        log_act($this->session->userdata('email_user'), 'Hapus ATM '.$id, 0, 0, date('Y-m-d H:i:s'));
        $this->m_atm->delete($id);
        $this->session->set_flashdata('success', 'Data berhasil di hapus');
        echo "<script>javascript:history.back();</script>";
    }

    public function tambah($wilayah)
    {
        log_act($this->session->userdata('email_user'), 'Tambah ATM', 0, 0, date('Y-m-d H:i:s'));
        //jalur validasi
        $this->form_validation->set_rules('id_atm', 'ID Atm', 'required|trim|is_unique[atm.id_atm]', [
            'is_unique' => 'ID sudah terdaftar',
        ]);
        $this->form_validation->set_rules('lokasi_atm', 'Lokasi Atm', 'required');
        $this->form_validation->set_rules('alamat_atm', 'Alamat Atm', 'required');
        $this->form_validation->set_rules('kd_atm_cabang', 'Kode Cabang Atm', 'required');
        $this->form_validation->set_rules('kd_atm_pengelola', 'Kode Pengelola Atm', 'required');
        
        //validasi
        if ($this->form_validation->run() == false) {
            //tidak valid
            $this->session->set_flashdata('error', form_error('id_atm').form_error('lokasi_atm').form_error('kd_atm_cabang').form_error('kd_atm_pengelola') );
            echo "<script>javascript:history.back();</script>";
        } else {
            //valid
            $id_atm     = $this->input->post('id_atm');
            $lokasi_atm     = $this->input->post('lokasi_atm');
            $alamat_atm     = $this->input->post('alamat_atm');
            $kd_atm_cabang     = $this->input->post('kd_atm_cabang');
            $kd_atm_pengelola     = $this->input->post('kd_atm_pengelola');
            $lat_atm     = $this->input->post('lat_atm');
            $lng_atm     = $this->input->post('lng_atm');
            $merk_dvr_atm     = $this->input->post('merk_dvr_atm');
            $merk_dome_atm     = $this->input->post('merk_dome_atm');
            $sn_dvr_atm     = $this->input->post('sn_dvr_atm');
            $sn_dome_atm     = $this->input->post('sn_dome_atm');
            $sn_hdd_atm     = $this->input->post('sn_hdd_atm');
            $date_problem_atm     = $this->input->post('date_problem_atm');
            //Array
            $dataATM = [
                'id_atm' => $id_atm,
                'lokasi_atm' => $lokasi_atm,
                'alamat_atm' => $alamat_atm,
                'kd_atm_cabang' => $kd_atm_cabang,
                'kd_atm_pengelola' => $kd_atm_pengelola,
                'lat_atm' => $lat_atm,
                'lng_atm' => $lng_atm,
                'merk_dvr_atm' => $merk_dvr_atm,
                'merk_dome_atm' => $merk_dome_atm,
                'sn_dvr_atm' => $sn_dvr_atm,
                'sn_dome_atm' => $sn_dome_atm,
                'sn_hdd_atm' => $sn_hdd_atm,
                'date_problem_atm' => $date_problem_atm,
                'created_atm' => date('Y-m-d H:i:s'),
            ];
            //Simpan di database lewat model
            $simpanATM = $this->m_atm->create($dataATM);
            //berhasil
            $this->session->set_flashdata('success', 'Data berhasil ditambah');
            redirect('atm/index/'$wilayah);
        }
    }

    public function ubah($id)
    {
        //jalur validasi
        $this->form_validation->set_rules('email', 'nama', 'required');

        //validasi
        if ($this->form_validation->run() == false) {
            //tidak valid
            $this->session->set_flashdata('error', form_error('email') . form_error('nama'));
            echo "<script>javascript:history.back();</script>";
            // redirect('c_wilayah');
        } else {
            //valid
            $email = $this->input->post('email');
            $nama = $this->input->post('nama');
            $cabang = $this->m_cabang->read_where(['id_cabang'=>$id])->result_array();

            //cek foto
            if ($_FILES['foto']['name'] == null) {
                //tidak ada foto
                $foto = $cabang[0]['foto_cabang'];
            } else {
                //ada foto
                //config upload
                $config['upload_path']          = 'img/profile/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 2000;
                $config['file_name']            = 'profile' . date('YmdHis');
                //load library
                $this->load->library('upload', $config);
                //kondisi error
                if (!$this->upload->do_upload('foto')) {
                    //jika error
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    echo "<script>javascript:history.back();</script>";
                    die();
                } else {
                    //jika tidak error
                    $foto = $this->upload->data('file_name');
                }
            }

            
            //Array Wilayah
            $dataCabang = [
                
                'nama_cabang' => $nama,
                'updated_cabang' => date('Y-m-d H:i:s'),
                'foto_cabang'=>$foto,
            ];
            //Simpan di database lewat model
            $simpanCabang = $this->m_cabang->update($id, $dataCabang);
            //berhasil
            $this->session->set_flashdata('success', 'Data berhasil diubah');
            redirect('cabang');
        }
    }


    public function cetak()
    {
        //Ambil data
        $cabang = $this->m_cabang->read()->result_array();
        //Halaman Landscape
        //Ukuran kertas A4
        $pdf = new FPDF('l', 'mm', 'A4');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', 'B', 16);
        // mencetak string 
        $pdf->Cell(280, 7, 'DATA CABANG', 0, 1, 'C');
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', 'B', 12);
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10, 7, '', 0, 1);
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', 'B', 10);
        // mencetak string 
        $pdf->Cell(10, 6, 'No', 1, 0, 'C');
        $pdf->Cell(50, 6, 'Email', 1, 0, 'C');
        $pdf->Cell(80, 6, 'Nama ATM Cabang', 1, 0, 'C');
        $pdf->Cell(60, 6, 'Nama Cabang', 1, 0, 'C');
        $pdf->Cell(40, 6, 'Created', 1, 0, 'C');
        $pdf->Cell(40, 6, 'Updated', 1, 1, 'C');
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', '', 10);
        //nomor
        $no = 1;
        //looping data
        foreach ($cabang as $key) :

            // mencetak string 
            $pdf->Cell(10, 6, $no++, 1, 0, 'C');
            $pdf->Cell(50, 6, $key['email_user'], 1, 0);
            $pdf->Cell(80, 6, $key['nama_atm_cabang'], 1, 0);
            $pdf->Cell(60, 6, $key['nama_cabang'], 1, 0);
            $pdf->Cell(40, 6, $key['created_cabang'], 1, 0, 'C');
            $pdf->Cell(40, 6, $key['updated_cabang'], 1, 1, 'C');

        endforeach;

        $pdf->Output();

    }

    public function unduh()
    {
         // Load plugin PHPExcel nya
         include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

         // Panggil class PHPExcel nya
         $excel = new PHPExcel();
 
         // Settingan awal fil excel
         $excel->getProperties()->setCreator('Pasti CCTV')
             ->setLastModifiedBy('Pasti CCTV')
             ->setTitle("Data Cabang")
             ->setSubject("Data Cabang")
             ->setDescription("Laporan Data Cabang")
             ->setKeywords("Data Cabang");
 
         // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
         $style_col = array(
             'font' => array('bold' => true), // Set font nya jadi bold
             'alignment' => array(
                 'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
                 'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
             ),
             'borders' => array(
                 'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
                 'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
                 'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
                 'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
             )
         );
 
         // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
         $style_row = array(
             'alignment' => array(
                 'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
             ),
             'borders' => array(
                 'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
                 'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
                 'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
                 'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
             )
         );
 
         // Set kolom A1
         $excel->setActiveSheetIndex(0)->setCellValue('A1', "DATA CABANG");
         // Set kolom B1
         $excel->setActiveSheetIndex(0)->setCellValue('A2', "PASTI CCTV");
         $excel->getActiveSheet()->mergeCells('A1:F1'); // Set Merge Cell pada kolom A1 sampai F1
         $excel->getActiveSheet()->mergeCells('A2:F2'); // Set Merge Cell pada kolom A1 sampai F1
         $excel->getActiveSheet()->getStyle('A1:A2')->getFont()->setBold(TRUE); // Set bold kolom A1
         $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
         $excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12); // Set font size 15 untuk kolom A1
         $excel->getActiveSheet()->getStyle('A1:A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
 
         // Buat header tabel nya pada baris ke 3
         $excel->setActiveSheetIndex(0)->setCellValue('A4', "No");
         $excel->setActiveSheetIndex(0)->setCellValue('B4', "Email");
         $excel->setActiveSheetIndex(0)->setCellValue('C4', "Nama ATM Cabang");
         $excel->setActiveSheetIndex(0)->setCellValue('D4', "Nama Cabang");
         $excel->setActiveSheetIndex(0)->setCellValue('E4', "Created");
         $excel->setActiveSheetIndex(0)->setCellValue('F4', "Updated");
 
         // Apply style header yang telah kita buat tadi ke masing-masing kolom header
         $excel->getActiveSheet()->getStyle('A4')->applyFromArray($style_col);
         $excel->getActiveSheet()->getStyle('B4')->applyFromArray($style_col);
         $excel->getActiveSheet()->getStyle('C4')->applyFromArray($style_col);
         $excel->getActiveSheet()->getStyle('D4')->applyFromArray($style_col);
         $excel->getActiveSheet()->getStyle('E4')->applyFromArray($style_col);
         $excel->getActiveSheet()->getStyle('F4')->applyFromArray($style_col);
 
         //ambil data antrian
         $data = $this->m_cabang->read()->result_array();
         $numrow = 5; // Set baris pertama untuk isi tabel adalah baris ke 4
         $no = 1; // Set nomor
         foreach ($data as $key) : // Lakukan looping pada variabel siswa
 
             $excel->setActiveSheetIndex(0)->setCellValue('A' . $numrow, $no++);
             $excel->setActiveSheetIndex(0)->setCellValue('B' . $numrow, $key['email_user']);
             $excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $key['nama_atm_cabang']);
             $excel->setActiveSheetIndex(0)->setCellValue('D' . $numrow, $key['nama_cabang']);
             $excel->setActiveSheetIndex(0)->setCellValue('E' . $numrow, $key['created_cabang']);
             $excel->setActiveSheetIndex(0)->setCellValue('F' . $numrow, $key['updated_cabang']);
 
             // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
             $excel->getActiveSheet()->getStyle('A' . $numrow)->applyFromArray($style_row);
             $excel->getActiveSheet()->getStyle('B' . $numrow)->applyFromArray($style_row);
             $excel->getActiveSheet()->getStyle('C' . $numrow)->applyFromArray($style_row);
             $excel->getActiveSheet()->getStyle('D' . $numrow)->applyFromArray($style_row);
             $excel->getActiveSheet()->getStyle('E' . $numrow)->applyFromArray($style_row);
             $excel->getActiveSheet()->getStyle('F' . $numrow)->applyFromArray($style_row);
 
             $numrow++; // Tambah 1 setiap kali looping
 
         endforeach;
 
         // Set width kolom
         $excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);  // Set width kolom A
         $excel->getActiveSheet()->getColumnDimension('B')->setWidth(30); // Set width kolom B
         $excel->getActiveSheet()->getColumnDimension('C')->setWidth(40); // Set width kolom B
         $excel->getActiveSheet()->getColumnDimension('D')->setWidth(20); // Set width kolom B
         $excel->getActiveSheet()->getColumnDimension('E')->setWidth(20); // Set width kolom B
         $excel->getActiveSheet()->getColumnDimension('F')->setWidth(20); // Set width kolom B
 
         // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
         $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
 
         // Set orientasi kertas jadi LANDSCAPE
         $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
 
         // Set judul file excel nya
         $excel->getActiveSheet(0)->setTitle("Export Data Cabang");
         $excel->setActiveSheetIndex(0);
 
         // Proses file excel
         header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
         header('Content-Disposition: attachment; filename="Export Data Cabang.xlsx"'); // Set nama file excel nya
         header('Cache-Control: max-age=0');
 
         $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
         $write->save('php://output');
    }
}
