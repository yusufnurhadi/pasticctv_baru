<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cabang extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('pdf');

        if (!$this->session->userdata('email_user')) {
            $this->session->set_flashdata('error', 'Anda harus login dahulu ');
            redirect();
            die();
        }
    }

    public function index()
    {
        log_act($this->session->userdata('email_user'), 'Page Cabang', 0, 0, date('Y-m-d H:i:s'));
        //config pagination
        $config['base_url'] = base_url('cabang/index/');
        $config['per_page'] = 10;
        $data['start'] = $this->uri->segment(3);

        //keyword
        if ($this->input->post('keyword')) {

            $keyword = $this->input->post('keyword');
            $this->session->set_userdata('key_cabang', $keyword);
        } else {
            // $config['total_rows'] = $this->m_cabang->read()->num_rows();
            // $data['cabang'] = $this->m_cabang->read_pagination($config['per_page'], $data['start'])->result_array();
        }

        $config['total_rows'] = $this->m_cabang->read_like([
            'nama_cabang' => $this->session->userdata('key_cabang'),
        ])->num_rows();
        $data['cabang'] = $this->m_cabang->read_like_pagination(array('nama_cabang' => $this->session->userdata('key_cabang')), $config['per_page'], $data['start'])->result_array();
        //inisialisasi
        $this->pagination->initialize($config);

        $data['total_rows'] = $config['total_rows'];
        $data['halaman'] = "cabang";
        $data['atm_cabang'] = $this->m_atm_cabang->read()->result_array();
        $this->load->view('index', $data);
    }

    public function refresh()
    {
        log_act($this->session->userdata('email_user'), 'Refresh Cabang', 0, 0, date('Y-m-d H:i:s'));
        $this->session->unset_userdata('key_cabang');
        redirect('cabang');
    }

    public function hapus($id)
    {
        log_act($this->session->userdata('email_user'), 'Hapus Cabang', 0, 0, date('Y-m-d H:i:s'));
        $cabang = $this->m_cabang->read_where(['id_cabang'=>$id])->row_array();
        $this->m_cabang->delete($cabang['email_user']);
        $this->session->set_flashdata('success', 'Data berhasil di hapus');
        echo "<script>javascript:history.back();</script>";
    }

    public function tambah()
    {
        log_act($this->session->userdata('email_user'), 'Tambah Cabang', 0, 0, date('Y-m-d H:i:s'));
        //jalur validasi
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('cabang', 'Cabang', 'required');
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        
        //cek foto
        if ($_FILES['foto']['name'] == null) {
            //tidak ada foto
            $foto = 'default.jpg';
        } else {
            //ada foto
            //config upload
            $config['upload_path']          = 'img/profile/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 2000;
            $config['file_name']            = 'profile' . date('YmdHis');
            //load library
            $this->load->library('upload', $config);
            //kondisi error
            if (!$this->upload->do_upload('foto')) {
                //jika error
                $this->session->set_flashdata('error', $this->upload->display_errors());
                echo "<script>javascript:history.back();</script>";
                die();
            } else {
                //jika tidak error
                $foto = $this->upload->data('file_name');
            }
        }
        
        //validasi
        if ($this->form_validation->run() == false) {
            //tidak valid
            $this->session->set_flashdata('error', form_error('email').form_error('cabang').form_error('nama') );
            echo "<script>javascript:history.back();</script>";
        } else {
            //valid
            $email     = $this->input->post('email');
            $cabang    = $this->input->post('cabang');
            $nama      = $this->input->post('nama');
            //Array
            $dataUser = [
                'email_user' => $email,
                'password_user' => PASSWORD_HASH(1234, PASSWORD_DEFAULT),
                'level_user' => '5',
                'is_active_user' => '1',
                'created_user' => date('Y-m-d H:i:s'),
            ];
            $data = [
                'email_user' => $email,
                'kd_atm_cabang' => $cabang,
                'nama_cabang' => $nama,
                'foto_cabang' => $foto,
                'notif_cabang' => 1,
                'created_cabang' => date('Y-m-d H:i:s'),
            ];
            //Simpan di database lewat model
            $simpanUser = $this->m_user->create($dataUser);
            $simpan = $this->m_cabang->create($data);
            //berhasil
            $this->session->set_flashdata('success', 'Data berhasil ditambah');
            redirect('cabang');
        }
    }

    public function ubah($id)
    {
        log_act($this->session->userdata('email_user'), 'Ubah Cabang', 0, 0, date('Y-m-d H:i:s'));
        //jalur validasi
        $this->form_validation->set_rules('email', 'nama', 'required');

        //validasi
        if ($this->form_validation->run() == false) {
            //tidak valid
            $this->session->set_flashdata('error', form_error('email') . form_error('nama'));
            echo "<script>javascript:history.back();</script>";
            // redirect('c_wilayah');
        } else {
            //valid
            $email = $this->input->post('email');
            $nama = $this->input->post('nama');
            $cabang = $this->m_cabang->read_where(['id_cabang'=>$id])->result_array();

            //cek foto
            if ($_FILES['foto']['name'] == null) {
                //tidak ada foto
                $foto = $cabang[0]['foto_cabang'];
            } else {
                //ada foto
                //config upload
                $config['upload_path']          = 'img/profile/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 2000;
                $config['file_name']            = 'profile' . date('YmdHis');
                //load library
                $this->load->library('upload', $config);
                //kondisi error
                if (!$this->upload->do_upload('foto')) {
                    //jika error
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    echo "<script>javascript:history.back();</script>";
                    die();
                } else {
                    //jika tidak error
                    $foto = $this->upload->data('file_name');
                }
            }

            
            //Array Wilayah
            $dataCabang = [
                
                'nama_cabang' => $nama,
                'updated_cabang' => date('Y-m-d H:i:s'),
                'foto_cabang'=>$foto,
            ];
            //Simpan di database lewat model
            $simpanCabang = $this->m_cabang->update($id, $dataCabang);
            //berhasil
            $this->session->set_flashdata('success', 'Data berhasil diubah');
            redirect('cabang');
        }
    }

    public function reset($id)
    {
        $cabang = $this->m_cabang->read_where(['id_cabang' => $id])->row_array();
        $ubah = $this->m_user->update($cabang['email_user'], [
            'password_user' => PASSWORD_HASH(1234, PASSWORD_DEFAULT),
        ]);
        //berhasil
        log_act($this->session->userdata('email_user'), 'Reset Cabang '.$cabang['nama_cabang'], 0, 0, date('Y-m-d H:i:s'));
        $this->session->set_flashdata('success', 'Data berhasil direset');
        redirect('cabang');
    }

    public function aktif($id)
    {
        $cabang = $this->m_cabang->read_where(['id_cabang' => $id])->row_array();
        $ubah = $this->m_user->update($cabang['email_user'], [
            'is_active_user' => 1,
        ]);
        //berhasil
        log_act($this->session->userdata('email_user'), 'Aktif Cabang '.$cabang['nama_cabang'], 0, 0, date('Y-m-d H:i:s'));
        $this->session->set_flashdata('success', 'Akun berhasil di aktifasi');
        redirect('cabang');
    }

    public function blokir($id)
    {
        $cabang = $this->m_cabang->read_where(['id_cabang' => $id])->row_array();
        $ubah = $this->m_user->update($cabang['email_user'], [
            'is_active_user' => 2,
        ]);
        //berhasil
        log_act($this->session->userdata('email_user'), 'Blokir Cabang '.$cabang['nama_cabang'], 0, 0, date('Y-m-d H:i:s'));
        $this->session->set_flashdata('success', 'Akun berhasil di blokir');
        redirect('cabang');
    }

    public function cetak()
    {
        log_act($this->session->userdata('email_user'), 'Cetak Cabang', 0, 0, date('Y-m-d H:i:s'));
        //Ambil data
        $cabang = $this->m_cabang->read()->result_array();
        //Halaman Landscape
        //Ukuran kertas A4
        $pdf = new FPDF('l', 'mm', 'A4');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', 'B', 16);
        // mencetak string 
        $pdf->Cell(280, 7, 'DATA CABANG', 0, 1, 'C');
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', 'B', 12);
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10, 7, '', 0, 1);
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', 'B', 10);
        // mencetak string 
        $pdf->Cell(10, 6, 'No', 1, 0, 'C');
        $pdf->Cell(50, 6, 'Email', 1, 0, 'C');
        $pdf->Cell(80, 6, 'Nama ATM Cabang', 1, 0, 'C');
        $pdf->Cell(60, 6, 'Nama Cabang', 1, 0, 'C');
        $pdf->Cell(40, 6, 'Created', 1, 0, 'C');
        $pdf->Cell(40, 6, 'Updated', 1, 1, 'C');
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', '', 10);
        //nomor
        $no = 1;
        //looping data
        foreach ($cabang as $key) :

            // mencetak string 
            $pdf->Cell(10, 6, $no++, 1, 0, 'C');
            $pdf->Cell(50, 6, $key['email_user'], 1, 0);
            $pdf->Cell(80, 6, $key['nama_atm_cabang'], 1, 0);
            $pdf->Cell(60, 6, $key['nama_cabang'], 1, 0);
            $pdf->Cell(40, 6, $key['created_cabang'], 1, 0, 'C');
            $pdf->Cell(40, 6, $key['updated_cabang'], 1, 1, 'C');

        endforeach;

        $pdf->Output();

    }

    public function unduh()
    {
        log_act($this->session->userdata('email_user'), 'Unduh Cabang', 0, 0, date('Y-m-d H:i:s'));
         // Load plugin PHPExcel nya
         include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

         // Panggil class PHPExcel nya
         $excel = new PHPExcel();
 
         // Settingan awal fil excel
         $excel->getProperties()->setCreator('Pasti CCTV')
             ->setLastModifiedBy('Pasti CCTV')
             ->setTitle("Data Cabang")
             ->setSubject("Data Cabang")
             ->setDescription("Laporan Data Cabang")
             ->setKeywords("Data Cabang");
 
         // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
         $style_col = array(
             'font' => array('bold' => true), // Set font nya jadi bold
             'alignment' => array(
                 'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
                 'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
             ),
             'borders' => array(
                 'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
                 'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
                 'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
                 'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
             )
         );
 
         // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
         $style_row = array(
             'alignment' => array(
                 'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
             ),
             'borders' => array(
                 'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
                 'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
                 'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
                 'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
             )
         );
 
         // Set kolom A1
         $excel->setActiveSheetIndex(0)->setCellValue('A1', "DATA CABANG");
         // Set kolom B1
         $excel->setActiveSheetIndex(0)->setCellValue('A2', "PASTI CCTV");
         $excel->getActiveSheet()->mergeCells('A1:F1'); // Set Merge Cell pada kolom A1 sampai F1
         $excel->getActiveSheet()->mergeCells('A2:F2'); // Set Merge Cell pada kolom A1 sampai F1
         $excel->getActiveSheet()->getStyle('A1:A2')->getFont()->setBold(TRUE); // Set bold kolom A1
         $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
         $excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12); // Set font size 15 untuk kolom A1
         $excel->getActiveSheet()->getStyle('A1:A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
 
         // Buat header tabel nya pada baris ke 3
         $excel->setActiveSheetIndex(0)->setCellValue('A4', "No");
         $excel->setActiveSheetIndex(0)->setCellValue('B4', "Email");
         $excel->setActiveSheetIndex(0)->setCellValue('C4', "Nama ATM Cabang");
         $excel->setActiveSheetIndex(0)->setCellValue('D4', "Nama Cabang");
         $excel->setActiveSheetIndex(0)->setCellValue('E4', "Created");
         $excel->setActiveSheetIndex(0)->setCellValue('F4', "Updated");
 
         // Apply style header yang telah kita buat tadi ke masing-masing kolom header
         $excel->getActiveSheet()->getStyle('A4')->applyFromArray($style_col);
         $excel->getActiveSheet()->getStyle('B4')->applyFromArray($style_col);
         $excel->getActiveSheet()->getStyle('C4')->applyFromArray($style_col);
         $excel->getActiveSheet()->getStyle('D4')->applyFromArray($style_col);
         $excel->getActiveSheet()->getStyle('E4')->applyFromArray($style_col);
         $excel->getActiveSheet()->getStyle('F4')->applyFromArray($style_col);
 
         //ambil data antrian
         $data = $this->m_cabang->read()->result_array();
         $numrow = 5; // Set baris pertama untuk isi tabel adalah baris ke 4
         $no = 1; // Set nomor
         foreach ($data as $key) : // Lakukan looping pada variabel siswa
 
             $excel->setActiveSheetIndex(0)->setCellValue('A' . $numrow, $no++);
             $excel->setActiveSheetIndex(0)->setCellValue('B' . $numrow, $key['email_user']);
             $excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $key['nama_atm_cabang']);
             $excel->setActiveSheetIndex(0)->setCellValue('D' . $numrow, $key['nama_cabang']);
             $excel->setActiveSheetIndex(0)->setCellValue('E' . $numrow, $key['created_cabang']);
             $excel->setActiveSheetIndex(0)->setCellValue('F' . $numrow, $key['updated_cabang']);
 
             // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
             $excel->getActiveSheet()->getStyle('A' . $numrow)->applyFromArray($style_row);
             $excel->getActiveSheet()->getStyle('B' . $numrow)->applyFromArray($style_row);
             $excel->getActiveSheet()->getStyle('C' . $numrow)->applyFromArray($style_row);
             $excel->getActiveSheet()->getStyle('D' . $numrow)->applyFromArray($style_row);
             $excel->getActiveSheet()->getStyle('E' . $numrow)->applyFromArray($style_row);
             $excel->getActiveSheet()->getStyle('F' . $numrow)->applyFromArray($style_row);
 
             $numrow++; // Tambah 1 setiap kali looping
 
         endforeach;
 
         // Set width kolom
         $excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);  // Set width kolom A
         $excel->getActiveSheet()->getColumnDimension('B')->setWidth(30); // Set width kolom B
         $excel->getActiveSheet()->getColumnDimension('C')->setWidth(40); // Set width kolom B
         $excel->getActiveSheet()->getColumnDimension('D')->setWidth(20); // Set width kolom B
         $excel->getActiveSheet()->getColumnDimension('E')->setWidth(20); // Set width kolom B
         $excel->getActiveSheet()->getColumnDimension('F')->setWidth(20); // Set width kolom B
 
         // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
         $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
 
         // Set orientasi kertas jadi LANDSCAPE
         $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
 
         // Set judul file excel nya
         $excel->getActiveSheet(0)->setTitle("Export Data Cabang");
         $excel->setActiveSheetIndex(0);
 
         // Proses file excel
         header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
         header('Content-Disposition: attachment; filename="Export Data Cabang.xlsx"'); // Set nama file excel nya
         header('Cache-Control: max-age=0');
 
         $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
         $write->save('php://output');
    }
}
