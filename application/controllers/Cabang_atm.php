<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cabang_atm extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('pdf');

        if (!$this->session->userdata('email_user')) {
            $this->session->set_flashdata('error', 'Anda harus login dahulu ');
            redirect();
            die();
        }
    }

    public function index()
    {
        log_act($this->session->userdata('email_user'), 'Page Cabang ATM', 0, 0, date('Y-m-d H:i:s'));
        //config pagination
        $config['base_url'] = base_url('cabang_atm/index/');
        $config['per_page'] = 10;
        $data['wilayah'] = $this->m_wilayah->read()->result_array();
        $data['start'] = $this->uri->segment(3);

        //keyword
        if ($this->input->post('keyword')) {

            $keyword = $this->input->post('keyword');
            $this->session->set_userdata('key_cabang_atm', $keyword);
        } else {
            // $config['total_rows'] = $this->m_atm_cabang->read()->num_rows();
            // $data['cabang_atm'] = $this->m_atm_cabang->read_pagination($config['per_page'], $data['start'])->result_array();
        }

        $config['total_rows'] = $this->m_atm_cabang->read_like([
            'nama_atm_cabang' => $this->session->userdata('key_atm_cabang'),
        ])->num_rows();
        $data['cabang_atm'] = $this->m_atm_cabang->read_like_pagination(array('nama_atm_cabang' => $this->session->userdata('key_cabang_atm')), $config['per_page'], $data['start'])->result_array();
        //inisialisasi
        $this->pagination->initialize($config);

        $data['total_rows'] = $config['total_rows'];
        $data['halaman'] = "cabang_atm";
        $this->load->view('index', $data);
    }

    public function refresh()
    {
        log_act($this->session->userdata('email_user'), 'Refresh Cabang ATM', 0, 0, date('Y-m-d H:i:s'));
        $this->session->unset_userdata('key_cabang_atm');
        redirect('cabang_atm');
    }

    public function hapus($id)
    {
        log_act($this->session->userdata('email_user'), 'Hapus Cabang ATM', 0, 0, date('Y-m-d H:i:s'));
        $this->m_atm_cabang->delete($id);
        $this->session->set_flashdata('success', 'Data berhasil di hapus');
        echo "<script>javascript:history.back();</script>";

    }

    public function tambah()
    {
        log_act($this->session->userdata('email_user'), 'Tambah Cabang ATM', 0, 0, date('Y-m-d H:i:s'));
        //jalur validasi
        $this->form_validation->set_rules('kode', 'Kode', 'required');
        $this->form_validation->set_rules('wilayah', 'Wilayah', 'required');
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        
        
        //validasi
        if ($this->form_validation->run() == false) {
            //tidak valid
            $this->session->set_flashdata('error', form_error('kode').form_error('wilayah').form_error('nama') );
            echo "<script>javascript:history.back();</script>";
        } else {
            //valid
            $kode = $this->input->post('kode');
            $wilayah = $this->input->post('wilayah');
            $nama = $this->input->post('nama');
            
            //Array
            $data = [
                'kd_atm_cabang' => $kode,
                'kd_wilayah' => $wilayah,
                'nama_atm_cabang' => $nama,
                'created_atm_cabang' => date('Y-m-d H:i:s'),
            ];
            
            //Simpan di database lewat model
            $simpan = $this->m_atm_cabang->create($data);
            //berhasil
            $this->session->set_flashdata('success', 'Data berhasil ditambah');
            redirect('cabang_atm');
        }
    }

    public function ubah($id)
    {
        log_act($this->session->userdata('email_user'), 'Ubah Cabang ATM', 0, 0, date('Y-m-d H:i:s'));
        //jalur validasi
        $this->form_validation->set_rules('kode', 'Kode', 'required');
        $this->form_validation->set_rules('wilayah', 'Wilayah', 'required');
        $this->form_validation->set_rules('nama', 'Nama', 'required');

        //validasi
        if ($this->form_validation->run() == false) {
            //tidak valid
            $this->session->set_flashdata('error', form_error('kode').form_error('wilayah').form_error('nama') );
            echo "<script>javascript:history.back();</script>";
            // redirect('c_wilayah');
        } else {
            //valid
            $kode = $this->input->post('kode');
            $wilayah = $this->input->post('wilayah');
            $nama = $this->input->post('nama');
            
            //Array Cabang ATM
            $datacabang_atm = [
            'kd_atm_cabang' => $kode,
            'kd_wilayah' => $wilayah,
            'nama_atm_cabang' => $nama,
            'created_atm_cabang' => date('Y-m-d H:i:s'),
            ];
            //Simpan di database lewat model
            $simpancabang_atm = $this->m_atm_cabang->update($id, $datacabang_atm);
            //berhasil
            $this->session->set_flashdata('success', 'Data berhasil diubah');
            redirect('cabang_atm');
        }
        
    }

    public function cetak()
    {
        log_act($this->session->userdata('email_user'), 'Cetak Cabang ATM', 0, 0, date('Y-m-d H:i:s'));
         //Ambil data
         $cabang_atm = $this->m_atm_cabang->read()->result_array();
         //Halaman Landscape
         //Ukuran kertas A4
         $pdf = new FPDF('l', 'mm', 'A4');
         // membuat halaman baru
         $pdf->AddPage();
         // setting jenis font yang akan digunakan
         $pdf->SetFont('Arial', 'B', 16);
         // mencetak string 
         $pdf->Cell(280, 7, 'DATA CABANG ATM', 0, 1, 'C');
         // setting jenis font yang akan digunakan
         $pdf->SetFont('Arial', 'B', 12);
         // Memberikan space kebawah agar tidak terlalu rapat
         $pdf->Cell(10, 7, '', 0, 1);
         // setting jenis font yang akan digunakan
         $pdf->SetFont('Arial', 'B', 10);
         // mencetak string 
         $pdf->Cell(10, 6, 'No', 1, 0, 'C');
         $pdf->Cell(50, 6, 'Kode ATM Cabang', 1, 0, 'C');
         $pdf->Cell(70, 6, 'Wilayah', 1, 0, 'C');
         $pdf->Cell(70, 6, 'Nama ATM Cabang', 1, 0, 'C');
         $pdf->Cell(40, 6, 'Created', 1, 0, 'C');
         $pdf->Cell(40, 6, 'Updated', 1, 1, 'C');
         // setting jenis font yang akan digunakan
         $pdf->SetFont('Arial', '', 10);
         //nomor
         $no = 1;
         //looping data
         foreach ($cabang_atm as $key) :
 
             // mencetak string 
             $pdf->Cell(10, 6, $no++, 1, 0, 'C');
             $pdf->Cell(50, 6, $key['kd_atm_cabang'], 1, 0);
             $pdf->Cell(70, 6, $key['nama_wilayah'], 1, 0);
             $pdf->Cell(70, 6, $key['nama_atm_cabang'], 1, 0);
             $pdf->Cell(40, 6, $key['created_atm_cabang'], 1, 0, 'C');
             $pdf->Cell(40, 6, $key['updated_atm_cabang'], 1, 1, 'C');
 
         endforeach;
 
         $pdf->Output();
 
        
    }
    
    public function unduh()
    {
        log_act($this->session->userdata('email_user'), 'Unduh Cabang ATM', 0, 0, date('Y-m-d H:i:s'));
         // Load plugin PHPExcel nya
         include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

         // Panggil class PHPExcel nya
         $excel = new PHPExcel();
 
         // Settingan awal fil excel
         $excel->getProperties()->setCreator('Pasti CCTV')
             ->setLastModifiedBy('Pasti CCTV')
             ->setTitle("Data Cabang ATM")
             ->setSubject("Data Cabang ATM")
             ->setDescription("Laporan Data Cabang ATM")
             ->setKeywords("Data Cabang ATM");
 
         // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
         $style_col = array(
             'font' => array('bold' => true), // Set font nya jadi bold
             'alignment' => array(
                 'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
                 'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
             ),
             'borders' => array(
                 'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
                 'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
                 'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
                 'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
             )
         );
 
         // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
         $style_row = array(
             'alignment' => array(
                 'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
             ),
             'borders' => array(
                 'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
                 'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
                 'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
                 'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
             )
         );
 
         // Set kolom A1
         $excel->setActiveSheetIndex(0)->setCellValue('A1', "DATA CABANG ATM");
         // Set kolom B1
         $excel->setActiveSheetIndex(0)->setCellValue('A2', "PASTI CCTV");
         $excel->getActiveSheet()->mergeCells('A1:E1'); // Set Merge Cell pada kolom A1 sampai F1
         $excel->getActiveSheet()->mergeCells('A2:E2'); // Set Merge Cell pada kolom A1 sampai F1
         $excel->getActiveSheet()->getStyle('A1:A2')->getFont()->setBold(TRUE); // Set bold kolom A1
         $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
         $excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12); // Set font size 15 untuk kolom A1
         $excel->getActiveSheet()->getStyle('A1:A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
 
         // Buat header tabel nya pada baris ke 3
         $excel->setActiveSheetIndex(0)->setCellValue('A4', "No");
         $excel->setActiveSheetIndex(0)->setCellValue('B4', "Kode ATM Cabang");
         $excel->setActiveSheetIndex(0)->setCellValue('C4', "Wilayah");
         $excel->setActiveSheetIndex(0)->setCellValue('D4', "Nama Cabang ATM");
         $excel->setActiveSheetIndex(0)->setCellValue('E4', "Created");
         $excel->setActiveSheetIndex(0)->setCellValue('F4', "Updated");
 
         // Apply style header yang telah kita buat tadi ke masing-masing kolom header
         $excel->getActiveSheet()->getStyle('A4')->applyFromArray($style_col);
         $excel->getActiveSheet()->getStyle('B4')->applyFromArray($style_col);
         $excel->getActiveSheet()->getStyle('C4')->applyFromArray($style_col);
         $excel->getActiveSheet()->getStyle('D4')->applyFromArray($style_col);
         $excel->getActiveSheet()->getStyle('E4')->applyFromArray($style_col);
         $excel->getActiveSheet()->getStyle('F4')->applyFromArray($style_col);
         
 
         //ambil data antrian
         $data = $this->m_atm_cabang->read()->result_array();
         $numrow = 5; // Set baris pertama untuk isi tabel adalah baris ke 4
         $no = 1; // Set nomor
         foreach ($data as $key) : // Lakukan looping pada variabel siswa
 
            $excel->setActiveSheetIndex(0)->setCellValue('A' . $numrow, $no++);
            $excel->setActiveSheetIndex(0)->setCellValue('B' . $numrow, $key['kd_atm_cabang']);
            $excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $key['nama_wilayah']);
            $excel->setActiveSheetIndex(0)->setCellValue('D' . $numrow, $key['nama_atm_cabang']);
            $excel->setActiveSheetIndex(0)->setCellValue('E' . $numrow, $key['created_atm_cabang']);
            $excel->setActiveSheetIndex(0)->setCellValue('F' . $numrow, $key['updated_atm_cabang']);
 
             // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
             $excel->getActiveSheet()->getStyle('A' . $numrow)->applyFromArray($style_row);
             $excel->getActiveSheet()->getStyle('B' . $numrow)->applyFromArray($style_row);
             $excel->getActiveSheet()->getStyle('C' . $numrow)->applyFromArray($style_row);
             $excel->getActiveSheet()->getStyle('D' . $numrow)->applyFromArray($style_row);
             $excel->getActiveSheet()->getStyle('E' . $numrow)->applyFromArray($style_row);
             $excel->getActiveSheet()->getStyle('F' . $numrow)->applyFromArray($style_row);
              
             $numrow++; // Tambah 1 setiap kali looping
 
         endforeach;
 
         // Set width kolom
         $excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);  // Set width kolom A
         $excel->getActiveSheet()->getColumnDimension('B')->setWidth(20); // Set width kolom B
         $excel->getActiveSheet()->getColumnDimension('C')->setWidth(30); // Set width kolom B
         $excel->getActiveSheet()->getColumnDimension('D')->setWidth(40); // Set width kolom B
         $excel->getActiveSheet()->getColumnDimension('E')->setWidth(20); // Set width kolom B
         $excel->getActiveSheet()->getColumnDimension('E')->setWidth(20); // Set width kolom B
         $excel->getActiveSheet()->getColumnDimension('F')->setWidth(20); // Set width kolom B
          
         // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
         $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
 
         // Set orientasi kertas jadi LANDSCAPE
         $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
 
         // Set judul file excel nya
         $excel->getActiveSheet(0)->setTitle("Export Data Cabang ATM");
         $excel->setActiveSheetIndex(0);
 
         // Proses file excel
         header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
         header('Content-Disposition: attachment; filename="Export Data Cabang ATM.xlsx"'); // Set nama file excel nya
         header('Cache-Control: max-age=0');
 
         $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
         $write->save('php://output');
        
    }
}
