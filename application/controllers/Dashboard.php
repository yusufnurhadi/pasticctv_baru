<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        if (!$this->session->userdata('email_user')) {
            $this->session->set_flashdata('error', 'Anda harus login dahulu ');
            redirect();
            die();
        }
    }

    public function index()
    {
        if ($this->session->userdata('level_user')) {

            if ($this->session->userdata('level_user') == 1) {
                log_act($this->session->userdata('nama_admin'), 'Page Dashboard', 0, 0, date('Y-m-d H:i:s'));
                $this->_admin();
            } elseif ($this->session->userdata('level_user') == 2) {
                log_act($this->session->userdata('nama_koordinator'), 'Page Dashboard', 0, 0, date('Y-m-d H:i:s'));
                $this->_koordinator();
            } elseif ($this->session->userdata('level_user') == 3) {
                log_act($this->session->userdata('nama_client'), 'Page Dashboard', 0, 0, date('Y-m-d H:i:s'));
                $this->_client();
            } elseif ($this->session->userdata('level_user') == 4) {
                log_act($this->session->userdata('nama_manager_area'), 'Page Dashboard', 0, 0, date('Y-m-d H:i:s'));
                $this->_manager_area();
            } elseif ($this->session->userdata('level_user') == 5) {
                log_act($this->session->userdata('nama_cabang'), 'Page Dashboard', 0, 0, date('Y-m-d H:i:s'));
                $this->_cabang();
            } elseif ($this->session->userdata('level_user') == 6) {
                log_act($this->session->userdata('nama_pengelola'), 'Page Dashboard', 0, 0, date('Y-m-d H:i:s'));
                $this->_pengelola();
            } elseif ($this->session->userdata('level_user') == 7) {
                log_act($this->session->userdata('nama_teknisi'), 'Page Dashboard', 0, 0, date('Y-m-d H:i:s'));
                $this->_teknisi();
            } else {
                log_act('unknow', 'Failed : Page Dashboard', 0, 0, date('Y-m-d H:i:s'));
                $this->session->set_flashdata('error', 'Anda harus login dahulu ');
                redirect();
                die();
            }

        } else {

            $this->session->set_flashdata('error', 'Anda harus login dahulu ');
            redirect();
            die();

        }

    }

    public function _admin()
    {
        $data['count_atm_wilayah']=[];
        $wilayah = $this->m_wilayah->read()->result_array();
        foreach ($wilayah as $key) {

            $data['count_atm_wilayah'][$key['kd_wilayah']]=$this->m_atm->read_where(['atm_cabang.kd_wilayah' => $key['kd_wilayah']])->num_rows();

        }
        $data['halaman']="dashboard";
        $data['count_wilayah']=count($wilayah);
        $data['count_cabang']=$this->m_atm_cabang->read()->num_rows();
        $data['count_pengelola']=$this->m_atm_pengelola->read()->num_rows();
        $data['count_atm']=$this->m_atm->read()->num_rows();
        $this->load->view('index', $data);
    }

    public function _pengelola()
    {
        $this->load->view('index');
    }

    public function _cabang()
    {
        $this->load->view('index');
    }

    public function _client()
    {
        $this->load->view('index');
    }

    public function _manager_area()
    {
        $this->load->view('index');
    }

    public function _koordinator()
    {
        $this->load->view('index');
    }

    public function _teknisi()
    {
        $this->load->view('index');
    }
    
}
