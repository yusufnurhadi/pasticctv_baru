<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Koordinator extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('pdf');

        if (!$this->session->userdata('email_user')) {
            $this->session->set_flashdata('error', 'Anda harus login dahulu ');
            redirect();
            die();
        }
    }

    public function index()
    {
        log_act($this->session->userdata('email_user'), 'Page Koordinator Wilayah', 0, 0, date('Y-m-d H:i:s'));
        //config pagination
        $config['base_url'] = base_url('koordinator/index/');
        $config['per_page'] = 10;
        $data['start'] = $this->uri->segment(3);

        //keyword
        if ($this->input->post('keyword')) {

            $keyword = $this->input->post('keyword');
            $change = $this->input->post('change');
            $this->session->set_userdata('key_koordinator', $keyword);
            $this->session->set_userdata('change_koordinator', $change);

            if ($this->session->userdata('change_koordinator')=="koordinator_wilayah.kd_wilayah") {

                $config['total_rows'] = $this->m_koordinator_wilayah->read_like_group([
                    $this->session->userdata('change_koordinator') => $this->session->userdata('key_koordinator'),
                ])->num_rows();
                $data['koordinator'] = $this->m_koordinator_wilayah->read_like_pagination_group([
                    $this->session->userdata('change_koordinator') => $this->session->userdata('key_koordinator'),
                ], $config['per_page'], $data['start'])->result_array();

            } else {

                $config['total_rows'] = $this->m_koordinator->read_like([
                    $this->session->userdata('change_koordinator') => $this->session->userdata('key_koordinator'),
                ])->num_rows();
                $data['koordinator'] = $this->m_koordinator->read_like_pagination([
                    $this->session->userdata('change_koordinator') => $this->session->userdata('key_koordinator'),
                ], $config['per_page'], $data['start'])->result_array();

            }

        } else {

            if ($this->session->userdata('key_koordinator')) {

                if ($this->session->userdata('change_koordinator')=="koordinator_wilayah.kd_wilayah") {

                    $config['total_rows'] = $this->m_koordinator_wilayah->read_like_group([
                        $this->session->userdata('change_koordinator') => $this->session->userdata('key_koordinator'),
                    ])->num_rows();
                    $data['koordinator'] = $this->m_koordinator_wilayah->read_like_pagination_group([
                        $this->session->userdata('change_koordinator') => $this->session->userdata('key_koordinator'),
                    ], $config['per_page'], $data['start'])->result_array();
    
                } else {
    
                    $config['total_rows'] = $this->m_koordinator->read_like([
                        $this->session->userdata('change_koordinator') => $this->session->userdata('key_koordinator'),
                    ])->num_rows();
                    $data['koordinator'] = $this->m_koordinator->read_like_pagination([
                        $this->session->userdata('change_koordinator') => $this->session->userdata('key_koordinator'),
                    ], $config['per_page'], $data['start'])->result_array();
    
                }

            } else {

                $config['total_rows'] = $this->m_koordinator->read()->num_rows();
                $data['koordinator'] = $this->m_koordinator->read_pagination($config['per_page'], $data['start'])->result_array();

            }

        }

        $a = 0;
        foreach ($data['koordinator'] as $koordinator) :

            $wilayah = $this->m_koordinator_wilayah->read_where([
                'koordinator_wilayah.id_koordinator' => $koordinator['id_koordinator'],
                ])->result_array();
            // $wilayah_baru = "";

            // foreach ($wilayah as $wil) {

            //     $wilayah_baru.=$wil['kd_wilayah']." ";
                
            // }

            // $data['koordinator'][$a]['wilayah']=$wilayah_baru;
            $data['koordinator'][$a]['wilayah']=$wilayah;
            $a++;
        
        endforeach;

        //inisialisasi
        $this->pagination->initialize($config);
        $data['total_rows'] = $config['total_rows'];
        $data['halaman'] = "koordinator";
        $this->load->view('index', $data);
    }

    public function refresh()
    {
        log_act($this->session->userdata('email_user'), 'Refresh Koordinator Wilayah', 0, 0, date('Y-m-d H:i:s'));
        $this->session->unset_userdata('key_koordinator');
        redirect('koordinator');
    }

    public function hapus($id)
    {
        $this->m_koordinator->delete($id);
        $this->session->set_flashdata('success', 'Data berhasil di hapus');
        log_act($this->session->userdata('email_user'), 'Hapus Koordinator Wilayah '.$id, 0, 0, date('Y-m-d H:i:s'));
        echo "<script>javascript:history.back();</script>";
    }

    public function tambah()
    {
        //jalur validasi
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('wilayah[]', 'Wilayah', 'required');
        
        //validasi
        if ($this->form_validation->run() == false) {
            //tidak valid
            $this->session->set_flashdata('error', form_error('nama').form_error('email').form_error('wilayah[]') );
            echo "<script>javascript:history.back();</script>";
        } else {
            //valid
            $nama = $this->input->post('nama');
            $email = $this->input->post('email');
            $wilayah = $this->input->post('wilayah');
            //cek foto
            if ($_FILES['foto']['name'] == null) {
                //tidak ada foto
                $foto = 'default.jpg';
            } else {
                //ada foto
                //config upload
                $config['upload_path']          = 'img/profile/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 2000;
                $config['file_name']            = 'profile' . date('YmdHis');
                //load library
                $this->load->library('upload', $config);
                //kondisi error
                if (!$this->upload->do_upload('foto')) {
                    //jika error
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    echo "<script>javascript:history.back();</script>";
                    die();
                } else {
                    //jika tidak error
                    $foto = $this->upload->data('file_name');
                }
            }
            //Array
            $dataUser = [
                'email_user' => $email,
                'password_user' => PASSWORD_HASH(1234, PASSWORD_DEFAULT),
                'level_user' => '2',
                'is_active_user' => '1',
                'created_user' => date('Y-m-d H:i:s'),
            ];
            $data = [
                'email_user' => $email,
                'nama_koordinator' => $nama,
                'foto_koordinator' => $foto,
                'notif_koordinator' => 1,
                'created_koordinator' => date('Y-m-d H:i:s'),
            ];
            //Simpan di database lewat model
            $simpanUser = $this->m_user->create($dataUser);
            $simpan = $this->m_koordinator->create($data);
            $id = $this->db->insert_id();

            foreach($wilayah as $wil) :

                $simpan = $this->m_koordinator_wilayah->create([
                    'id_koordinator' => $id,
                    'kd_wilayah' => $wil,
                    'created_koordinator_wilayah' => date('Y-m-d H:i:s'),
                ]);

            endforeach;

            //berhasil
            log_act($this->session->userdata('email_user'), 'Tambah Koordinator Wilayah '.$nama, 0, 0, date('Y-m-d H:i:s'));
            $this->session->set_flashdata('success', 'Data berhasil ditambah');
            redirect('koordinator');
        }
    }

    public function ubah($id)
    {
        //jalur validasi
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('wilayah[]', 'Wilayah', 'required');
        
        //validasi
        if ($this->form_validation->run() == false) {
            //tidak valid
            $this->session->set_flashdata('error', form_error('nama').form_error('wilayah[]') );
            echo "<script>javascript:history.back();</script>";
        } else {
            //valid
            $nama = $this->input->post('nama');
            $wilayah = $this->input->post('wilayah');
            $koordinator = $this->m_koordinator->read_where(['koordinator.id_koordinator'=>$id])->result_array();

            //cek foto
            if ($_FILES['foto']['name'] == null) {
                //tidak ada foto
                $foto = $koordinator[0]['foto_koordinator'];
            } else {
                //ada foto
                //config upload
                $config['upload_path']          = 'img/profile/';
                $config['allowed_types']        = 'gif|jpg|png|jpeg';
                $config['max_size']             = 2000;
                $config['file_name']            = 'profile-koordinator-' . date('YmdHis');
                //load library
                $this->load->library('upload', $config);
                //kondisi error
                if (!$this->upload->do_upload('foto')) {
                    //jika error
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    echo "<script>javascript:history.back();</script>";
                    die();
                } else {
                    //jika tidak error
                    $foto = $this->upload->data('file_name');
                }
            }

            //cek wilayah

            // $korwil = $this->m_koordinator_wilayah->read_where(['koordinator_wilayah.id_koordinator' => $id])->result_array();
            $hapus = $this->m_koordinator_wilayah->delete_koordinator($id);
            foreach($wilayah as $wil) :

                $simpan = $this->m_koordinator_wilayah->create([
                    'id_koordinator' => $id,
                    'kd_wilayah' => $wil,
                    'created_koordinator_wilayah' => date('Y-m-d H:i:s'),
                ]);

            endforeach;

            //Array
            $data = [
                'nama_koordinator' => $nama,
                'foto_koordinator' => $foto,
                'updated_koordinator' => date('Y-m-d H:i:s'),
            ];
            //Simpan di database lewat model
            $ubah = $this->m_koordinator->update($id, $data);

            //berhasil
            log_act($this->session->userdata('email_user'), 'Ubah Koordinator Wilayah '.$nama, 0, 0, date('Y-m-d H:i:s'));
            $this->session->set_flashdata('success', 'Data berhasil diubah');
            redirect('koordinator');
        }
    }
    
    public function reset($id)
    {
        $koordinator = $this->m_koordinator->read_where(['id_koordinator' => $id])->row_array();
        $ubah = $this->m_user->update($koordinator['email_user'], [
            'password_user' => PASSWORD_HASH(1234, PASSWORD_DEFAULT),
        ]);
        //berhasil
        log_act($this->session->userdata('email_user'), 'Reset Koordinator '.$koordinator['nama_koordinator'], 0, 0, date('Y-m-d H:i:s'));
        $this->session->set_flashdata('success', 'Data berhasil direset');
        redirect('koordinator');
    }

    public function aktif($id)
    {
        $koordinator = $this->m_koordinator->read_where(['id_koordinator' => $id])->row_array();
        $ubah = $this->m_user->update($koordinator['email_user'], [
            'is_active_user' => 1,
        ]);
        //berhasil
        log_act($this->session->userdata('email_user'), 'Aktif Koordinator '.$koordinator['nama_koordinator'], 0, 0, date('Y-m-d H:i:s'));
        $this->session->set_flashdata('success', 'Akun berhasil di aktifasi');
        redirect('koordinator');
    }

    public function blokir($id)
    {
        $koordinator = $this->m_koordinator->read_where(['id_koordinator' => $id])->row_array();
        $ubah = $this->m_user->update($koordinator['email_user'], [
            'is_active_user' => 2,
        ]);
        //berhasil
        log_act($this->session->userdata('email_user'), 'Blokir Koordinator '.$koordinator['nama_koordinator'], 0, 0, date('Y-m-d H:i:s'));
        $this->session->set_flashdata('success', 'Akun berhasil di blokir');
        redirect('koordinator');
    }

    public function cetak()
    {
        log_act($this->session->userdata('email_user'), 'Cetak Kordinator', 0, 0, date('Y-m-d H:i:s'));
        //Ambil data
        $koordinator = $this->m_koordinator->read()->result_array();
        //Halaman Landscape
        //Ukuran kertas A4
        $pdf = new FPDF('l', 'mm', 'A4');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', 'B', 16);
        // mencetak string 
        $pdf->Cell(280, 7, 'DATA KOORDINATOR', 0, 1, 'C');
              
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10, 7, '', 0, 1);
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', 'B', 10);
        // mencetak string 
        $pdf->Cell(10, 6, 'No', 1, 0, 'C');
        $pdf->Cell(70, 6, 'Email', 1, 0, 'C');
        $pdf->Cell(80, 6, 'Nama', 1, 0, 'C');
        $pdf->Cell(60, 6, 'Created', 1, 0, 'C');
        $pdf->Cell(60, 6, 'Updated', 1, 1, 'C');
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', '', 10);
        //nomor
        $no = 1;
        //looping data
        foreach ($koordinator as $key) :

            // mencetak string 
            $pdf->Cell(10, 6, $no++, 1, 0, 'C');
            $pdf->Cell(70, 6, $key['email_user'], 1, 0);
            $pdf->Cell(80, 6, $key['nama_koordinator'], 1, 0);
            $pdf->Cell(60, 6, $key['created_koordinator'], 1, 0, 'C');
            $pdf->Cell(60, 6, $key['updated_koordinator'], 1, 1, 'C');

        endforeach;

        $pdf->Output();
    }

    public function unduh()
    {
        log_act($this->session->userdata('email_user'), 'Unduh Koordinator', 0, 0, date('Y-m-d H:i:s'));
        // Load plugin PHPExcel nya
        include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

        // Panggil class PHPExcel nya
        $excel = new PHPExcel();

        // Settingan awal fil excel
        $excel->getProperties()->setCreator('Pasti CCTV')
            ->setLastModifiedBy('Pasti CCTV')
            ->setTitle("Data Koordinator")
            ->setSubject("Data Koordinator")
            ->setDescription("Laporan Data Koordinator")
            ->setKeywords("Data Koordinator");

        // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
        $style_col = array(
            'font' => array('bold' => true), // Set font nya jadi bold
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
            ),
            'borders' => array(
                'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
                'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
                'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
                'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
            )
        );

        // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
        $style_row = array(
            'alignment' => array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
            ),
            'borders' => array(
                'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
                'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
                'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
                'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
            )
        );

        // Set kolom A1
        $excel->setActiveSheetIndex(0)->setCellValue('A1', "DATA KOORDINATOR");
        // Set kolom B1
        $excel->setActiveSheetIndex(0)->setCellValue('A2', "PASTI CCTV");
        $excel->getActiveSheet()->mergeCells('A1:E1'); // Set Merge Cell pada kolom A1 sampai E1
        $excel->getActiveSheet()->mergeCells('A2:E2'); // Set Merge Cell pada kolom A1 sampai E1
        $excel->getActiveSheet()->getStyle('A1:A2')->getFont()->setBold(TRUE); // Set bold kolom A1
        $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
        $excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12); // Set font size 15 untuk kolom A1
        $excel->getActiveSheet()->getStyle('A1:A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

        // Buat header tabel nya pada baris ke 3
        $excel->setActiveSheetIndex(0)->setCellValue('A4', "No");
        $excel->setActiveSheetIndex(0)->setCellValue('B4', "Email");
        $excel->setActiveSheetIndex(0)->setCellValue('C4', "Nama");
        $excel->setActiveSheetIndex(0)->setCellValue('D4', "Created");
        $excel->setActiveSheetIndex(0)->setCellValue('E4', "Updated");

        // Apply style header yang telah kita buat tadi ke masing-masing kolom header
        $excel->getActiveSheet()->getStyle('A4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('B4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('C4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('D4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('E4')->applyFromArray($style_col);

        //ambil data antrian
        $data = $this->m_koordinator->read()->result_array();
        $numrow = 5; // Set baris pertama untuk isi tabel adalah baris ke 4
        $no = 1; // Set nomor
        foreach ($data as $key) : // Lakukan looping pada variabel siswa

            $excel->setActiveSheetIndex(0)->setCellValue('A' . $numrow, $no++);
            $excel->setActiveSheetIndex(0)->setCellValue('B' . $numrow, $key['email_user']);
            $excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $key['nama_koordinator']);
            $excel->setActiveSheetIndex(0)->setCellValue('D' . $numrow, $key['created_koordinator']);
            $excel->setActiveSheetIndex(0)->setCellValue('E' . $numrow, $key['updated_koordinator']);

            // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
            $excel->getActiveSheet()->getStyle('A' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('B' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('C' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('D' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('E' . $numrow)->applyFromArray($style_row);

            $numrow++; // Tambah 1 setiap kali looping

        endforeach;

        // Set width kolom
        $excel->getActiveSheet()->getColumnDimension('A')->setWidth(5); // Set width kolom A
        $excel->getActiveSheet()->getColumnDimension('B')->setWidth(40); // Set width kolom B
        $excel->getActiveSheet()->getColumnDimension('C')->setWidth(30); // Set width kolom B
        $excel->getActiveSheet()->getColumnDimension('D')->setWidth(20); // Set width kolom B
        $excel->getActiveSheet()->getColumnDimension('E')->setWidth(20); // Set width kolom B

        // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
        $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);

        // Set orientasi kertas jadi LANDSCAPE
        $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

        // Set judul file excel nya
        $excel->getActiveSheet(0)->setTitle("Export Data Koordinator");
        $excel->setActiveSheetIndex(0);

        // Proses file excel
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="Export Data Koordinator.xlsx"'); // Set nama file excel nya
        header('Cache-Control: max-age=0');

        $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $write->save('php://output');
    }
}
