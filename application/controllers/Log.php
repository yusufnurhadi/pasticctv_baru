<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Log extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('pdf');

        if (!$this->session->userdata('id_admin')) {
            $this->session->set_flashdata('error', 'Anda harus login dahulu ');
            redirect();
            die();
        }
    }

    public function index()
    {
        //config pagination
        $config['base_url'] = base_url('log/index/');
        $config['per_page'] = 10;
        $data['start'] = $this->uri->segment(3);

        //keyword
        if ($this->input->post('keyword')) {

            $keyword = $this->input->post('keyword');
            $change = $this->input->post('change');
            $this->session->set_userdata('key_log', $keyword);
            $this->session->set_userdata('change_log', $change);

            $config['total_rows'] = $this->m_log->read_like([
                $this->session->userdata('change_log') => $this->session->userdata('key_log'),
            ])->num_rows();
            $data['log'] = $this->m_log->read_like_pagination([
                $this->session->userdata('change_log') => $this->session->userdata('key_log'),
            ], $config['per_page'], $data['start'])->result_array();

        } else {

            if ($this->session->userdata('key_log')) {

                $config['total_rows'] = $this->m_log->read_like([
                    $this->session->userdata('change_log') => $this->session->userdata('key_log'),
                ])->num_rows();
                $data['log'] = $this->m_log->read_like_pagination([
                    $this->session->userdata('change_log') => $this->session->userdata('key_log')
                ], $config['per_page'], $data['start'])->result_array();

            } else {

                $config['total_rows'] = $this->m_log->read()->num_rows();
                $data['log'] = $this->m_log->read_pagination($config['per_page'], $data['start'])->result_array();

            }

        }
    
        //inisialisasi
        $this->pagination->initialize($config);

        $data['total_rows'] = $config['total_rows'];
        $data['halaman'] = "log";
        $this->load->view('index', $data);
    }

    public function refresh()
    {
        $this->session->unset_userdata('key_log');
        $this->session->unset_userdata('change_log');
        redirect('log');
    }

    public function data_unduh()
    {
        //jalur validasi
        $this->form_validation->set_rules('from', 'From', 'required');
        $this->form_validation->set_rules('to', 'To', 'required');
        
        //validasi
        if ($this->form_validation->run() == false) {
            //tidak valid
            $this->session->set_flashdata('error', form_error('from').form_error('to') );
            echo "<script>javascript:history.back();</script>";
        } else {
            //valid
            $from = $this->input->post('from');
            $to = $this->input->post('to');
            $this->unduh($from, $to);
        }
    }

    public function unduh($awal, $akhir)
    {
        // Load plugin PHPExcel nya
        include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

        // Panggil class PHPExcel nya
        $excel = new PHPExcel();

        // Settingan awal fil excel
        $excel->getProperties()->setCreator('Pasti CCTV')
            ->setLastModifiedBy('Pasti CCTV')
            ->setTitle("Data Log Activity")
            ->setSubject("Data Log Activity")
            ->setDescription("Laporan Data Log Activity")
            ->setKeywords("Data Log Activity");

        // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
        $style_col = array(
            'font' => array('bold' => true), // Set font nya jadi bold
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
            ),
            'borders' => array(
                'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
                'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
                'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
                'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
            )
        );

        // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
        $style_row = array(
            'alignment' => array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
            ),
            'borders' => array(
                'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
                'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
                'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
                'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
            )
        );

        // Set kolom A1
        $excel->setActiveSheetIndex(0)->setCellValue('A1', "DATA LOG ACTIVITY");
        // Set kolom B1
        $excel->setActiveSheetIndex(0)->setCellValue('A2', "PASTI CCTV");
        $excel->getActiveSheet()->mergeCells('A1:G1'); // Set Merge Cell pada kolom A1 sampai E1
        $excel->getActiveSheet()->mergeCells('A2:G2'); // Set Merge Cell pada kolom A1 sampai E1
        $excel->getActiveSheet()->getStyle('A1:A2')->getFont()->setBold(TRUE); // Set bold kolom A1
        $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
        $excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12); // Set font size 15 untuk kolom A1
        $excel->getActiveSheet()->getStyle('A1:A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

        // Buat header tabel nya pada baris ke 3
        $excel->setActiveSheetIndex(0)->setCellValue('A4', "No");
        $excel->setActiveSheetIndex(0)->setCellValue('B4', "Nama User");
        $excel->setActiveSheetIndex(0)->setCellValue('C4', "Aktifitas");
        $excel->setActiveSheetIndex(0)->setCellValue('D4', "Latitude Lokasi");
        $excel->setActiveSheetIndex(0)->setCellValue('E4', "Longitude Lokasi");
        $excel->setActiveSheetIndex(0)->setCellValue('F4', "Time Created");
        $excel->setActiveSheetIndex(0)->setCellValue('G4', "Time Updated");

        // Apply style header yang telah kita buat tadi ke masing-masing kolom header
        $excel->getActiveSheet()->getStyle('A4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('B4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('C4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('D4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('E4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('F4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('G4')->applyFromArray($style_col);

        //ambil data antrian
        $data = $this->m_log->read_where([
            'created_log >=' => $awal,
            'created_log <=' => $akhir,
        ])->result_array();
        $numrow = 5; // Set baris pertama untuk isi tabel adalah baris ke 4
        $no = 1; // Set nomor
        foreach ($data as $key) : // Lakukan looping pada variabel siswa

            $excel->setActiveSheetIndex(0)->setCellValue('A' . $numrow, $no++);
            $excel->setActiveSheetIndex(0)->setCellValue('B' . $numrow, $key['nama_user']);
            $excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $key['activity_log']);
            $excel->setActiveSheetIndex(0)->setCellValue('D' . $numrow, $key['lat_log']);
            $excel->setActiveSheetIndex(0)->setCellValue('E' . $numrow, $key['lng_log']);
            $excel->setActiveSheetIndex(0)->setCellValue('F' . $numrow, $key['created_log']);
            $excel->setActiveSheetIndex(0)->setCellValue('G' . $numrow, $key['updated_log']);

            // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
            $excel->getActiveSheet()->getStyle('A' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('B' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('C' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('D' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('E' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('F' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('G' . $numrow)->applyFromArray($style_row);

            $numrow++; // Tambah 1 setiap kali looping

        endforeach;

        // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
        $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);

        // Set orientasi kertas jadi LANDSCAPE
        $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

        // Set judul file excel nya
        $excel->getActiveSheet(0)->setTitle("Export Data Log Activity");
        $excel->setActiveSheetIndex(0);

        // Proses file excel
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="Export Data Log Activity.xlsx"'); // Set nama file excel nya
        header('Cache-Control: max-age=0');

        $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $write->save('php://output');
    }

}