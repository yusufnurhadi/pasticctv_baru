<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

    public function __construct()
	{
		parent::__construct();
		if (isset($this->session->userdata['email_user'])) {
			redirect('dashboard');
		}
	}

	public function index()
	{
		//jalur validasi
		$this->form_validation->set_rules('email', 'email', 'required|trim');
		$this->form_validation->set_rules('password', 'Password', 'required|trim');

		//validasi
		if ($this->form_validation->run() == false) {
            //tidak valid
            $this->session->set_flashdata('error', form_error('email').form_error('password') );
			$this->load->view('login');
		} else {
			//valid
			$this->_login();
		}
	}

	private function _login()
	{
		//ambil post
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		//cek model
		$user = $this->m_user->read_where(['email_user' => $email])->row_array();

		//validasi user
		if ($user) {
			//user valid
			//cek status user
			if ($user['is_active_user'] == 1) {
				//user aktif (is_active_user == 1)
				//validasi password
				if (password_verify($password, $user['password_user'])) {
					//password valid
					//alihkan ke masing-masing controller
					switch ($user['level_user']) {
						case 1:
							// 1 = admin
							$admin = $this->m_admin->read_where(['admin.email_user' => $email])->row_array();
							$this->session->set_userdata($admin);
							$this->session->set_flashdata('success', 'Selamat Datang '.$admin['nama_admin']);
							log_act($admin['nama_admin'], 'Login Success', 0, 0, date('Y-m-d H:i:s'));
							redirect('dashboard');
							break;

						case 2:
							// 2 = koordinator
							$koordinator = $this->m_koordinator->read_where(['koordinator.email_user' => $email])->row_array();
							$this->session->set_userdata($koordinator);
							$this->session->set_flashdata('success', 'Selamat Datang '.$koordinator['nama_koordinator']);
							redirect('dashboard');
                            break;
                            
                        case 3:
							// 3 = client
							$client = $this->m_client->read_where(['client.email_user' => $email])->row_array();
							$this->session->set_userdata($client);
							$this->session->set_flashdata('success', 'Selamat Datang '.$client['nama_client']);
							redirect('dashboard');
                            break;
                            
                        case 4:
							// 4 = manager_area
							$manager_area = $this->m_manager_area->read_where(['manager_area.email_user' => $email])->row_array();
							$this->session->set_userdata($manager_area);
							$this->session->set_flashdata('success', 'Selamat Datang '.$manager_area['nama_manager_area']);
							redirect('dashboard');
                            break;
                            
                        case 5:
							// 5 = cabang
							$cabang = $this->m_cabang->read_where(['cabang.email_user' => $email])->row_array();
							$this->session->set_userdata($cabang);
							$this->session->set_flashdata('success', 'Selamat Datang '.$cabang['nama_cabang']);
							redirect('dashboard');
                            break;
                            
                        case 6:
							// 6 = pengelola
							$pengelola = $this->m_pengelola->read_where(['pengelola.email_user' => $email])->row_array();
							$this->session->set_userdata($pengelola);
							$this->session->set_flashdata('success', 'Selamat Datang '.$pengelola['nama_pengelola']);
							redirect('dashboard');
                            break;
                            
                        // case 7:
						// 	// 7 = teknisi
						// 	$teknisi = $this->m_teknisi->read_where(['teknisi.email_user' => $email])->row_array();
						// 	$this->session->set_userdata($teknisi);
						// 	$this->session->set_flashdata('success', 'Selamat Datang '.$teknisi['nama_teknisi']);
						// 	redirect('dashboard');
						// 	break;

						default:
                            //default kosong
                            $this->session->set_flashdata('error', 'Anda tidak terdaftar');
                            $this->load->view('login');
                            // redirect();
							break;
					}
				} else {
					//password tidak valid
					$this->session->set_flashdata('error', 'Password salah');
					log_act('Unknow', 'Login Failed ; Password Salah', 0, 0, date('Y-m-d H:i:s'));
                    $this->load->view('login');
					// redirect();
				}
			} else {
				//user tidak aktif (is_active_user == 0)
				$this->session->set_flashdata('error', 'Akun tidak aktif. Tunggu konfirmasi kami melalui email atau hubungi Monitoring MDM agar segera aktifasi akun anda');
				log_act('Unknow', 'Login Failed ; Akun Tidak Aktif', 0, 0, date('Y-m-d H:i:s'));
				$this->load->view('login');
                // redirect();
			}
		} else {
			//user tidak valid
			$this->session->set_flashdata('error', 'Anda tidak terdaftar');
			log_act('Unknow', 'Login Failed ; Tidak Terdaftar', 0, 0, date('Y-m-d H:i:s'));
            $this->load->view('login');
            // redirect();
		}
	}
}