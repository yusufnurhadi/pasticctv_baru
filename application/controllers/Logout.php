<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Logout extends CI_Controller
{

    public function index()
    {
        //logout
        $this->session->sess_destroy();
        redirect();
    }
}
