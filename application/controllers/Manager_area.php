<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Manager_area extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('pdf');

        if (!$this->session->userdata('email_user')) {
            $this->session->set_flashdata('error', 'Anda harus login dahulu ');
            redirect();
            die();
        }
    }

    public function index()
    {
        log_act($this->session->userdata('email_user'), 'Page Manager Area', 0, 0, date('Y-m-d H:i:s'));
        //config pagination
        $config['base_url'] = base_url('manager_area/index/');
        $config['per_page'] = 10;
        $data['start'] = $this->uri->segment(3);

        //keyword
        if ($this->input->post('keyword')) {

            $keyword = $this->input->post('keyword');
            $this->session->set_userdata('key_manager_area', $keyword);
        } else {
            // $config['total_rows'] = $this->m_manager_area->read()->num_rows();
            // $data['manager_area'] = $this->m_manager_area->read_pagination($config['per_page'], $data['start'])->result_array();
        }

        $config['total_rows'] = $this->m_manager_area->read_like([
            'nama_manager_area' => $this->session->userdata('key_manager_area'),
        ])->num_rows();
        $data['manager_area'] = $this->m_manager_area->read_like_pagination(array('nama_manager_area' => $this->session->userdata('key_manager_area')), $config['per_page'], $data['start'])->result_array();
        //inisialisasi
        $this->pagination->initialize($config);

        $data['total_rows'] = $config['total_rows'];
        $data['wilayah'] = $this->m_wilayah->read()->result_array();
        $data['halaman'] = "manager_area";
        $this->load->view('index', $data);
    }

    public function refresh()
    {
        log_act($this->session->userdata('email_user'), 'Refresh Manager Area', 0, 0, date('Y-m-d H:i:s'));
        $this->session->unset_userdata('key_manager_area');
        redirect('manager_area');
    }

    public function hapus($id)
    {
        log_act($this->session->userdata('email_user'), 'Hapus Manager Area', 0, 0, date('Y-m-d H:i:s'));
        $manager_area = $this->m_manager_area->read_where(['id_manager_area'=>$id])->row_array();
        $this->m_manager_area->delete($manager_area['email_user']);
        $this->session->set_flashdata('success', 'Data berhasil di hapus');
        echo "<script>javascript:history.back();</script>";
    }

    public function tambah()
    {
        log_act($this->session->userdata('email_user'), 'Tambah Manager Area', 0, 0, date('Y-m-d H:i:s'));
        //jalur validasi
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('wilayah', 'Wilayah', 'required');
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        
        //cek foto
        if ($_FILES['foto']['name'] == null) {
            //tidak ada foto
            $foto = 'default.jpg';
        } else {
            //ada foto
            //config upload
            $config['upload_path']          = 'img/profile/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 2000;
            $config['file_name']            = 'profile' . date('YmdHis');
            //load library
            $this->load->library('upload', $config);
            //kondisi error
            if (!$this->upload->do_upload('foto')) {
                //jika error
                $this->session->set_flashdata('error', $this->upload->display_errors());
                echo "<script>javascript:history.back();</script>";
                die();
            } else {
                //jika tidak error
                $foto = $this->upload->data('file_name');
            }
        }
     
     
     //validasi
     if ($this->form_validation->run() == false) {
         //tidak valid
         $this->session->set_flashdata('error', form_error('email'),form_error('wilayah').form_error('nama') );
         echo "<script>javascript:history.back();</script>";
     } else {
         //valid
         $email = $this->input->post('email');
         $wilayah = $this->input->post('wilayah');
         $nama = $this->input->post('nama');
         //Array
         $dataUser = [
             'email_user' => $email,
             'password_user' => PASSWORD_HASH(1234, PASSWORD_DEFAULT),
             'level_user' => '4',
             'is_active_user' => '1',
             'created_user' => date('Y-m-d H:i:s'),
         ];
         $data = [
             'email_user' => $email,
             'kd_wilayah' => $wilayah,
             'nama_manager_area' => $nama,
             'foto_manager_area' => $foto,
             'notif_manager_area' => 1,
             'created_manager_area' => date('Y-m-d H:i:s'),
         ];
         //Simpan di database lewat model
         $simpanManager = $this->m_user->create($dataUser);
         $simpan = $this->m_manager_area->create($data);
        //  Berhasil
         $this->session->set_flashdata('success', 'Data berhasil ditambah');
         redirect('manager_area');
     }
    }

    public function ubah($id)
    {
        log_act($this->session->userdata('email_user'), 'Ubah Manager Area', 0, 0, date('Y-m-d H:i:s'));
        //jalur validasi
        $this->form_validation->set_rules('email','Email','required');
        $this->form_validation->set_rules('wilayah','Wilayah','required');
        $this->form_validation->set_rules('nama','Nama','required');

        //validasi
        if ($this->form_validation->run() == false) {
            //tidak valid
            $this->session->set_flashdata('error', form_error('wilayah'). form_error('nama'));
            echo "<script>javascript:history.back();</script>";
            // redirect('c_wilayah');
        } else {
            //valid
            $wilayah = $this->input->post('wilayah');
            $nama  = $this->input->post('nama');
            $manager_area = $this->m_manager_area->read_where(['id_manager_area'=>$id])->result_array();

            //cek foto
            if ($_FILES['foto']['name'] == null) {
                //tidak ada foto
                $foto = $manager_area[0]['foto_manager_area'];
            } else {
                //ada foto
                //config upload
                $config['upload_path']          = 'img/profile/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 2000;
                $config['file_name']            = 'profile' . date('YmdHis');
                //load library
                $this->load->library('upload', $config);
                //kondisi error
                if (!$this->upload->do_upload('foto')) {
                    //jika error
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    echo "<script>javascript:history.back();</script>";
                    die();
                } else {
                    //jika tidak error
                    $foto = $this->upload->data('file_name');
                }
            }
            
            //Array Client
            $dataManager = [
                'kd_wilayah' => $wilayah,
                'nama_manager_area' => $nama,
                'updated_manager_area' => date('Y-m-d H:i:s'),
                'foto_manager_area'=> $foto,
            ];
            //Simpan di database lewat model
            $simpanManger = $this->m_manager_area->update($id, $dataManager);
            //berhasil
            $this->session->set_flashdata('success', 'Data berhasil diubah');
            redirect('manager_area');
        }
    }

    public function reset($id)
    {
        $manager_area = $this->m_manager_area->read_where(['id_manager_area' => $id])->row_array();
        $ubah = $this->m_user->update($manager_area['email_user'], [
            'password_user' => PASSWORD_HASH(1234, PASSWORD_DEFAULT),
        ]);
        //berhasil
        log_act($this->session->userdata('email_user'), 'Reset Manager Area '.$manager_area['nama_manager_area'], 0, 0, date('Y-m-d H:i:s'));
        $this->session->set_flashdata('success', 'Data berhasil direset');
        redirect('manager_area');
    }

    public function aktif($id)
    {
        $manager_area = $this->m_manager_area->read_where(['id_manager_area' => $id])->row_array();
        $ubah = $this->m_user->update($manager_area['email_user'], [
            'is_active_user' => 1,
        ]);
        //berhasil
        log_act($this->session->userdata('email_user'), 'Aktif Manager Area '.$manager_area['nama_manager_area'], 0, 0, date('Y-m-d H:i:s'));
        $this->session->set_flashdata('success', 'Akun berhasil di aktifasi');
        redirect('manager_area');
    }

    public function blokir($id)
    {
        $manager_area = $this->m_manager_area->read_where(['id_manager_area' => $id])->row_array();
        $ubah = $this->m_user->update($manager_area['email_user'], [
            'is_active_user' => 2,
        ]);
        //berhasil
        log_act($this->session->userdata('email_user'), 'Blokir Manager Area '.$manager_area['nama_manager_area'], 0, 0, date('Y-m-d H:i:s'));
        $this->session->set_flashdata('success', 'Akun berhasil di blokir');
        redirect('manager_area');
    }

    public function cetak()
    {
        log_act($this->session->userdata('email_user'), 'Cetak Manager Area', 0, 0, date('Y-m-d H:i:s'));
        //Ambil data
        $manager_area = $this->m_manager_area->read()->result_array();
        //Halaman Landscape
        //Ukuran kertas A4
        $pdf = new FPDF('l', 'mm', 'A4');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', 'B', 16);
        // mencetak string 
        $pdf->Cell(280, 7, 'DATA MANAGER AREA', 0, 1, 'C');
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', 'B', 12);
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10, 7, '', 0, 1);
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', 'B', 10);
        // mencetak string 
        $pdf->Cell(10, 6, 'No', 1, 0, 'C');
        $pdf->Cell(50, 6, 'Email', 1, 0, 'C');
        $pdf->Cell(80, 6, 'Wilayah', 1, 0, 'C');
        $pdf->Cell(60, 6, 'Manager Area', 1, 0, 'C');
        $pdf->Cell(40, 6, 'Created', 1, 0, 'C');
        $pdf->Cell(40, 6, 'Updated', 1, 1, 'C');
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', '', 10);
        //nomor
        $no = 1;
        //looping data
        foreach ($manager_area as $key) :

            // mencetak string 
            $pdf->Cell(10, 6, $no++, 1, 0, 'C');
            $pdf->Cell(50, 6, $key['email_user'], 1, 0);
            $pdf->Cell(80, 6, $key['nama_wilayah'], 1, 0);
            $pdf->Cell(60, 6, $key['nama_manager_area'], 1, 0);
            $pdf->Cell(40, 6, $key['created_manager_area'], 1, 0, 'C');
            $pdf->Cell(40, 6, $key['updated_manager_area'], 1, 1, 'C');

        endforeach;

        $pdf->Output();
    }

    public function unduh()
    {
        log_act($this->session->userdata('email_user'), 'Unduh Manager Area', 0, 0, date('Y-m-d H:i:s'));
        // Load plugin PHPExcel nya
        include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

        // Panggil class PHPExcel nya
        $excel = new PHPExcel();

        // Settingan awal fil excel
        $excel->getProperties()->setCreator('Pasti CCTV')
            ->setLastModifiedBy('Pasti CCTV')
            ->setTitle("Data Manager Area")
            ->setSubject("Data Manager Area")
            ->setDescription("Laporan Data Manager Area")
            ->setKeywords("Data Manager Area");

        // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
        $style_col = array(
            'font' => array('bold' => true), // Set font nya jadi bold
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
            ),
            'borders' => array(
                'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
                'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
                'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
                'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
            )
        );

        // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
        $style_row = array(
            'alignment' => array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
            ),
            'borders' => array(
                'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
                'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
                'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
                'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
            )
        );

        // Set kolom A1
        $excel->setActiveSheetIndex(0)->setCellValue('A1', "DATA MANAGER AREA");
        // Set kolom B1
        $excel->setActiveSheetIndex(0)->setCellValue('A2', "PASTI CCTV");
        $excel->getActiveSheet()->mergeCells('A1:F1'); // Set Merge Cell pada kolom A1 sampai F1
        $excel->getActiveSheet()->mergeCells('A2:F2'); // Set Merge Cell pada kolom A1 sampai F1
        $excel->getActiveSheet()->getStyle('A1:A2')->getFont()->setBold(TRUE); // Set bold kolom A1
        $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
        $excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12); // Set font size 15 untuk kolom A1
        $excel->getActiveSheet()->getStyle('A1:A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

        // Buat header tabel nya pada baris ke 3
        $excel->setActiveSheetIndex(0)->setCellValue('A4', "No");
        $excel->setActiveSheetIndex(0)->setCellValue('B4', "Email");
        $excel->setActiveSheetIndex(0)->setCellValue('C4', "Wilayah");
        $excel->setActiveSheetIndex(0)->setCellValue('D4', "Manager Area");
        $excel->setActiveSheetIndex(0)->setCellValue('E4', "Created");
        $excel->setActiveSheetIndex(0)->setCellValue('F4', "Updated");

        // Apply style header yang telah kita buat tadi ke masing-masing kolom header
        $excel->getActiveSheet()->getStyle('A4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('B4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('C4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('D4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('E4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('F4')->applyFromArray($style_col);

        //ambil data antrian
        $data = $this->m_manager_area->read()->result_array();
        $numrow = 5; // Set baris pertama untuk isi tabel adalah baris ke 4
        $no = 1; // Set nomor
        foreach ($data as $key) : // Lakukan looping pada variabel siswa

            $excel->setActiveSheetIndex(0)->setCellValue('A' . $numrow, $no++);
            $excel->setActiveSheetIndex(0)->setCellValue('B' . $numrow, $key['email_user']);
            $excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $key['nama_wilayah']);
            $excel->setActiveSheetIndex(0)->setCellValue('D' . $numrow, $key['nama_manager_area']);
            $excel->setActiveSheetIndex(0)->setCellValue('E' . $numrow, $key['created_manager_area']);
            $excel->setActiveSheetIndex(0)->setCellValue('F' . $numrow, $key['updated_manager_area']);

            // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
            $excel->getActiveSheet()->getStyle('A' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('B' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('C' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('D' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('E' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('F' . $numrow)->applyFromArray($style_row);

            $numrow++; // Tambah 1 setiap kali looping

        endforeach;

        // Set width kolom
        $excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);  // Set width kolom A
        $excel->getActiveSheet()->getColumnDimension('B')->setWidth(30); // Set width kolom B
        $excel->getActiveSheet()->getColumnDimension('C')->setWidth(40); // Set width kolom B
        $excel->getActiveSheet()->getColumnDimension('D')->setWidth(30); // Set width kolom B
        $excel->getActiveSheet()->getColumnDimension('E')->setWidth(20); // Set width kolom B
        $excel->getActiveSheet()->getColumnDimension('F')->setWidth(20); // Set width kolom B

        // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
        $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);

        // Set orientasi kertas jadi LANDSCAPE
        $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

        // Set judul file excel nya
        $excel->getActiveSheet(0)->setTitle("Export Data Manager Area");
        $excel->setActiveSheetIndex(0);

        // Proses file excel
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="Export Data Manager Area.xlsx"'); // Set nama file excel nya
        header('Cache-Control: max-age=0');

        $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $write->save('php://output');
    }
}
