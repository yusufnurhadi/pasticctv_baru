<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Part extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('pdf');

        if (!$this->session->userdata('email_user')) {
            $this->session->set_flashdata('error', 'Anda harus login dahulu ');
            redirect();
            die();
        }
    }

    public function index()
    {
        //config pagination
        $config['base_url'] = base_url('part/index/');
        $config['per_page'] = 10;
        $data['start'] = $this->uri->segment(3);

        //keyword
        if ($this->input->post('keyword')) {

            $keyword = $this->input->post('keyword');
            $this->session->set_userdata('key_part', $keyword);
        } else {
            // $config['total_rows'] = $this->m_part->read()->num_rows();
            // $data['part'] = $this->m_part->read_pagination($config['per_page'], $data['start'])->result_array();
        }

        $config['total_rows'] = $this->m_part->read_like([
            'nama_part' => $this->session->userdata('key_part'),
        ])->num_rows();
        $data['part'] = $this->m_part->read_like_pagination(array('nama_part' => $this->session->userdata('key_part')), $config['per_page'], $data['start'])->result_array();
        //inisialisasi
        $this->pagination->initialize($config);

        $data['total_rows'] = $config['total_rows'];
        $data['halaman'] = "part";
        $this->load->view('index', $data);
    }

    public function refresh()
    {
        $this->session->unset_userdata('key_part');
        redirect('part');
    }

    public function tambah()
    {
        //jalur validasi
        $this->form_validation->set_rules('kode', 'Kode Part', 'required');
        $this->form_validation->set_rules('nama', 'Nama Part', 'required');
       

        //validasi
        if ($this->form_validation->run() == false) {
            //tidak valid
            $this->session->set_flashdata('error', form_error('kode') . form_error('nama'));
            echo "<script>javascript:history.back();</script>";
            // redirect('c_part');
        } else {
            //valid
            $kode = $this->input->post('kode');
            $nama = $this->input->post('nama');
            //Array part
            $datapart = [
                'kd_part' => $kode,
                'nama_part' => $nama,
                'created_part' => date('Y-m-d H:i:s'),
            ];
            //Simpan di database lewat model
            $simpanpart = $this->m_part->create($datapart);
            //berhasil
            $this->session->set_flashdata('success', 'Data berhasil ditambah');
            redirect('part');
        }
    }

    public function ubah($id)
    {
        //jalur validasi
        $this->form_validation->set_rules('kode', 'Kode Part', 'required');
        $this->form_validation->set_rules('nama', 'Nama Part', 'required');
       

        //validasi
        if ($this->form_validation->run() == false) {
            //tidak valid
            $this->session->set_flashdata('error', form_error('kode') . form_error('nama'));
            echo "<script>javascript:history.back();</script>";
            // redirect('c_part');
        } else {
            //valid
            $kode = $this->input->post('kode');
            $nama = $this->input->post('nama');
            //Array part
            $datapart = [
                'kd_part' => $kode,
                'nama_part' => $nama,
                'updated_part' => date('Y-m-d H:i:s'),
            ];
            //Simpan di database lewat model
            $simpanpart = $this->m_part->update($id, $datapart);
            //berhasil
            $this->session->set_flashdata('success', 'Data berhasil diubah');
            redirect('part');
        }
    }

    public function hapus($id)
    {
        $this->m_part->delete($id);
        $this->session->set_flashdata('success', 'Data berhasil di hapus');
        echo "<script>javascript:history.back();</script>";
    }

    public function cetak()
    {
        //Ambil data
        $part = $this->m_part->read()->result_array();
        //Halaman Landscape
        //Ukuran kertas A4
        $pdf = new FPDF('l', 'mm', 'A4');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', 'B', 16);
        // mencetak string 
        $pdf->Cell(280, 7, 'DATA PART', 0, 1, 'C');
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', 'B', 12);
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10, 7, '', 0, 1);
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', 'B', 10);
        // mencetak string 
        $pdf->Cell(10, 6, 'No', 1, 0, 'C');
        $pdf->Cell(60, 6, 'Kode Part', 1, 0, 'C');
        $pdf->Cell(90, 6, 'Nama Part', 1, 0, 'C');
        $pdf->Cell(60, 6, 'Created', 1, 0, 'C');
        $pdf->Cell(60, 6, 'Updated', 1, 1, 'C');
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', '', 10);
        //nomor
        $no = 1;
        //looping data
        foreach ($part as $key) :

            // mencetak string 
            $pdf->Cell(10, 6, $no++, 1, 0, 'C');
            $pdf->Cell(60, 6, $key['kd_part'], 1, 0);
            $pdf->Cell(90, 6, $key['nama_part'], 1, 0);
            $pdf->Cell(60, 6, $key['created_part'], 1, 0, 'C');
            $pdf->Cell(60, 6, $key['updated_part'], 1, 1, 'C');

        endforeach;

        $pdf->Output();
        
    }
    
    public function unduh()
    {
         // Load plugin PHPExcel nya
         include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

         // Panggil class PHPExcel nya
         $excel = new PHPExcel();
 
         // Settingan awal fil excel
         $excel->getProperties()->setCreator('Pasti CCTV')
             ->setLastModifiedBy('Pasti CCTV')
             ->setTitle("Data Part")
             ->setSubject("Data Part")
             ->setDescription("Laporan Data Part")
             ->setKeywords("Data Part");
 
         // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
         $style_col = array(
             'font' => array('bold' => true), // Set font nya jadi bold
             'alignment' => array(
                 'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
                 'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
             ),
             'borders' => array(
                 'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
                 'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
                 'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
                 'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
             )
         );
 
         // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
         $style_row = array(
             'alignment' => array(
                 'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
             ),
             'borders' => array(
                 'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
                 'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
                 'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
                 'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
             )
         );
 
         // Set kolom A1
         $excel->setActiveSheetIndex(0)->setCellValue('A1', "DATA PART");
         // Set kolom B1
         $excel->setActiveSheetIndex(0)->setCellValue('A2', "PASTI CCTV");
         $excel->getActiveSheet()->mergeCells('A1:E1'); // Set Merge Cell pada kolom A1 sampai F1
         $excel->getActiveSheet()->mergeCells('A2:E2'); // Set Merge Cell pada kolom A1 sampai F1
         $excel->getActiveSheet()->getStyle('A1:A2')->getFont()->setBold(TRUE); // Set bold kolom A1
         $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
         $excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12); // Set font size 15 untuk kolom A1
         $excel->getActiveSheet()->getStyle('A1:A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
 
         // Buat header tabel nya pada baris ke 3
         $excel->setActiveSheetIndex(0)->setCellValue('A4', "No");
         $excel->setActiveSheetIndex(0)->setCellValue('B4', "Kode Part");
         $excel->setActiveSheetIndex(0)->setCellValue('C4', "Nama Part");
         $excel->setActiveSheetIndex(0)->setCellValue('D4', "Created");
         $excel->setActiveSheetIndex(0)->setCellValue('E4', "Updated");
         
         // Apply style header yang telah kita buat tadi ke masing-masing kolom header
         $excel->getActiveSheet()->getStyle('A4')->applyFromArray($style_col);
         $excel->getActiveSheet()->getStyle('B4')->applyFromArray($style_col);
         $excel->getActiveSheet()->getStyle('C4')->applyFromArray($style_col);
         $excel->getActiveSheet()->getStyle('D4')->applyFromArray($style_col);
         $excel->getActiveSheet()->getStyle('E4')->applyFromArray($style_col);
                  
 
         //ambil data antrian
         $data = $this->m_part->read()->result_array();
         $numrow = 5; // Set baris pertama untuk isi tabel adalah baris ke 4
         $no = 1; // Set nomor
         foreach ($data as $key) : // Lakukan looping pada variabel siswa
 
             $excel->setActiveSheetIndex(0)->setCellValue('A' . $numrow, $no++);
             $excel->setActiveSheetIndex(0)->setCellValue('B' . $numrow, $key['kd_part']);
             $excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $key['nama_part']);
             $excel->setActiveSheetIndex(0)->setCellValue('D' . $numrow, $key['created_part']);
             $excel->setActiveSheetIndex(0)->setCellValue('E' . $numrow, $key['updated_part']);
 
             // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
             $excel->getActiveSheet()->getStyle('A' . $numrow)->applyFromArray($style_row);
             $excel->getActiveSheet()->getStyle('B' . $numrow)->applyFromArray($style_row);
             $excel->getActiveSheet()->getStyle('C' . $numrow)->applyFromArray($style_row);
             $excel->getActiveSheet()->getStyle('D' . $numrow)->applyFromArray($style_row);
             $excel->getActiveSheet()->getStyle('E' . $numrow)->applyFromArray($style_row);
              
             $numrow++; // Tambah 1 setiap kali looping
 
         endforeach;
 
         // Set width kolom
         $excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);  // Set width kolom A
         $excel->getActiveSheet()->getColumnDimension('B')->setWidth(30); // Set width kolom B
         $excel->getActiveSheet()->getColumnDimension('C')->setWidth(40); // Set width kolom B
         $excel->getActiveSheet()->getColumnDimension('D')->setWidth(20); // Set width kolom B
         $excel->getActiveSheet()->getColumnDimension('E')->setWidth(20); // Set width kolom B
                   
         // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
         $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
 
         // Set orientasi kertas jadi LANDSCAPE
         $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
 
         // Set judul file excel nya
         $excel->getActiveSheet(0)->setTitle("Export Data Part");
         $excel->setActiveSheetIndex(0);
 
         // Proses file excel
         header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
         header('Content-Disposition: attachment; filename="Export Data Part.xlsx"'); // Set nama file excel nya
         header('Cache-Control: max-age=0');
 
         $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
         $write->save('php://output');
    }
}
