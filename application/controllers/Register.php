<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Register extends CI_Controller
{

    public function __construct()
	{
		parent::__construct();
		if (isset($this->session->userdata['email_user'])) {
			redirect('dashboard');
		}
	}

	public function index()
	{
		//jalur validasi
		$this->form_validation->set_rules('nama', 'nama', 'required|trim');
		$this->form_validation->set_rules('email', 'email', 'required|trim|is_unique[user.email_user]', [
            'is_unique' => 'Email sudah terdaftar',
        ]);
		$this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[4]|matches[password2]', [
			'matches' => 'Password tidak cocok',
			'min_length' => 'Password terlalu pendek. Minimal 4 karakter'
		]);
		$this->form_validation->set_rules('password2', 'Password', 'required|trim|min_length[4]|matches[password1]', [
			'matches' => 'Password tidak cocok',
			'min_length' => 'Password terlalu pendek. Minimal 4 karakter'
		]);
		$this->form_validation->set_rules('akses', 'Password', 'required|trim');

		//validasi
		if ($this->form_validation->run() == false) {
            //tidak valid
            $this->session->set_flashdata('error', form_error('nama').form_error('email').form_error('password1').form_error('password2').form_error('akses'));
            $data['wilayah'] = $this->m_wilayah->read()->result_array();
            $data['cabang'] = $this->m_atm_cabang->read()->result_array();
            $data['pengelola'] = $this->m_atm_pengelola->read()->result_array();
			$this->load->view('register', $data);
		} else {
			//valid
			$this->_register();
		}
	}

	private function _register()
	{
		//ambil post
		$nama = $this->input->post('nama');
		$email = $this->input->post('email');
		$password = $this->input->post('password1');
        $akses = $this->input->post('akses');

        // Array
        $dataUser = [
            'email_user' => $email,
            'password_user' => password_hash($password, PASSWORD_DEFAULT),
            'level_user' => $akses,
            'is_active_user' => 0,
            'created_user' => date('Y-m-d H:i:s'),
        ];
        //simpan array
        $this->m_user->create($dataUser);
        
        switch ($akses) {

            case 1:
                // 1 = admin
                // Array
                $array = [
                    'email_user' => $email,
                    'nama_admin' => $nama,
                    'foto_admin' => 'default.jpg',
                    'notif_admin' => 1,
                    'created_admin' => date('Y-m-d H:i:s'),
                ];
                //simpan array
                $this->m_admin->create($array);
                $this->session->set_flashdata('success', 'Akun Berhasil didaftarkan. Tunggu konfirmasi kami melalui email atau hubungi pihak Monitoring MDM agar segera aktifasi akun anda');
                redirect();
                break;
                
            // case 7:
            // 	// 7 = teknisi
            // 	$teknisi = $this->m_teknisi->read_where(['teknisi.email_user' => $email])->row_array();
            // 	$this->session->set_userdata($teknisi);
            // 	$this->session->set_flashdata('success', 'Selamat Datang '.$teknisi['nama_teknisi']);
            // 	redirect('dashboard');
            // 	break;

            default:
                //default kosong
                $this->session->set_flashdata('error', 'Anda tidak terdaftar');
                $this->load->view('register');
                // redirect();
                break;

        }

		//validasi user
		if ($user) {
			//user valid
			//cek status user
			if ($user['is_active_user'] == 1) {
				//user aktif (is_active_user == 1)
				//validasi password
				if (password_verify($password, $user['password_user'])) {
					//password valid
					//alihkan ke masing-masing controller
					switch ($user['level_user']) {
						case 1:
							// 1 = admin
							$admin = $this->m_admin->read_where(['admin.email_user' => $email])->row_array();
							$this->session->set_userdata($admin);
							$this->session->set_flashdata('success', 'Selamat Datang '.$admin['nama_admin']);
							redirect('dashboard');
							break;

						case 2:
							// 2 = koordinator
							$koordinator = $this->m_koordinator->read_where(['koordinator.email_user' => $email])->row_array();
							$this->session->set_userdata($koordinator);
							$this->session->set_flashdata('success', 'Selamat Datang '.$koordinator['nama_koordinator']);
							redirect('dashboard');
                            break;
                            
                        case 3:
							// 3 = client
							$client = $this->m_client->read_where(['client.email_user' => $email])->row_array();
							$this->session->set_userdata($client);
							$this->session->set_flashdata('success', 'Selamat Datang '.$client['nama_client']);
							redirect('dashboard');
                            break;
                            
                        case 4:
							// 4 = manager_area
							$manager_area = $this->m_manager_area->read_where(['manager_area.email_user' => $email])->row_array();
							$this->session->set_userdata($manager_area);
							$this->session->set_flashdata('success', 'Selamat Datang '.$manager_area['nama_manager_area']);
							redirect('dashboard');
                            break;
                            
                        case 5:
							// 5 = cabang
							$cabang = $this->m_cabang->read_where(['cabang.email_user' => $email])->row_array();
							$this->session->set_userdata($cabang);
							$this->session->set_flashdata('success', 'Selamat Datang '.$cabang['nama_cabang']);
							redirect('dashboard');
                            break;
                            
                        case 6:
							// 6 = pengelola
							$pengelola = $this->m_pengelola->read_where(['pengelola.email_user' => $email])->row_array();
							$this->session->set_userdata($pengelola);
							$this->session->set_flashdata('success', 'Selamat Datang '.$pengelola['nama_pengelola']);
							redirect('dashboard');
                            break;
                            
                        // case 7:
						// 	// 7 = teknisi
						// 	$teknisi = $this->m_teknisi->read_where(['teknisi.email_user' => $email])->row_array();
						// 	$this->session->set_userdata($teknisi);
						// 	$this->session->set_flashdata('success', 'Selamat Datang '.$teknisi['nama_teknisi']);
						// 	redirect('dashboard');
						// 	break;

						default:
                            //default kosong
                            $this->session->set_flashdata('error', 'Anda tidak terdaftar');
                            $this->load->view('register');
                            // redirect();
							break;
					}
				} else {
					//password tidak valid
					$this->session->set_flashdata('error', 'Password salah');
                    $this->load->view('register');
					// redirect();
				}
			} else {
				//user tidak aktif (is_active_user == 0)
				$this->session->set_flashdata('error', 'Akun tidak aktif. Tunggu konfirmasi kami melalui email atau hubungi Monitoring MDM agar segera aktifasi akun anda');
                $this->load->view('register');
                // redirect();
			}
		} else {
			//user tidak valid
			$this->session->set_flashdata('error', 'Anda tidak terdaftar');
            $this->load->view('register');
            // redirect();
		}
	}
}