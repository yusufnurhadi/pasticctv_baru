<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Wilayah extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('pdf');

        if (!$this->session->userdata('email_user')) {
            $this->session->set_flashdata('error', 'Anda harus login dahulu ');
            redirect();
            die();
        }
    }

    public function index()
    {
        //config pagination
        $config['base_url'] = base_url('wilayah/index/');
        $config['per_page'] = 10;
        $data['start'] = $this->uri->segment(3);

        //keyword
        if ($this->input->post('keyword')) {

            $keyword = $this->input->post('keyword');
            $change = $this->input->post('change');
            $this->session->set_userdata('key_wilayah', $keyword);
            $this->session->set_userdata('change_wilayah', $change);

            $config['total_rows'] = $this->m_wilayah->read_like([
                $this->session->userdata('change_wilayah') => $this->session->userdata('key_wilayah'),
            ])->num_rows();
            $data['wilayah_cms'] = $this->m_wilayah->read_like_pagination([
                $this->session->userdata('change_wilayah') => $this->session->userdata('key_wilayah'),
            ], $config['per_page'], $data['start'])->result_array();

        } else {

            if ($this->session->userdata('key_wilayah')) {

                $config['total_rows'] = $this->m_wilayah->read_like([
                    $this->session->userdata('change_wilayah') => $this->session->userdata('key_wilayah'),
                ])->num_rows();
                $data['wilayah_cms'] = $this->m_wilayah->read_like_pagination([
                    $this->session->userdata('change_wilayah') => $this->session->userdata('key_wilayah')
                ], $config['per_page'], $data['start'])->result_array();

            } else {

                $config['total_rows'] = $this->m_wilayah->read()->num_rows();
                $data['wilayah_cms'] = $this->m_wilayah->read_pagination($config['per_page'], $data['start'])->result_array();

            }

        }

        //inisialisasi
        $this->pagination->initialize($config);
        $data['total_rows'] = $config['total_rows'];
        $data['halaman'] = "wilayah";
        log_act($this->session->userdata('email_user'), 'Page Wilayah', 0, 0, date('Y-m-d H:i:s'));
        $this->load->view('index', $data);
    }

    public function refresh()
    {
        $this->session->unset_userdata('key_wilayah');
        $this->session->unset_userdata('change_wilayah');
        log_act($this->session->userdata('email_user'), 'Refresh Wilayah', 0, 0, date('Y-m-d H:i:s'));
        redirect('wilayah');
    }

    public function tambah()
    {
        //jalur validasi
        $this->form_validation->set_rules('nama', 'Nama Wilayah', 'required');
        $this->form_validation->set_rules('kode', 'Kode Wilayah', 'required|is_unique[wilayah.kd_wilayah]', [
            'is_unique' => 'Kode Wilayah sudah terdaftar',
        ]);

        //validasi
        if ($this->form_validation->run() == false) {
            //tidak valid
            $this->session->set_flashdata('error', form_error('nama') . form_error('kode'));
            echo "<script>javascript:history.back();</script>";
            // redirect('c_wilayah');
        } else {
            //valid
            $nama = $this->input->post('nama');
            $kode = $this->input->post('kode');
            //Array Wilayah
            $dataWilayah = [
                'kd_wilayah' => $kode,
                'nama_wilayah' => $nama,
                'created_wilayah' => date('Y-m-d H:i:s'),
            ];
            //Simpan di database lewat model
            $simpanWilayah = $this->m_wilayah->create($dataWilayah);
            //berhasil
            log_act($this->session->userdata('email_user'), 'Tambah Wilayah '.$kode, 0, 0, date('Y-m-d H:i:s'));
            $this->session->set_flashdata('success', 'Data berhasil ditambah');
            redirect('wilayah');
        }
    }

    public function ubah($id)
    {
        //jalur validasi
        $this->form_validation->set_rules('nama', 'Nama Wilayah', 'required');

        //validasi
        if ($this->form_validation->run() == false) {
            //tidak valid
            $this->session->set_flashdata('error', form_error('nama') . form_error('kode'));
            echo "<script>javascript:history.back();</script>";
            // redirect('c_wilayah');
        } else {
            //valid
            $nama = $this->input->post('nama');
            //Array Wilayah
            $dataWilayah = [
                'nama_wilayah' => $nama,
                'updated_wilayah' => date('Y-m-d H:i:s'),
            ];
            //Simpan di database lewat model
            $simpanWilayah = $this->m_wilayah->update($id, $dataWilayah);
            //berhasil
            log_act($this->session->userdata('email_user'), 'Ubah Wilayah '.$id, 0, 0, date('Y-m-d H:i:s'));
            $this->session->set_flashdata('success', 'Data berhasil diubah');
            redirect('wilayah');
        }
    }

    public function hapus($id)
    {
        $this->m_wilayah->delete($id);
        log_act($this->session->userdata('email_user'), 'Hapus Wilayah '.$id, 0, 0, date('Y-m-d H:i:s'));
        $this->session->set_flashdata('success', 'Data berhasil di hapus');
        echo "<script>javascript:history.back();</script>";
    }

    public function cetak()
    {
        log_act($this->session->userdata('email_user'), 'Cetak Wilayah', 0, 0, date('Y-m-d H:i:s'));
        //Ambil data
        $wilayah = $this->m_wilayah->read()->result_array();
        //Halaman Landscape
        //Ukuran kertas A4
        $pdf = new FPDF('l', 'mm', 'A4');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', 'B', 16);
        // mencetak string 
        $pdf->Cell(280, 7, 'DATA WILAYAH', 0, 1, 'C');
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', 'B', 12);
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10, 7, '', 0, 1);
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', 'B', 10);
        // mencetak string 
        $pdf->Cell(10, 6, 'No', 1, 0, 'C');
        $pdf->Cell(50, 6, 'Kode Wilayah', 1, 0, 'C');
        $pdf->Cell(100, 6, 'Nama Wilayah', 1, 0, 'C');
        $pdf->Cell(60, 6, 'Created', 1, 0, 'C');
        $pdf->Cell(60, 6, 'Updated', 1, 1, 'C');
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', '', 10);
        //nomor
        $no = 1;
        //looping data
        foreach ($wilayah as $key) :

            // mencetak string 
            $pdf->Cell(10, 6, $no++, 1, 0, 'C');
            $pdf->Cell(50, 6, $key['kd_wilayah'], 1, 0);
            $pdf->Cell(100, 6, $key['nama_wilayah'], 1, 0);
            $pdf->Cell(60, 6, $key['created_wilayah'], 1, 0, 'C');
            $pdf->Cell(60, 6, $key['updated_wilayah'], 1, 1, 'C');

        endforeach;

        $pdf->Output();
    }

    public function unduh()
    {
        log_act($this->session->userdata('email_user'), 'Unduh Wilayah', 0, 0, date('Y-m-d H:i:s'));
        // Load plugin PHPExcel nya
        include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

        // Panggil class PHPExcel nya
        $excel = new PHPExcel();

        // Settingan awal fil excel
        $excel->getProperties()->setCreator('Pasti CCTV')
            ->setLastModifiedBy('Pasti CCTV')
            ->setTitle("Data Wilayah")
            ->setSubject("Data Wilayah")
            ->setDescription("Laporan Data Wilayah")
            ->setKeywords("Data Wilayah");

        // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
        $style_col = array(
            'font' => array('bold' => true), // Set font nya jadi bold
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
            ),
            'borders' => array(
                'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
                'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
                'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
                'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
            )
        );

        // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
        $style_row = array(
            'alignment' => array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
            ),
            'borders' => array(
                'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
                'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
                'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
                'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
            )
        );

        // Set kolom A1
        $excel->setActiveSheetIndex(0)->setCellValue('A1', "DATA WILAYAH");
        // Set kolom B1
        $excel->setActiveSheetIndex(0)->setCellValue('A2', "PASTI CCTV");
        $excel->getActiveSheet()->mergeCells('A1:E1'); // Set Merge Cell pada kolom A1 sampai E1
        $excel->getActiveSheet()->mergeCells('A2:E2'); // Set Merge Cell pada kolom A1 sampai E1
        $excel->getActiveSheet()->getStyle('A1:A2')->getFont()->setBold(TRUE); // Set bold kolom A1
        $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
        $excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12); // Set font size 15 untuk kolom A1
        $excel->getActiveSheet()->getStyle('A1:A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

        // Buat header tabel nya pada baris ke 3
        $excel->setActiveSheetIndex(0)->setCellValue('A4', "No");
        $excel->setActiveSheetIndex(0)->setCellValue('B4', "Kode Wilayah");
        $excel->setActiveSheetIndex(0)->setCellValue('C4', "Nama Wilayah");
        $excel->setActiveSheetIndex(0)->setCellValue('D4', "Created");
        $excel->setActiveSheetIndex(0)->setCellValue('E4', "Updated");

        // Apply style header yang telah kita buat tadi ke masing-masing kolom header
        $excel->getActiveSheet()->getStyle('A4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('B4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('C4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('D4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('E4')->applyFromArray($style_col);

        //ambil data antrian
        $data = $this->m_wilayah->read()->result_array();
        $numrow = 5; // Set baris pertama untuk isi tabel adalah baris ke 4
        $no = 1; // Set nomor
        foreach ($data as $key) : // Lakukan looping pada variabel siswa

            $excel->setActiveSheetIndex(0)->setCellValue('A' . $numrow, $no++);
            $excel->setActiveSheetIndex(0)->setCellValue('B' . $numrow, $key['kd_wilayah']);
            $excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $key['nama_wilayah']);
            $excel->setActiveSheetIndex(0)->setCellValue('D' . $numrow, $key['created_wilayah']);
            $excel->setActiveSheetIndex(0)->setCellValue('E' . $numrow, $key['updated_wilayah']);

            // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
            $excel->getActiveSheet()->getStyle('A' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('B' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('C' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('D' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('E' . $numrow)->applyFromArray($style_row);

            $numrow++; // Tambah 1 setiap kali looping

        endforeach;

        // Set width kolom
        $excel->getActiveSheet()->getColumnDimension('A')->setWidth(5); // Set width kolom A
        $excel->getActiveSheet()->getColumnDimension('B')->setWidth(20); // Set width kolom B
        $excel->getActiveSheet()->getColumnDimension('C')->setWidth(40); // Set width kolom B
        $excel->getActiveSheet()->getColumnDimension('D')->setWidth(20); // Set width kolom B
        $excel->getActiveSheet()->getColumnDimension('E')->setWidth(20); // Set width kolom B

        // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
        $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);

        // Set orientasi kertas jadi LANDSCAPE
        $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

        // Set judul file excel nya
        $excel->getActiveSheet(0)->setTitle("Export Data Wilayah");
        $excel->setActiveSheetIndex(0);

        // Proses file excel
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="Export Data Wilayah.xlsx"'); // Set nama file excel nya
        header('Cache-Control: max-age=0');

        $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $write->save('php://output');
    }
}
