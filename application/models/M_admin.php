<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_admin extends CI_Model
{

	public function create($data)
	{
		$this->db->insert('admin', $data);
	}
	public function read()
	{
		$this->db->join('user', 'user.email_user = admin.email_user');
		return $this->db->get('admin');
	}
	public function read_where($array)
	{
		$this->db->join('user', 'user.email_user = admin.email_user');
		return $this->db->get_where('admin', $array);
	}
	public function read_pagination($limit, $start)
	{
		$this->db->join('user', 'user.email_user = admin.email_user');
		return $this->db->get('admin', $limit, $start);
	}
	public function read_like($array)
	{
		$this->db->join('user', 'user.email_user = admin.email_user');
		$this->db->like($array);
		return $this->db->get('admin');
	}
	public function read_like_pagination($array, $limit, $start)
	{
		$this->db->join('user', 'user.email_user = admin.email_user');
		$this->db->like($array);
		return $this->db->get('admin', $limit, $start);
	}
	public function update($id, $data)
	{
		$this->db->update('admin', $data, ['id_admin' => $id]);
	}
	public function delete($email)
	{
		$tables = array('admin', 'user');
		$this->db->where('email_user', $email);
		$this->db->delete($tables);
	}
}
