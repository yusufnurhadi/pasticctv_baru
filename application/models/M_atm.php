<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_atm extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('atm', $data);
    }
    public function read()
    {
        $this->db->join('atm_cabang', 'atm_cabang.kd_atm_cabang = atm.kd_atm_cabang');
        $this->db->join('atm_pengelola', 'atm_pengelola.kd_atm_pengelola = atm.kd_atm_pengelola');
        return $this->db->get('atm');
    }
    public function read_where($array)
    {
        $this->db->join('atm_cabang', 'atm_cabang.kd_atm_cabang = atm.kd_atm_cabang');
        $this->db->join('atm_pengelola', 'atm_pengelola.kd_atm_pengelola = atm.kd_atm_pengelola');
        return $this->db->get_where('atm', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->join('atm_cabang', 'atm_cabang.kd_atm_cabang = atm.kd_atm_cabang');
        $this->db->join('atm_pengelola', 'atm_pengelola.kd_atm_pengelola = atm.kd_atm_pengelola');
        return $this->db->get('atm', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->join('atm_cabang', 'atm_cabang.kd_atm_cabang = atm.kd_atm_cabang');
        $this->db->join('atm_pengelola', 'atm_pengelola.kd_atm_pengelola = atm.kd_atm_pengelola');
        $this->db->like($array);
        return $this->db->get('atm');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->join('atm_cabang', 'atm_cabang.kd_atm_cabang = atm.kd_atm_cabang');
        $this->db->join('atm_pengelola', 'atm_pengelola.kd_atm_pengelola = atm.kd_atm_pengelola');
        $this->db->like($array);
        return $this->db->get('atm', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('atm', $data, ['id_atm' => $id]);
    }
    public function delete($id)
    {
        $tables = array('atm');
        $this->db->where('id_atm', $id);
        $this->db->delete($tables);
    }
}