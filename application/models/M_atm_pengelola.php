<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_atm_pengelola extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('atm_pengelola', $data);
    }
    public function read()
    {
        $this->db->join('wilayah', 'wilayah.kd_wilayah = atm_pengelola.kd_wilayah');
        return $this->db->get('atm_pengelola');
    }
    public function read_where($array)
    {
        $this->db->join('wilayah', 'wilayah.kd_wilayah = atm_pengelola.kd_wilayah');
        return $this->db->get_where('atm_pengelola', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->join('wilayah', 'wilayah.kd_wilayah = atm_pengelola.kd_wilayah');
        return $this->db->get('atm_pengelola', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->join('wilayah', 'wilayah.kd_wilayah = atm_pengelola.kd_wilayah');
        $this->db->like($array);
        return $this->db->get('atm_pengelola');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->join('wilayah', 'wilayah.kd_wilayah = atm_pengelola.kd_wilayah');
        $this->db->like($array);
        return $this->db->get('atm_pengelola', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('atm_pengelola', $data, ['kd_atm_pengelola' => $id]);
    }
    public function delete($id)
    {
        $tables = array('atm_pengelola');
        $this->db->where('kd_atm_pengelola', $id);
        $this->db->delete($tables);
    }
}
