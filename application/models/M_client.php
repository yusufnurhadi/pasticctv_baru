<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_client extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('client', $data);
    }
    public function read()
    {
        $this->db->join('user', 'user.email_user = client.email_user');
        $this->db->join('wilayah', 'wilayah.kd_wilayah = client.kd_wilayah');
        return $this->db->get('client');
    }
    public function read_where($array)
    {
        $this->db->join('user', 'user.email_user = client.email_user');
        $this->db->join('wilayah', 'wilayah.kd_wilayah = client.kd_wilayah');
        return $this->db->get_where('client', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->join('user', 'user.email_user = client.email_user');
        $this->db->join('wilayah', 'wilayah.kd_wilayah = client.kd_wilayah');
        return $this->db->get('client', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->join('user', 'user.email_user = client.email_user');
        $this->db->join('wilayah', 'wilayah.kd_wilayah = client.kd_wilayah');
        $this->db->like($array);
        return $this->db->get('client');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->join('user', 'user.email_user = client.email_user');
        $this->db->join('wilayah', 'wilayah.kd_wilayah = client.kd_wilayah');
        $this->db->like($array);
        return $this->db->get('client', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('client', $data, ['id_client' => $id]);
    }
    public function delete($email)
    {
        $tables = array('client','user');
        $this->db->where('email_user', $email);
        $this->db->delete($tables);
    }
}
