<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_koordinator extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('koordinator', $data);
    }
    public function read()
    {
        $this->db->join('user', 'user.email_user = koordinator.email_user');
        return $this->db->get('koordinator');
    }
    public function read_where($array)
    {
        $this->db->join('user', 'user.email_user = koordinator.email_user');
        return $this->db->get_where('koordinator', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->join('user', 'user.email_user = koordinator.email_user');
        return $this->db->get('koordinator', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->join('user', 'user.email_user = koordinator.email_user');
        $this->db->like($array);
        return $this->db->get('koordinator');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->join('user', 'user.email_user = koordinator.email_user');
        $this->db->like($array);
        return $this->db->get('koordinator', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('koordinator', $data, ['id_koordinator' => $id]);
    }
    public function delete($id)
    {
        $tables = array('koordinator');
        $this->db->where('id_koordinator', $id);
        $this->db->delete($tables);
    }
}