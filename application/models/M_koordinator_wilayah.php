<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_koordinator_wilayah extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('koordinator_wilayah', $data);
    }
    public function read()
    {
        $this->db->join('koordinator', 'koordinator.id_koordinator = koordinator_wilayah.id_koordinator');
        $this->db->join('wilayah', 'wilayah.kd_wilayah = koordinator_wilayah.kd_wilayah');
        return $this->db->get('koordinator_wilayah');
    }
    public function read_where($array)
    {
        $this->db->join('koordinator', 'koordinator.id_koordinator = koordinator_wilayah.id_koordinator');
        $this->db->join('wilayah', 'wilayah.kd_wilayah = koordinator_wilayah.kd_wilayah');
        return $this->db->get_where('koordinator_wilayah', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->join('koordinator', 'koordinator.id_koordinator = koordinator_wilayah.id_koordinator');
        $this->db->join('wilayah', 'wilayah.kd_wilayah = koordinator_wilayah.kd_wilayah');
        return $this->db->get('koordinator_wilayah', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->join('koordinator', 'koordinator.id_koordinator = koordinator_wilayah.id_koordinator');
        $this->db->join('wilayah', 'wilayah.kd_wilayah = koordinator_wilayah.kd_wilayah');
        $this->db->like($array);
        return $this->db->get('koordinator_wilayah');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->join('koordinator', 'koordinator.id_koordinator = koordinator_wilayah.id_koordinator');
        $this->db->join('wilayah', 'wilayah.kd_wilayah = koordinator_wilayah.kd_wilayah');
        $this->db->like($array);
        return $this->db->get('koordinator_wilayah', $limit, $start);
    }
    public function read_like_group($array)
    {
        $this->db->join('koordinator', 'koordinator.id_koordinator = koordinator_wilayah.id_koordinator');
        $this->db->join('user', 'user.email_user = koordinator.email_user');
        $this->db->join('wilayah', 'wilayah.kd_wilayah = koordinator_wilayah.kd_wilayah');
        $this->db->like($array);
        $this->db->group_by("koordinator.id_koordinator");
        return $this->db->get('koordinator_wilayah');
    }
    public function read_like_pagination_group($array, $limit, $start)
    {
        $this->db->join('koordinator', 'koordinator.id_koordinator = koordinator_wilayah.id_koordinator');
        $this->db->join('user', 'user.email_user = koordinator.email_user');
        $this->db->join('wilayah', 'wilayah.kd_wilayah = koordinator_wilayah.kd_wilayah');
        $this->db->like($array);
        $this->db->group_by("koordinator.id_koordinator");
        return $this->db->get('koordinator_wilayah', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('koordinator_wilayah', $data, ['id_koordinator_wilayah' => $id]);
    }
    public function delete($id)
    {
        $tables = array('koordinator_wilayah');
        $this->db->where('id_koordinator_wilayah', $id);
        $this->db->delete($tables);
    }
    public function delete_where($id, $kd)
    {
        $tables = array('koordinator_wilayah');
        $this->db->where('id_koordinator', $id);
        $this->db->where('kd_wilayah', $kd);
        $this->db->delete($tables);
    }
    public function delete_koordinator($id)
    {
        $tables = array('koordinator_wilayah');
        $this->db->where('id_koordinator', $id);
        $this->db->delete($tables);
    }
}
