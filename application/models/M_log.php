<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_log extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('log', $data);
    }
    public function read()
    {
        $this->db->order_by('created_log', 'DESC');
        return $this->db->get('log');
    }
    public function read_where($array)
    {
        $this->db->order_by('created_log', 'DESC');
        return $this->db->get_where('log', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->order_by('created_log', 'DESC');
        return $this->db->get('log', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->like($array);
        $this->db->order_by('created_log', 'DESC');
        return $this->db->get('log');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->like($array);
        $this->db->order_by('created_log', 'DESC');
        return $this->db->get('log', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('log', $data, ['id_log' => $id]);
    }
    public function delete($id)
    {
        $tables = array('log');
        $this->db->where('id_log', $id);
        $this->db->delete($tables);
    }
}
