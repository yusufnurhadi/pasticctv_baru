<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_manager_area extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('manager_area', $data);
    }
    public function read()
    {
        $this->db->join('user', 'user.email_user = manager_area.email_user');
        $this->db->join('wilayah', 'wilayah.kd_wilayah = manager_area.kd_wilayah');
        return $this->db->get('manager_area');
    }
    public function read_where($array)
    {
        $this->db->join('user', 'user.email_user = manager_area.email_user');
        $this->db->join('wilayah', 'wilayah.kd_wilayah = manager_area.kd_wilayah');
        return $this->db->get_where('manager_area', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->join('user', 'user.email_user = manager_area.email_user');
        $this->db->join('wilayah', 'wilayah.kd_wilayah = manager_area.kd_wilayah');
        return $this->db->get('manager_area', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->join('user', 'user.email_user = manager_area.email_user');
        $this->db->join('wilayah', 'wilayah.kd_wilayah = manager_area.kd_wilayah');
        $this->db->like($array);
        return $this->db->get('manager_area');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->join('user', 'user.email_user = manager_area.email_user');
        $this->db->join('wilayah', 'wilayah.kd_wilayah = manager_area.kd_wilayah');
        $this->db->like($array);
        return $this->db->get('manager_area', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->join('user', 'user.email_user = manager_area.email_user');
        $this->db->join('wilayah', 'wilayah.kd_wilayah = manager_area.kd_wilayah');
        $this->db->update('manager_area', $data, ['id_manager_area' => $id]);
    }
    public function delete($email)
    {
        $tables = array('manager_area','user');
        $this->db->where('email_user', $email);
        $this->db->delete($tables);
    }
}
