<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_part extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('part', $data);
    }
    public function read()
    {
        return $this->db->get('part');
    }
    public function read_where($array)
    {
        return $this->db->get_where('part', $array);
    }
    public function read_pagination($limit, $start)
    {
        return $this->db->get('part', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->like($array);
        return $this->db->get('part');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->like($array);
        return $this->db->get('part', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('part', $data, ['kd_part' => $id]);
    }
    public function delete($id)
    {
        $tables = array('part');
        $this->db->where('kd_part', $id);
        $this->db->delete($tables);
    }
}
