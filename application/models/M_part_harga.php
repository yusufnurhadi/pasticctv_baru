<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_part_harga extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('part_harga', $data);
    }
    public function read()
    {
        $this->db->join('part', 'part.kd_part = part_harga.kd_part');
        $this->db->join('wilayah', 'wilayah.kd_wilayah = part_harga.kd_wilayah');
        return $this->db->get('part_harga');
    }
    public function read_where($array)
    {
        $this->db->join('part', 'part.kd_part = part_harga.kd_part');
        $this->db->join('wilayah', 'wilayah.kd_wilayah = part_harga.kd_wilayah');
        return $this->db->get_where('part_harga', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->join('part', 'part.kd_part = part_harga.kd_part');
        $this->db->join('wilayah', 'wilayah.kd_wilayah = part_harga.kd_wilayah');
        return $this->db->get('part_harga', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->join('part', 'part.kd_part = part_harga.kd_part');
        $this->db->join('wilayah', 'wilayah.kd_wilayah = part_harga.kd_wilayah');
        $this->db->like($array);
        return $this->db->get('part_harga');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->join('part', 'part.kd_part = part_harga.kd_part');
        $this->db->join('wilayah', 'wilayah.kd_wilayah = part_harga.kd_wilayah');
        $this->db->like($array);
        return $this->db->get('part_harga', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('part_harga', $data, ['id_part_harga' => $id]);
    }
    public function delete($id)
    {
        $tables = array('part_harga');
        $this->db->where('id_part_harga', $id);
        $this->db->delete($tables);
    }
}
