<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_snapshoot_cm extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('snapshoot_cm', $data);
    }
    public function read()
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = snapshoot_cm.id_ticket_header');
        return $this->db->get('snapshoot_cm');
    }
    public function read_where($array)
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = snapshoot_cm.id_ticket_header');
        return $this->db->get_where('snapshoot_cm', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = snapshoot_cm.id_ticket_header');
        return $this->db->get('snapshoot_cm', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = snapshoot_cm.id_ticket_header');
        $this->db->like($array);
        return $this->db->get('snapshoot_cm');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = snapshoot_cm.id_ticket_header');
        $this->db->like($array);
        return $this->db->get('snapshoot_cm', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('snapshoot_cm', $data, ['id_snapshoot_cm' => $id]);
    }
    public function delete($id)
    {
        $tables = array('snapshoot_cm');
        $this->db->where('id_snapshoot_cm', $id);
        $this->db->delete($tables);
    }
}
