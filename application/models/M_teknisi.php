<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_teknisi extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('teknisi', $data);
    }
    public function read()
    {
        $this->db->join('user', 'user.email_user = teknisi.email_user');
        return $this->db->get('teknisi');
    }
    public function read_where($array)
    {
        $this->db->join('user', 'user.email_user = teknisi.email_user');
        return $this->db->get_where('teknisi', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->join('user', 'user.email_user = teknisi.email_user');
        return $this->db->get('teknisi', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->join('user', 'user.email_user = teknisi.email_user');
        $this->db->like($array);
        return $this->db->get('teknisi');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->join('user', 'user.email_user = teknisi.email_user');
        $this->db->like($array);
        return $this->db->get('teknisi', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('teknisi', $data, ['id_teknisi' => $id]);
    }
    public function delete($email)
    {
        $tables = array('teknisi','user');
        $this->db->where('email_user', $email);
        $this->db->delete($tables);
}
}
