<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_ticket_cancel extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('ticket_cancel', $data);
    }
    public function read()
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = ticket_cancel_ticket_header');
        $this->db->join('pengelola', 'pengelola.id_pengelola = ticket_cancel.id_pengelola');
        return $this->db->get('ticket_cancel');
    }
    public function read_where($array)
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = ticket_cancel_ticket_header');
        $this->db->join('pengelola', 'pengelola.id_pengelola = ticket_cancel.id_pengelola');
        return $this->db->get_where('ticket_cancel', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = ticket_cancel_ticket_header');
        $this->db->join('pengelola', 'pengelola.id_pengelola = ticket_cancel.id_pengelola');
        return $this->db->get('ticket_cancel', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = ticket_cancel_ticket_header');
        $this->db->join('pengelola', 'pengelola.id_pengelola = ticket_cancel.id_pengelola');
        $this->db->like($array);
        return $this->db->get('ticket_cancel');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = ticket_cancel_ticket_header');
        $this->db->join('pengelola', 'pengelola.id_pengelola = ticket_cancel.id_pengelola');
        $this->db->like($array);
        return $this->db->get('ticket_cancel', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('ticket_cancel', $data, ['id_ticket_cancel' => $id]);
    }
    public function delete($id)
    {
        $tables = array('ticket_cancel');
        $this->db->where('id_ticket_cancel', $id);
        $this->db->delete($tables);
    }
}
