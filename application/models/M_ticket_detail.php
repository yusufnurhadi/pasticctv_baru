<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_ticket_detail extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('ticket_detail', $data);
    }
    public function read()
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = ticket_detail.id_ticket_header');
        $this->db->join('koordinator', 'koordinator.id_koordinator = ticket_detail.id_koordinator');
        $this->db->join('teknisi', 'teknisi.id_teknisi = ticket_detail.id_teknisi');
        return $this->db->get('ticket_detail');
    }
    public function read_where($array)
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = ticket_detail.id_ticket_header');
        $this->db->join('koordinator', 'koordinator.id_koordinator = ticket_detail.id_koordinator');
        $this->db->join('teknisi', 'teknisi.id_teknisi = ticket_detail.id_teknisi');
        return $this->db->get_where('ticket_detail', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = ticket_detail.id_ticket_header');
        $this->db->join('koordinator', 'koordinator.id_koordinator = ticket_detail.id_koordinator');
        $this->db->join('teknisi', 'teknisi.id_teknisi = ticket_detail.id_teknisi');
        return $this->db->get('ticket_detail', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = ticket_detail.id_ticket_header');
        $this->db->join('koordinator', 'koordinator.id_koordinator = ticket_detail.id_koordinator');
        $this->db->join('teknisi', 'teknisi.id_teknisi = ticket_detail.id_teknisi');
        $this->db->like($array);
        return $this->db->get('ticket_detail');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = ticket_detail.id_ticket_header');
        $this->db->join('koordinator', 'koordinator.id_koordinator = ticket_detail.id_koordinator');
        $this->db->join('teknisi', 'teknisi.id_teknisi = ticket_detail.id_teknisi');
        $this->db->like($array);
        return $this->db->get('ticket_detail', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('ticket_detail', $data, ['id_ticket_detail' => $id]);
    }
    public function delete($id)
    {
        $tables = array('ticket_detail');
        $this->db->where('id_ticket_detail', $id);
        $this->db->delete($tables);
    }
}
