<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_ticket_header extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('ticket_header', $data);
    }
    public function read()
    {
        $this->db->join('atm', 'atm.id_atm = ticket_header.id_atm');
        $this->db->join('pengelola', 'pengelola.id_pengelola = ticket_header.id_pengelola');
        return $this->db->get('ticket_header');
    }
    public function read_where($array)
    {
        $this->db->join('atm', 'atm.id_atm = ticket_header.id_atm');
        $this->db->join('pengelola', 'pengelola.id_pengelola = ticket_header.id_pengelola');
        return $this->db->get_where('ticket_header', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->join('atm', 'atm.id_atm = ticket_header.id_atm');
        $this->db->join('pengelola', 'pengelola.id_pengelola = ticket_header.id_pengelola');
        return $this->db->get('ticket_header', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->join('atm', 'atm.id_atm = ticket_header.id_atm');
        $this->db->join('pengelola', 'pengelola.id_pengelola = ticket_header.id_pengelola');
        $this->db->like($array);
        return $this->db->get('ticket_header');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->join('atm', 'atm.id_atm = ticket_header.id_atm');
        $this->db->join('pengelola', 'pengelola.id_pengelola = ticket_header.id_pengelola');
        $this->db->like($array);
        return $this->db->get('ticket_header', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('ticket_header', $data, ['id_ticket_header' => $id]);
    }
    public function delete($id)
    {
        $tables = array('ticket_header');
        $this->db->where('id_ticket_header', $id);
        $this->db->delete($tables);
    }
}
