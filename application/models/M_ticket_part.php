<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_ticket_part extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('ticket_part', $data);
    }
    public function read()
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = ticket_part.id_ticket_header');
        $this->db->join('part', 'part.kd_part = ticket_part.kd_part');
        return $this->db->get('ticket_part');
    }
    public function read_where($array)
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = ticket_part.id_ticket_header');
        $this->db->join('part', 'part.kd_part = ticket_part.kd_part');
        return $this->db->get_where('ticket_part', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = ticket_part.id_ticket_header');
        $this->db->join('part', 'part.kd_part = ticket_part.kd_part');
        return $this->db->get('ticket_part', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = ticket_part.id_ticket_header');
        $this->db->join('part', 'part.kd_part = ticket_part.kd_part');
        $this->db->like($array);
        return $this->db->get('ticket_part');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = ticket_part.id_ticket_header');
        $this->db->join('part', 'part.kd_part = ticket_part.kd_part');
        $this->db->like($array);
        return $this->db->get('ticket_part', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('ticket_part', $data, ['id_ticket_part' => $id]);
    }
    public function delete($id)
    {
        $tables = array('ticket_part');
        $this->db->where('id_ticket_part', $id);
        $this->db->delete($tables);
    }
}
