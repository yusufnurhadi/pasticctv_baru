<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_ticket_pending extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('ticket_pending', $data);
    }
    public function read()
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = ticket_pending.id_ticket_header');
        $this->db->join('teknisi', 'teknisi.id_teknisi = ticket_pending.id_teknisi');
        return $this->db->get('ticket_pending');
    }
    public function read_where($array)
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = ticket_pending.id_ticket_header');
        $this->db->join('teknisi', 'teknisi.id_teknisi = ticket_pending.id_teknisi');
        return $this->db->get_where('ticket_pending', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = ticket_pending.id_ticket_header');
        $this->db->join('teknisi', 'teknisi.id_teknisi = ticket_pending.id_teknisi');
        return $this->db->get('ticket_pending', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = ticket_pending.id_ticket_header');
        $this->db->join('teknisi', 'teknisi.id_teknisi = ticket_pending.id_teknisi');
        $this->db->like($array);
        return $this->db->get('ticket_pending');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = ticket_pending.id_ticket_header');
        $this->db->join('teknisi', 'teknisi.id_teknisi = ticket_pending.id_teknisi');
        $this->db->like($array);
        return $this->db->get('ticket_pending', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('ticket_pending', $data, ['id_ticket_pending' => $id]);
    }
    public function delete($id)
    {
        $tables = array('ticket_pending');
        $this->db->where('id_ticket_pending', $id);
        $this->db->delete($tables);
    }
}
