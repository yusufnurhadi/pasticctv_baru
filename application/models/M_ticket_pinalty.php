<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_ticket_pinalty extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('ticket_pinalty', $data);
    }
    public function read()
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = ticket_pinalty.id_ticket_header');
        return $this->db->get('ticket_pinalty');
    }
    public function read_where($array)
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = ticket_pinalty.id_ticket_header');
        return $this->db->get_where('ticket_pinalty', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = ticket_pinalty.id_ticket_header');
        return $this->db->get('ticket_pinalty', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = ticket_pinalty.id_ticket_header');
        $this->db->like($array);
        return $this->db->get('ticket_pinalty');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = ticket_pinalty.id_ticket_header');
        $this->db->like($array);
        return $this->db->get('ticket_pinalty', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('ticket_pinalty', $data, ['id_ticket_pinalty' => $id]);
    }
    public function delete($id)
    {
        $tables = array('ticket_pinalty');
        $this->db->where('id_ticket_pinalty', $id);
        $this->db->delete($tables);
    }
}
