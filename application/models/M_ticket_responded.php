<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_ticket_responded extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('ticket_responded', $data);
    }
    public function read()
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = ticket_responded.id_ticket_header');
        $this->db->join('teknisi', 'teknisi.id_teknisi = ticket_responded.id_teknisi');
        return $this->db->get('ticket_responded');
    }
    public function read_where($array)
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = ticket_responded.id_ticket_header');
        $this->db->join('teknisi', 'teknisi.id_teknisi = ticket_responded.id_teknisi');
        return $this->db->get_where('ticket_responded', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = ticket_responded.id_ticket_header');
        $this->db->join('teknisi', 'teknisi.id_teknisi = ticket_responded.id_teknisi');
        return $this->db->get('ticket_responded', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = ticket_responded.id_ticket_header');
        $this->db->join('teknisi', 'teknisi.id_teknisi = ticket_responded.id_teknisi');
        $this->db->like($array);
        return $this->db->get('ticket_responded');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = ticket_responded.id_ticket_header');
        $this->db->join('teknisi', 'teknisi.id_teknisi = ticket_responded.id_teknisi');
        $this->db->like($array);
        return $this->db->get('ticket_responded', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('ticket_responded', $data, ['id_ticket_responded' => $id]);
    }
    public function delete($id)
    {
        $tables = array('ticket_responded');
        $this->db->where('id_ticket_responded', $id);
        $this->db->delete($tables);
    }
}
