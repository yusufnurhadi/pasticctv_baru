<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_ticket_sn extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('ticket_sn', $data);
    }
    public function read()
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = ticket_sn.id_ticket_header');
        return $this->db->get('ticket_sn');
    }
    public function read_where($array)
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = ticket_sn.id_ticket_header');
        return $this->db->get_where('ticket_sn', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = ticket_sn.id_ticket_header');
        return $this->db->get('ticket_sn', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = ticket_sn.id_ticket_header');
        $this->db->like($array);
        return $this->db->get('ticket_sn');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = ticket_sn.id_ticket_header');
        $this->db->like($array);
        return $this->db->get('ticket_sn', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('ticket_sn', $data, ['id_ticket_sn' => $id]);
    }
    public function delete($id)
    {
        $tables = array('ticket_sn');
        $this->db->where('id_ticket_sn', $id);
        $this->db->delete($tables);
    }
}
