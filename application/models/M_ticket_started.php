<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_ticket_started extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('ticket_started', $data);
    }
    public function read()
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = ticket_started.id_ticket_header');
        $this->db->join('teknisi', 'teknisi.id_teknisi = ticket_started.id_teknisi');
        return $this->db->get('ticket_started');
    }
    public function read_where($array)
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = ticket_started.id_ticket_header');
        $this->db->join('teknisi', 'teknisi.id_teknisi = ticket_started.id_teknisi');
        return $this->db->get_where('ticket_started', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = ticket_started.id_ticket_header');
        $this->db->join('teknisi', 'teknisi.id_teknisi = ticket_started.id_teknisi');
        return $this->db->get('ticket_started', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = ticket_started.id_ticket_header');
        $this->db->join('teknisi', 'teknisi.id_teknisi = ticket_started.id_teknisi');
        $this->db->like($array);
        return $this->db->get('ticket_started');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->join('ticket_header', 'ticket_header.id_ticket_header = ticket_started.id_ticket_header');
        $this->db->join('teknisi', 'teknisi.id_teknisi = ticket_started.id_teknisi');
        $this->db->like($array);
        return $this->db->get('ticket_started', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('ticket_started', $data, ['id_ticket_started' => $id]);
    }
    public function delete($id)
    {
        $tables = array('ticket_started');
        $this->db->where('id_ticket_started', $id);
        $this->db->delete($tables);
    }
}
