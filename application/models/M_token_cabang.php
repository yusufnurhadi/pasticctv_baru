<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_token_cabang extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('token_cabang', $data);
    }
    public function read()
    {
        $this->db->join('cabang', 'cabang.id_cabang = token_cabang.id_cabang');
        return $this->db->get('token_cabang');
    }
    public function read_where($array)
    {
        $this->db->join('cabang', 'cabang.id_cabang = token_cabang.id_cabang');
        return $this->db->get_where('token_cabang', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->join('cabang', 'cabang.id_cabang = token_cabang.id_cabang');
        return $this->db->get('token_cabang', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->join('cabang', 'cabang.id_cabang = token_cabang.id_cabang');
        $this->db->like($array);
        return $this->db->get('token_cabang');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->join('cabang', 'cabang.id_cabang = token_cabang.id_cabang');
        $this->db->like($array);
        return $this->db->get('token_cabang', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('token_cabang', $data, ['token_cabang' => $id]);
    }
    public function delete($id)
    {
        $tables = array('token_cabang');
        $this->db->where('token_cabang', $id);
        $this->db->delete($tables);
    }
}
