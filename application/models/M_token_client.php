<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_token_client extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('token_client', $data);
    }
    public function read()
    {
        $this->db->join('client', 'client.id_client = token_client.id_client');
        return $this->db->get('token_client');
    }
    public function read_where($array)
    {
        $this->db->join('client', 'client.id_client = token_client.id_client');
        return $this->db->get_where('token_client', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->join('client', 'client.id_client = token_client.id_client');
        return $this->db->get('token_client', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->join('client', 'client.id_client = token_client.id_client');
        $this->db->like($array);
        return $this->db->get('token_client');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->join('client', 'client.id_client = token_client.id_client');
        $this->db->like($array);
        return $this->db->get('token_client', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('token_client', $data, ['token_client' => $id]);
    }
    public function delete($id)
    {
        $tables = array('token_client');
        $this->db->where('token_client', $id);
        $this->db->delete($tables);
    }
}
