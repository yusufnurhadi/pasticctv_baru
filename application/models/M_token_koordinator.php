<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_token_koordinator extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('token_koordinator', $data);
    }
    public function read()
    {
        $this->db->join('koordinator', 'koordinator.id_koordinator = token_koordinator.id_koordinator');
        return $this->db->get('token_koordinator');
    }
    public function read_where($array)
    {
        return $this->db->get_where('token_koordinator', $array);
    }
    public function read_pagination($limit, $start)
    {
        return $this->db->get('token_koordinator', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->like($array);
        return $this->db->get('token_koordinator');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->like($array);
        return $this->db->get('token_koordinator', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('token_koordinator', $data, ['token_koordinator' => $id]);
    }
    public function delete($id)
    {
        $tables = array('token_koordinator');
        $this->db->where('token_koordinator', $id);
        $this->db->delete($tables);
    }
}
