<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_token_manager_area extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('token_manager_area', $data);
    }
    public function read()
    {
        $this->db->join('manager_area', 'manager_area.id_manager_area = token_manager_area.id_manager_area');
        return $this->db->get('token_manager_area');
    }
    public function read_where($array)
    {
        $this->db->join('manager_area', 'manager_area.id_manager_area = token_manager_area.id_manager_area');
        return $this->db->get_where('token_manager_area', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->join('manager_area', 'manager_area.id_manager_area = token_manager_area.id_manager_area');
        return $this->db->get('token_manager_area', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->join('manager_area', 'manager_area.id_manager_area = token_manager_area.id_manager_area');
        $this->db->like($array);
        return $this->db->get('token_manager_area');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->join('manager_area', 'manager_area.id_manager_area = token_manager_area.id_manager_area');
        $this->db->like($array);
        return $this->db->get('token_manager_area', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('token_manager_area', $data, ['token_manager_area' => $id]);
    }
    public function delete($id)
    {
        $tables = array('token_manager_area');
        $this->db->where('token_manager_area', $id);
        $this->db->delete($tables);
    }
}
