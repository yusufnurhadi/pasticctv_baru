<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_token_pengelola extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('token_pengelola', $data);
    }
    public function read()
    {
        $this->db->join('pengelola', 'pengelola.id_pengelola = token_pengelola.id_pengelola');
        return $this->db->get('token_pengelola');
    }
    public function read_where($array)
    {
        return $this->db->get_where('token_pengelola', $array);
    }
    public function read_pagination($limit, $start)
    {
        return $this->db->get('token_pengelola', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->like($array);
        return $this->db->get('token_pengelola');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->like($array);
        return $this->db->get('token_pengelola', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('token_pengelola', $data, ['token_pengelola' => $id]);
    }
    public function delete($id)
    {
        $tables = array('token_pengelola');
        $this->db->where('token_pengelola', $id);
        $this->db->delete($tables);
    }
}
