<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_token_teknisi extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('token_teknisi', $data);
    }
    public function read()
    {
        $this->db->join('teknisi', 'teknisi.id_teknisi = token_teknisi.id_teknisi');
        return $this->db->get('token_teknisi');
    }
    public function read_where($array)
    {
        $this->db->join('teknisi', 'teknisi.id_teknisi = token_teknisi.id_teknisi');
        return $this->db->get_where('token_teknisi', $array);
    }
    public function read_pagination($limit, $start)
    {
        $this->db->join('teknisi', 'teknisi.id_teknisi = token_teknisi.id_teknisi');
        return $this->db->get('token_teknisi', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->join('teknisi', 'teknisi.id_teknisi = token_teknisi.id_teknisi');
        $this->db->like($array);
        return $this->db->get('token_teknisi');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->join('teknisi', 'teknisi.id_teknisi = token_teknisi.id_teknisi');
        $this->db->like($array);
        return $this->db->get('token_teknisi', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('token_teknisi', $data, ['token_teknisi' => $id]);
    }
    public function delete($id)
    {
        $tables = array('token_teknisi');
        $this->db->where('token_teknisi', $id);
        $this->db->delete($tables);
    }
}
