<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_user extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('user', $data);
    }
    public function read()
    {
        return $this->db->get('user');
    }
    public function read_where($array)
    {
        return $this->db->get_where('user', $array);
    }
    public function read_pagination($limit, $start)
    {
        return $this->db->get('user', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->like($array);
        return $this->db->get('user');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->like($array);
        return $this->db->get('user', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('user', $data, ['email_user' => $id]);
    }
    public function delete($id)
    {
        $tables = array('user');
        $this->db->where('email_user', $id);
        $this->db->delete($tables);
    }
}
