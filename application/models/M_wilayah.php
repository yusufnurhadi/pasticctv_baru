<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_wilayah extends CI_Model
{

    public function create($data)
    {
        $this->db->insert('wilayah', $data);
    }
    public function read()
    {
        return $this->db->get('wilayah');
    }
    public function read_where($array)
    {
        return $this->db->get_where('wilayah', $array);
    }
    public function read_pagination($limit, $start)
    {
        return $this->db->get('wilayah', $limit, $start);
    }
    public function read_like($array)
    {
        $this->db->like($array);
        return $this->db->get('wilayah');
    }
    public function read_like_pagination($array, $limit, $start)
    {
        $this->db->like($array);
        return $this->db->get('wilayah', $limit, $start);
    }
    public function update($id, $data)
    {
        $this->db->update('wilayah', $data, ['kd_wilayah' => $id]);
    }
    public function delete($id)
    {
        $tables = array('wilayah');
        $this->db->where('kd_wilayah', $id);
        $this->db->delete($tables);
    }
}
