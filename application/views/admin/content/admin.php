<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Administrator</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>">Home</a></li>
                        <li class="breadcrumb-item active">Administrator</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="row mb-3">
            <div class="col-md">
                <a href="#" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal-tambah">Tambah Data</a>
                <a href="<?= base_url('admin/cetak/') ?>" target="_blank" class="btn btn-primary btn-sm">Print Data to PDF</a>
                <a href="<?= base_url('admin/unduh/') ?>" target="_blank" class="btn btn-success btn-sm">Export Data to Excel</a>
            </div>
        </div>

        <!-- Default box -->
        <div class="card">
            <div class="card-header" style="overflow-x: auto;">
                <div class="row">
                    <div class="col-6">
                        <form action="<?= base_url('/admin') ?>" method="post">
                            <div class="input-group input-group-sm" style="width: 200px;">
                                <input type="text" name="keyword" class="form-control" placeholder="Search ..." autocomplete="off" autofocus="" value="<?= $this->session->userdata('key_admin') ?>">
                                <select name="change" class="form-control">
                                    <option value="nama_admin" <?php if($this->session->userdata('change_admin')=='nama_admin') echo "selected" ?>>Nama</option>
                                    <option value="admin.email_user" <?php if($this->session->userdata('change_admin')=='admin.email_user') echo "selected" ?>>Email</option>
                                </select>
                                <div class="input-group-append">
                                    <button type="submit" name="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-6 text-right">
                        <a href="<?= base_url('admin/refresh') ?>" class="btn btn-secondary" title="Refresh">
                            <i class="fas fa-history"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body" style="overflow-x: auto;">
                <table class="table table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th>No</th>
                            <th nowrap>Nama</th>
                            <th nowrap>Email</th>
                            <th nowrap>Foto</th>
                            <th nowrap>Status</th>
                            <th nowrap>Time Created</th>
                            <th nowrap>Time Updated</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (empty($admin)) : ?>

                            <tr>
                                <td colspan="8" class="text-center">Tidak ada data</td>
                            </tr>

                        <?php endif; ?>
                        <?php foreach ($admin as $key) :?>
                        <tr>
                            <td> <?= ++$start; ?> </td> 
                            <td nowrap> <?= $key ['nama_admin'] ?> </td> 
                            <td nowrap> <?= $key ['email_user'] ?> </td> 
                            <td nowrap>
                                <a href="#" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#modal-profile-<?= $key['id_admin'] ?>" title="Lihat Foto Profile">Lihat</a>
                            </td> 
                            <td nowrap>
                                <?php if ($key ['is_active_user'] == 1) { ?>
                                    <a class="badge bg-success"> Active </a>
                                <?php } elseif ($key ['is_active_user'] == 0) { ?>
                                    <a class="badge bg-primary"> Registered </a>
                                <?php } else { ?> 
                                    <a class="badge bg-danger"> Block </a>
                                <?php } ?> 
                            </td> 
                            <td nowrap> <?= $key ['created_admin'] ?> </td> 
                            <td nowrap> <?= $key ['updated_admin'] ?> </td> 
                            <td nowrap>
                                <a href="<?= base_url('admin/reset/' . $key['id_admin']) ?>" class="btn btn-xs btn-secondary" title="Reset Password" onclick="return confirm('Apakah anda yakin ingin mereset password menjadi 1234 ?')">Reset Password</a>
                                <a href="#" class="btn btn-xs btn-warning" data-toggle="modal" data-target="#modal-ubah-<?= $key['id_admin'] ?>" title="Ubah">Ubah</a>

                                <?php if ($key['is_active_user']==1) { ?>

                                    <a href="<?= base_url('admin/blokir/' . $key['id_admin']) ?>" class="btn btn-xs btn-danger" title="Blokir" onclick="return confirm('Apakah anda yakin ingin memblokir ?')">Blokir</a>
                                
                                <?php } else { ?>

                                    <a href="<?= base_url('admin/aktif/' . $key['id_admin']) ?>" class="btn btn-xs btn-success" title="Aktif" onclick="return confirm('Apakah anda yakin ingin mengaktifkan ?')">Aktif</a>

                                <?php } ?>

                                <a href="<?= base_url('admin/hapus/' . $key['id_admin']) ?>" class="btn btn-xs btn-danger" title="Hapus" onclick="return confirm('Apakah anda yakin ingin menghapus ?')">Hapus</a>
                            </td> 
                        </tr>
                        <!-- Modal Edit -->
                        <div class="modal fade" data-backdrop="static" id="modal-ubah-<?= $key['id_admin'] ?>">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Ubah Data Administrator</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <?= form_open_multipart('admin/ubah/' . $key['id_admin']); ?>
                                        <div class="form-group">
                                            <label>Nama *</label>
                                            <input type="text" name="nama" value="<?= $key['nama_admin'] ?>" 
                                            class="form-control" required>
                                            
                                        </div>
                                        <div class="form-group">
                                            <label>Email *</label>
                                            <input type="text" name="email" value="<?= $key['email_user'] ?>" class="form-control" placeholder="Email" readonly>
                                        </div>

                                        <div class="form-group">
                                        <label>Foto</label><br>
                                        <img style="max-height: 100px; max-width: 100px;" src="<?= base_url('img/profile/' . $key['foto_admin']) ?>">
                                        <input type="file" name="foto" class="form-control">
                                        </div>
                                        
                                        <div class="modal-footer justify-content-between">
                                            <input type="submit" value="Simpan" class="btn btn-primary form-control">
                                        </div>
                                        <?= form_close(); ?>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->

                        <!-- Modal Foto -->
                        <div class="modal fade" data-backdrop="static" id="modal-profile-<?= $key['id_admin'] ?>">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title"><?= $key['nama_admin'] ?></h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <center><img src="<?= base_url('img/profile/'.$key['foto_admin']) ?>" style="max-height: 200px; max-width: 200px;"></center>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                    
                        <?php endforeach;?>
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
            <div class="card-footer clearfix">
                Tampil <?= count($admin); ?> dari <?= $total_rows; ?> data
                <?= $this->pagination->create_links(); ?>
            </div>
        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div class="modal fade" data-backdrop="static" id="modal-tambah">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Data Administrator</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= form_open_multipart('admin/tambah'); ?>
                <div class="form-group">
                    <label>Nama *</label>
                    <input type="text" name="nama" class="form-control" placeholder="Nama Admin" required>
                </div>
                <div class="form-group">
                    <label>Email *</label>
                    <input type="email" name="email" class="form-control" placeholder="Email User "required>
                </div>
                <div class="form-group">
                    <label>Foto</label>
                    <input type="file" name="foto" class="form-control" placeholder="Foto">
                </div>
                <div class="modal-footer justify-content-between">
                    <input type="submit" value="Simpan" class="btn btn-primary form-control">
                </div>
                <?= form_close(); ?>
            </div>
        <!-- <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save</button>
      </div> -->
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>