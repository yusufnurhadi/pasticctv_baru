<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0"><?= $tittle_atm ?></h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>">Home</a></li>
                        <li class="breadcrumb-item active"><?= $tittle_atm ?></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="row mb-3">
            <div class="col-md">

                <a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-tambah">Tambah Data</a>
                <a href="<?= base_url('atm/cetak/') ?>" target="_blank" class="btn btn-primary btn-sm">Print Data to PDF</a>
                <a href="<?= base_url('atm/unduh/') ?>" target="_blank" class="btn btn-success btn-sm">Export Data to Excel</a>
            </div>
        </div>

        <!-- Default box -->
        <div class="card">
            <div class="card-body" style="overflow-x: auto;">
                <table class="table table-bordered" id="example1">
                    <thead class="thead-dark">
                        <tr>
                            <th nowrap>No</th>
                            <th nowrap>ID ATM</th>
                            <th nowrap>Lokasi</th>
                            <th nowrap>Alamat</th>
                            <th nowrap>Wilayah</th>
                            <th nowrap>Cabang</th>
                            <th nowrap>Pengelola</th>
                            <th nowrap>Merk DVR</th>
                            <th nowrap>Merk Kamera</th>
                            <th nowrap>SN DVR</th>
                            <th nowrap>SN Kamera</th>
                            <th nowrap>SN HDD</th>
                            <th nowrap>Last Problem</th>
                            <th nowrap>Time Created</th>
                            <th nowrap>Time Updated</th>
                            <th nowrap>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        $no=1;
                        foreach ($atm as $key) : ?>

                            <tr>
                                <td><?= $no++; ?></td>
                                <td nowrap><?= $key['id_atm'] ?></td>
                                <td nowrap><?= $key['lokasi_atm'] ?></td>
                                <td nowrap><?= $key['alamat_atm'] ?></td>
                                <td nowrap><?= $key['kd_wilayah'] ?></td>
                                <td nowrap><?= $key['nama_atm_cabang'] ?></td>
                                <td nowrap><?= $key['nama_atm_pengelola'] ?></td>
                                <td nowrap><?= $key['merk_dvr_atm'] ?></td>
                                <td nowrap><?= $key['merk_dome_atm'] ?></td>
                                <td nowrap><?= $key['sn_dvr_atm'] ?></td>
                                <td nowrap><?= $key['sn_dome_atm'] ?></td>
                                <td nowrap><?= $key['sn_hdd_atm'] ?></td>
                                <td nowrap><?= $key['date_problem_atm'] ?></td>
                                <td nowrap><?= $key['created_atm'] ?></td>
                                <td nowrap><?= $key['updated_atm'] ?></td>
                                <td nowrap>
                                    <a href="<?= base_url('atm/detail/'.$key['id_atm']) ?>" class="btn btn-xs btn-primary" title="Detail">Detail</a>
                                    <a href="#" class="btn btn-xs btn-warning" data-toggle="modal" data-target="#modal-ubah-<?= $key['id_atm'] ?>" title="Ubah">Ubah</a>
                                    <a href="<?= base_url('atm/hapus/' . $key['id_atm']) ?>" class="btn btn-xs btn-danger" title="Hapus" onclick="return confirm('Apakah anda yakin ingin menghapus ?')">Hapus</a>
                                </td>
                            </tr>

                            <!-- Modal Edit -->
                            <div class="modal fade" data-backdrop="static" id="modal-ubah-<?= $key['id_atm'] ?>">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Ubah Data ATM</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <?= form_open_multipart('atm/ubah/' . $key['id_atm']); ?>
                                            <div class="form-group">
                                                <label>ID ATM *</label>
                                                <input type="text" name="id_atm" value="<?= $key['id_atm'] ?>" class="form-control" placeholder="ID ATM" required readonly>
                                            </div>
                                            <div class="form-group">
                                                <label>Lokasi *</label>
                                                <input type="text" name="lokasi" value="<?= $key['lokasi_atm'] ?>" class="form-control" placeholder="Lokasi" required>
                                            </div>
                                            <div class="form-group">
                                                <label>Alamat *</label>
                                                <input type="text" name="alamat" value="<?= $key['alamat_atm'] ?>" class="form-control" placeholder="Alamat" required>
                                            </div>
                                            <div class="form-group">
                                                <label>Cabang *</label>
                                                <select name="cabang" class="form-control" required>
                                                    <option value="">-- Pilih Cabang --</option>
                                                    <?php foreach($cabang as $key) : ?>

                                                        <option value="">-- Pilih Cabang --</option>

                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Status *</label>
                                                <select name="status" class="form-control">
                                                    <option value="1" <?php if($key['is_active_user']==1) echo "selected" ?> >Aktif</option>
                                                    <option value="0" <?php if($key['is_active_user']==0) echo "selected" ?> >Non Aktif</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Notifikasi *</label>
                                                <select name="notif" class="form-control">
                                                    <option value="1" <?php if($key['notif_user']==1) echo "selected" ?> >Aktif</option>
                                                    <option value="0" <?php if($key['notif_user']==0) echo "selected" ?> >Non Aktif</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Notifikasi Telegram *</label>
                                                <select name="notif_tele" class="form-control">
                                                    <option value="1" <?php if($key['notif_tele_user']==1) echo "selected" ?> >Aktif</option>
                                                    <option value="0" <?php if($key['notif_tele_user']==0) echo "selected" ?> >Non Aktif</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Notifikasi Email *</label>
                                                <select name="notif_email" class="form-control">
                                                    <option value="1" <?php if($key['notif_email_user']==1) echo "selected" ?> >Aktif</option>
                                                    <option value="0" <?php if($key['notif_email_user']==0) echo "selected" ?> >Non Aktif</option>
                                                </select>
                                            </div>
                                            <div class="modal-footer justify-content-between">
                                                <input type="submit" value="Simpan" class="btn btn-primary form-control">
                                            </div>
                                            <?= form_close(); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.modal -->

                        <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div class="modal fade" data-backdrop="static" id="modal-tambah">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- <div class="overlay d-flex justify-content-center align-items-center"> -->
            <!-- <i class="fas fa-2x fa-sync fa-spin"></i> -->
            <!-- </div> -->
            <div class="modal-header">
                <h4 class="modal-title">Tambah Data Super Admin</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= form_open_multipart('admin/tambah'); ?>
                <div class="form-group">
                    <label>Nama *</label>
                    <input type="text" name="nama" class="form-control" placeholder="Nama" required>
                </div>
                <div class="form-group">
                    <label>Email *</label>
                    <input type="email" name="email" class="form-control" placeholder="Email" required>
                </div>
                <div class="form-group">
                    <label>Password *</label>
                    <input type="password" name="password" class="form-control" placeholder="Password" required>
                </div>
                <div class="form-group">
                    <label>No Telp *</label>
                    <input type="text" name="no" class="form-control" placeholder="No Telp" required>
                </div>
                <div class="form-group">
                    <label>Status *</label>
                    <select name="status" class="form-control" required>
                        <option value="1">Aktif</option>
                        <option value="0">Non Aktif</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Notifikasi *</label>
                    <select name="notif" class="form-control" required>
                        <option value="1">Aktif</option>
                        <option value="0">Non Aktif</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Notif Telegram *</label>
                    <select name="notif_tele" class="form-control" required>
                        <option value="1">Aktif</option>
                        <option value="0">Non Aktif</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Notif Email *</label>
                    <select name="notif_email" class="form-control" required>
                        <option value="1">Aktif</option>
                        <option value="0">Non Aktif</option>
                    </select>
                </div>
                <div class="modal-footer justify-content-between">
                    <input type="submit" value="Simpan" class="btn btn-primary form-control">
                </div>
                <?= form_close(); ?>
            </div>
            <!-- <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save</button>
      </div> -->
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>