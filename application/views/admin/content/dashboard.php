<?php

  function card($warna, $angka, $text, $icon, $url) {
    echo "<div class='col-12 col-sm-6 col-md-3'>";
    echo "<a href='".base_url($url)."'>";
    echo "<div class='info-box' style='background-color:rgba(255,255,255,0.5); border-radius: 10px;'>";
    echo "<span class='info-box-icon bg-$warna elevation-1'><i class='fas fa-$icon'></i></span>";
    echo "<div class='info-box-content'>";
    echo "<span class='info-box-text text-dark text-bold'>$text</span>";
    echo "<span class='info-box-number'>";
    echo "<div class='text-dark text-bold'>$angka</div>";
    // echo "<small>%</small>";
    echo "</span>";
    echo "</div>";
    echo "</div>";
    echo "</a>";
    echo "</div>";
  }
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Dashboard</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">Home</li>
          </ol>
        </div><!-- /.col -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
    <!-- Info boxes -->
      <div class="row">

        <?php

          card('info', $count_wilayah, 'Wilayah', 'university', 'wilayah/');
          card('info', $count_wilayah, 'Cabang', 'university', 'cabang_atm/');
          card('info', $count_wilayah, 'Pengelola', 'university', 'pengelola_atm/');

        ?>

      </div>
      <div class="row">

        <?php

          foreach ($wilayah as $key) :
            
            card('secondary', $count_atm_wilayah[$key['kd_wilayah']], 'ATM '.$key['kd_wilayah'], 'book', 'atm/index/'.$key['kd_wilayah']);

          endforeach;

        ?>

      </div>

      <div class="row mb-2">
        <div class="col-sm-6">
          <h5 class="m-0">Corrective Maintenance</h5>
        </div><!-- /.col -->
      </div>

      <div class="row">

        <?php

          card('primary', 0, 'New Ticket', 'ticket-alt', 'ticket_cm/new');
          card('secondary', 0, 'Process Ticket', 'ticket-alt', 'ticket_cm/process');
          card('warning', 0, 'Pending Ticket', 'ticket-alt', 'ticket_cm/pending');
          card('danger', 0, 'Cancel Ticket', 'ticket-alt', 'ticket_cm/cancel');
          // card('danger', 0, 'Pinalty Ticket', 'ticket-alt', 'ticket_cm/pinalty');

        ?>

      </div>

      <div class="row mb-2">
        <div class="col-sm-6">
          <h5 class="m-0">Preventive Maintenance</h5>
        </div><!-- /.col -->
      </div>

      <div class="row">

        <?php

          card('primary', 0, 'New Ticket', 'ticket-alt', 'ticket_pm/new');
          card('secondary', 0, 'Process Ticket', 'ticket-alt', 'ticket_pm/process');

        ?>

      </div>

      <div class="row mb-2">
        <div class="col-sm-6">
          <h5 class="m-0">History Ticket</h5>
        </div><!-- /.col -->
      </div>

      <div class="row">

        <?php

          card('primary', 0, 'Finish This Month', 'ticket-alt', 'ticket/finish');
          card('secondary', 0, 'History', 'ticket-alt', 'ticket/history');

        ?>

      </div>

      <div class="row">
        <div class="col-md-12">
          <!-- AREA CHART -->
          <div class="card card-secondary">
            <div class="card-header">
              <h3 class="card-title">Tickets Annual Chart</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
              </div>
            </div>
            <div class="card-body">
              <div class="chart">
                <canvas id="areaChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <!-- AREA CHART -->
          <div class="card card-secondary">
            <div class="card-header">
              <h3 class="card-title">SLA Annual Chart (Hour)</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
              </div>
            </div>
            <div class="card-body">
              <div class="chart">
                <canvas id="areaChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>

    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->