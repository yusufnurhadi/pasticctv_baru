<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Log Activity</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>">Home</a></li>
                        <li class="breadcrumb-item active">Log Activity</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="row mb-3">
            <div class="col-md">
                <a href="#" class="btn btn-success btn-sm" data-toggle="modal" data-target="#modal-export">Export Data to Excel</a>
            </div>
        </div>

        <!-- Default box -->
        <div class="card">
            <div class="card-header" style="overflow-x: auto;">
                <div class="row">
                    <div class="col-6">
                        <form action="<?= base_url('/log') ?>" method="post">
                            <div class="input-group input-group-sm" style="width: 200px;">
                                <input type="text" name="keyword" class="form-control" placeholder="Search ..." autocomplete="off" autofocus="" value="<?= $this->session->userdata('key_log') ?>">
                                <select name="change" class="form-control">
                                    <option value="nama_user" <?php if($this->session->userdata('change_log')=='nama_user') echo "selected" ?>>Nama</option>
                                    <option value="activity_log" <?php if($this->session->userdata('change_log')=='activity_log') echo "selected" ?>>Aktifitas</option>
                                    <option value="created_log" <?php if($this->session->userdata('change_log')=='created_log') echo "selected" ?>>Time Created</option>
                                </select>
                                <div class="input-group-append">
                                    <button type="submit" name="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-6 text-right">
                        <a href="<?= base_url('log/refresh') ?>" class="btn btn-secondary" title="Refresh">
                            <i class="fas fa-history"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body" style="overflow-x: auto;">
                <table class="table table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th>No</th>
                            <th nowrap>Nama User</th>
                            <th nowrap>Aktifitas</th>
                            <th nowrap>Lokasi</th>
                            <th nowrap>Time Created</th>
                            <th nowrap>Time Updated</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (empty($log)) : ?>

                            <tr>
                                <td colspan="6" class="text-center">Tidak ada data</td>
                            </tr>

                        <?php endif; ?>
                        <?php foreach ($log as $key) :?>
                        <tr>
                            <td> <?= ++$start; ?> </td> 
                            <td nowrap> <?= $key ['nama_user'] ?> </td> 
                            <td nowrap> <?= $key ['activity_log'] ?> </td> 
                            <td nowrap>
                                <?php

                                if ($key['lat_log'] == "0.000000" || $key['lng_log'] == "0.000000") {
                                    echo "Unknow";
                                } else {
                                    echo "<a href='".base_url('maps_lokasi/'.$key['lat_log'].'/'.$key['lng_log'].'/'.$key ['nama_user'].' '.$key ['activity_log'].' '.$key ['created_log'])."' target='_blank' class='badge bg-primary'>Lihat Lokasi</a>";
                                }

                                ?>
                            </td>
                            <td nowrap> <?= $key ['created_log'] ?> </td> 
                            <td nowrap> <?= $key ['updated_log'] ?> </td> 
                        </tr>
                        <!-- Modal Edit -->
                        <div class="modal fade" data-backdrop="static" id="modal-ubah-<?= $key['id_log'] ?>">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Ubah Data logistrator</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <?= form_open_multipart('log/ubah/' . $key['id_log']); ?>
                                            <div class="form-group">
                                                <label>Nama *</label>
                                                <input type="text" name="nama" value="<?= $key['nama_log'] ?>" 
                                                class="form-control" required>
                                                
                                            </div>
                                            <div class="form-group">
                                                <label>Email *</label>
                                                <input type="text" name="email" value="<?= $key['email_user'] ?>" class="form-control" placeholder="Email" readonly>
                                            </div>

                                            <div class="form-group">
                                            <label>Foto</label><br>
                                            <img style="max-height: 100px; max-width: 100px;" src="<?= base_url('img/profile/' . $key['foto_log']) ?>">
                                            <input type="file" name="foto" class="form-control">
                                            </div>
                                            
                                            <div class="modal-footer justify-content-between">
                                                <input type="submit" value="Simpan" class="btn btn-primary form-control">
                                            </div>
                                            <?= form_close(); ?>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                        <?php endforeach;?>
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
            <div class="card-footer clearfix">
                Tampil <?= count($log); ?> dari <?= $total_rows; ?> data
                <?= $this->pagination->create_links(); ?>
            </div>
        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div class="modal fade" data-backdrop="static" id="modal-export">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Export Data Log Activity</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= form_open_multipart('log/data_unduh/', ['target' => '_blank']); ?>
                <div class="form-group">
                    <label>Date From *</label>
                    <input type="date" name="from" class="form-control" placeholder="Date From" required>
                </div>
                <div class="form-group">
                    <label>Date To *</label>
                    <input type="date" name="to" class="form-control" placeholder="Date To" required>
                </div>
                <div class="modal-footer justify-content-between">
                    <input type="submit" value="Simpan" class="btn btn-primary form-control">
                </div>
                <?= form_close(); ?>
            </div>
        <!-- <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save</button>
      </div> -->
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>