<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Pengelola ATM</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>">Home</a></li>
                        <li class="breadcrumb-item active">Pengelola ATM</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="row mb-3">
            <div class="col-md">
                <a href="#" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal-tambah">Tambah Data</a>
                <a href="<?= base_url('pengelola_atm/cetak/') ?>" target="_blank" class="btn btn-primary btn-sm">Print Data to PDF</a>
                <a href="<?= base_url('pengelola_atm/unduh/') ?>" target="_blank" class="btn btn-success btn-sm">Export Data to Excel</a>
            </div>
        </div>

        <!-- Default box -->
        <div class="card">
            <div class="card-header" style="overflow-x: auto;">
                <div class="row">
                    <div class="col-6">
                        <form action="<?= base_url('/pengelola_atm') ?>" method="post">
                            <div class="input-group input-group-sm" style="width: 200px;">
                                <input type="text" name="keyword" class="form-control" placeholder="Search By Name Here" autocomplete="off" autofocus="">
                                <div class="input-group-append">
                                    <button type="submit" name="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-6 text-right">
                        <a href="<?= base_url('pengelola_atm/refresh') ?>" class="btn btn-secondary" title="Refresh">
                            <i class="fas fa-history"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body" style="overflow-x: auto;">
                <table class="table table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th>No</th>
                            <th nowrap>Kode ATM Pengelola</th>
                            <th nowrap>Wilayah</th>
                            <th nowrap>ATM Pengelola</th>
                            <th nowrap>Time Created</th>
                            <th nowrap>Time Updated</th>
                            <th>Aksi</th>
                        
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (empty($pengelola_atm)) : ?>

                            <tr>
                                <td colspan="7" class="text-center">Tidak ada data</td>
                            </tr>

                        <?php endif; ?>
                        <?php foreach ($pengelola_atm as $key) :?>
                        <tr>
                        <td> <?= ++$start; ?> </td> 
                        
                        <td nowrap> <?= $key ['kd_atm_pengelola'] ?> </td> 
                        <td nowrap> <?= $key ['kd_wilayah'] ?> </td> 
                        <td nowrap> <?= $key ['nama_atm_pengelola'] ?> </td> 
                        <td nowrap> <?= $key ['created_atm_pengelola'] ?> </td> 
                        <td nowrap> <?= $key ['updated_atm_pengelola'] ?> </td>                                
                        <td nowrap>
                       <a href="#" class="btn btn-xs btn-warning" data-toggle="modal" data-target="#modal-ubah-<?= $key['kd_atm_pengelola'] ?>" title="Ubah">Ubah</a>

                        <a href="<?= base_url('pengelola_atm/hapus/' . $key['kd_atm_pengelola']) ?>" class="btn btn-xs btn-danger" title="Hapus" onclick="return confirm('Apakah anda yakin ingin menghapus ?')">Hapus</a>
                            
                        </td> 
                        </tr>
                       <!-- Modal Edit -->
                       <div class="modal fade" data-backdrop="static" id="modal-ubah-<?= $key['kd_atm_pengelola'] ?>">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Ubah Data Pengelola ATM</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <?= form_open_multipart('pengelola_atm/ubah/' . $key['kd_atm_pengelola']); ?>
                                            
                                        <div class="form-group">
                                        <label>Kode ATM Pengelola *</label>
                                        <input type="text" name="kode" value="<?= $key['kd_atm_pengelola'] ?>" 
                                        class="form-control" required>
                                        </div>                                       								
                                        
                                        <div class="form-group">
                                            <label>Wilayah *</label>
                                            <select class="form-control" name="wilayah" placeholder="Kode Wilayah" required>
                                                <option value="">-- Pilih Wilayah --</option>
                                            <?php foreach ($wilayah as $w) : ?>
                                                <option value="<?= $w['kd_wilayah'] ?>"<?php if($key['kd_wilayah']==$w['kd_wilayah']) echo "selected"; ?> ><?= $w['nama_wilayah'] ?></option>
                                            <?php endforeach; ?>
                                            </select>
                                        </div>
                
                                             <div class="form-group">
                                                <label>Nama ATM Pengelola *</label>
                                                <input type="text" name="nama" value="<?= $key['nama_atm_pengelola'] ?>" class="form-control" placeholder="Nama Client" required>
                                            </div>
                                            <div class="modal-footer justify-content-between">
                                                <input type="submit" value="Simpan" class="btn btn-primary form-control">
                                            </div>
                                            <?= form_close(); ?>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
<?php endforeach;?>
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
            <div class="card-footer clearfix">
                Tampil <?= count($pengelola_atm); ?> dari <?= $total_rows; ?> data
                <?= $this->pagination->create_links(); ?>
            </div>
        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div class="modal fade" data-backdrop="static" id="modal-tambah">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Data Pengelola ATM</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= form_open_multipart('pengelola_atm/tambah'); ?>
                <div class="form-group">
               <label>Kode ATM Pengelola *</label>
               <input type="text" name="kode" class="form-control" placeholder="Nama ATM Pengelola" required>
                </div>

                <div class="form-group">
		        <label>Wilayah *</label>
				<select class="form-control" name="wilayah" placeholder="Kode Wilayah" required>
				    <option value="">-- Pilih Wilayah --</option>
                <?php foreach ($wilayah as $key) : ?>
				    <option value="<?= $key['kd_wilayah'] ?>"><?= $key['nama_wilayah'] ?></option>
                <?php endforeach; ?>
				</select>
				</div>

                <div class="form-group">
                    <label>Nama ATM Pengelola *</label>
                    <input type="text" name="nama" class="form-control" placeholder="Nama ATM Pengelola" required>
                </div>
                            
                <div class="modal-footer justify-content-between">
                    <input type="submit" value="Simpan" class="btn btn-primary form-control">
                </div>
                <?= form_close(); ?>
            </div>
            <!-- <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save</button>
      </div> -->
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>