<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Wilayah</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>">Home</a></li>
                        <li class="breadcrumb-item active">Wilayah</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="row mb-3">
            <div class="col-md">
                <a href="#" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal-tambah">Tambah Data</a>
                <a href="<?= base_url('wilayah/cetak/') ?>" target="_blank" class="btn btn-primary btn-sm">Print Data to PDF</a>
                <a href="<?= base_url('wilayah/unduh/') ?>" target="_blank" class="btn btn-success btn-sm">Export Data to Excel</a>
            </div>
        </div>

        <!-- Default box -->
        <div class="card">
            <div class="card-header" style="overflow-x: auto;">
                <div class="row">
                    <div class="col-6">
                        <form action="<?= base_url('/wilayah') ?>" method="post">
                            <div class="input-group input-group-sm" style="width: 200px;">
                                <input type="text" name="keyword" class="form-control" placeholder="Search ..." autocomplete="off" autofocus="" value="<?= $this->session->userdata('key_wilayah') ?>">
                                <select name="change" class="form-control">
                                    <option value="kd_wilayah" <?php if($this->session->userdata('change_wilayah')=='kd_wilayah') echo "selected" ?>>Kode</option>
                                    <option value="nama_wilayah" <?php if($this->session->userdata('change_wilayah')=='nama_wilayah') echo "selected" ?>>Nama</option>
                                </select>
                                <div class="input-group-append">
                                    <button type="submit" name="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-6 text-right">
                        <a href="<?= base_url('wilayah/refresh') ?>" class="btn btn-secondary" title="Refresh">
                            <i class="fas fa-history"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body" style="overflow-x: auto;">
                <table class="table table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th>No</th>
                            <th nowrap>Kode Wilayah</th>
                            <th nowrap>Nama Wilayah</th>
                            <th nowrap>Time Created</th>
                            <th nowrap>Time Updated</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php if (empty($wilayah_cms)) : ?>

                            <tr>
                                <td colspan="6" class="text-center">Tidak ada data</td>
                            </tr>

                        <?php endif; ?>

                        <?php foreach ($wilayah_cms as $key) : ?>

                            <tr>
                                <td><?= ++$start; ?></td>
                                <td nowrap><?= $key['kd_wilayah'] ?></td>
                                <td nowrap><?= $key['nama_wilayah'] ?></td>
                                <td nowrap><?= $key['created_wilayah'] ?></td>
                                <td nowrap><?= $key['updated_wilayah'] ?></td>
                                <td nowrap>
                                    <a href="#" class="btn btn-xs btn-warning" data-toggle="modal" data-target="#modal-ubah-<?= $key['kd_wilayah'] ?>" title="Ubah">Ubah</a>
                                    <a href="<?= base_url('wilayah/hapus/' . $key['kd_wilayah']) ?>" class="btn btn-xs btn-danger" title="Hapus" onclick="return confirm('Apakah anda yakin ingin menghapus ?')">Hapus</a>
                                </td>
                            </tr>

                            <!-- Modal Edit -->
                            <div class="modal fade" data-backdrop="static" id="modal-ubah-<?= $key['kd_wilayah'] ?>">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Ubah Data Wilayah</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <?= form_open_multipart('wilayah/ubah/' . $key['kd_wilayah']); ?>
                                            <div class="form-group">
                                                <label>Kode Wilayah *</label>
                                                <input type="text" name="kode" value="<?= $key['kd_wilayah'] ?>" class="form-control" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label>Nama Wilayah *</label>
                                                <input type="text" name="nama" value="<?= $key['nama_wilayah'] ?>" class="form-control" placeholder="Nama Wilayah" required>
                                            </div>
                                            <div class="modal-footer justify-content-between">
                                                <input type="submit" value="Simpan" class="btn btn-primary form-control">
                                            </div>
                                            <?= form_close(); ?>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->

                        <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
            <div class="card-footer clearfix">
                Tampil <?= count($wilayah_cms); ?> dari <?= $total_rows; ?> data
                <?= $this->pagination->create_links(); ?>
            </div>
        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div class="modal fade" data-backdrop="static" id="modal-tambah">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Data Wilayah</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= form_open_multipart('wilayah/tambah'); ?>
                <div class="form-group">
                    <label>Kode Wilayah *</label>
                    <input type="text" name="kode" class="form-control" placeholder="Kode Wilayah" required>
                </div>
                <div class="form-group">
                    <label>Nama Wilayah *</label>
                    <input type="text" name="nama" class="form-control" placeholder="Nama Wilayah" required>
                </div>
                <div class="modal-footer justify-content-between">
                    <input type="submit" value="Simpan" class="btn btn-primary form-control">
                </div>
                <?= form_close(); ?>
            </div>
            <!-- <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save</button>
      </div> -->
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>