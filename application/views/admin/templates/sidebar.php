<?php 
$wilayah = $this->load->m_wilayah->read()->result_array();
// if ($halaman == "ticket") $halaman1 = $halaman.'_'.$tittle;
if ($halaman == "atm") $halaman_atm = $tittle_atm;

function nav_item($text, $icon, $halaman, $page, $url) {

    if (isset($halaman1)) {
        $halaman=$halaman1;
    }

    $hasil = '';

    if (isset($halaman))
        if ($halaman == $page)
            $hasil = 'active';
   
    echo "<li class='nav-item'>";
    echo "<a href='".base_url($url)."' class='nav-link $hasil'>";
    echo "<i class='nav-icon fas fa-$icon'></i>";
    echo "<p>$text</p>";
    echo "</a>";
    echo "</li>";

}

function nav_treeview($text, $icon, $halaman, $array) {

    $hasil = '';
    $hasil2 = '';

    for ($i=0; $i < count($array); $i++) {

        if (isset($halaman))
            if ($halaman == $array[$i]['halaman'])
            {
                $hasil = 'active';
                $hasil2 = 'menu-open';
            }
    }

    echo "<li class='nav-item has-treeview $hasil2'>";
    echo "<a href='#' class='nav-link $hasil'>";
    echo "<i class='nav-icon fas fa-$icon'></i>";
    echo "<p>";
    echo "$text";
    echo "<i class='right fas fa-angle-left'></i>";
    echo "</p>";
    echo "</a>";
    echo "<ul class='nav nav-treeview'>";

    for ($i=0; $i < count($array); $i++) {

        if (isset($array[$i]['array'])) {

            $hasil = '';
            $hasil2 = '';

            if (isset($halaman))
                if ($halaman == $array[$i]['halaman']) {
                    $hasil = 'active';
                    $hasil2 = 'menu-open';
                }

            echo "<li class='nav-item has-treeview $hasil2'>";
            echo "<a href='#' class='nav-link $hasil'>";
            echo "<i class='far fa-circle nav-icon'></i>";
            echo "<p>";
            echo $array[$i]['kd'];
            echo "<i class='right fas fa-angle-left'></i>";
            echo "</p>";
            echo "</a>";
            echo "<ul class='nav nav-treeview'>";

            for ($j=0; $j < count($array[$i]['array']); $j++) {

                $hasil = '';
                $hasil2 = '';

                if (isset($halaman)) {
                    if ($halaman == $array[$i]['array'][$j]['halaman']) {
                        $hasil = 'active';
                        $hasil2 = 'menu-open';
                    }
                }

                echo "<li class='nav-item'>";
                echo "<a href='".base_url($array[$i]['array'][$j]['url'])."' class='nav-link $hasil'>";
                echo "<i class='far fa-dot-circle nav-icon'></i>";
                echo "<p>".$array[$i]['array'][$j]['kd']."</p>";
                echo "</a>";
                echo "</li>";

            }

            echo "</ul>";
            echo "</li>";

        } else {

            $hasil = '';

            if (isset($halaman))
                if ($halaman == $array[$i]['halaman'])
                    $hasil = 'active';

            echo "<li class='nav-item'>";
            echo "<a href='".base_url($array[$i]['url'])."' class='nav-link $hasil'>";
            echo "<i class='far fa-circle nav-icon'></i>";
            echo "<p>".$array[$i]['kd']."</p>";
            echo "</a>";
            echo "</li>";

        }

    }

    echo "</ul>";
    echo "</li>";

}

$array_atm = [];
$array_harga = [];
foreach ($wilayah as $key) {
    $array_atm[]= [
        'kd' => $key['kd_wilayah'],
        'url' => 'atm/index/'.$key['kd_wilayah'],
        'halaman' => 'ATM '.$key['kd_wilayah'],
    ];
    $array_harga[]= [
        'kd' => $key['kd_wilayah'],
        'url' => 'harga/index/'.$key['kd_wilayah'],
        'halaman' => 'harga',
    ];
}

?>
<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
        <img src="<?= base_url('img/logo/cctv-kecil.png') ?>" alt="Pasti CCTV Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <!-- <img src="<?= base_url('assets/') ?>dist/img/AdminLTELogo.png"> -->
        <span class="brand-text font-weight-light"><img src="<?= base_url('img/logo/cctv-besar.png') ?>" alt="Pasti Kinclong Logo" style="max-width: 150px;"></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-4 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="<?= base_url('img/profile/' . $this->session->userdata('foto_admin')) ?>" class="img-circle elevation-2" alt="<?= $this->session->userdata('nama_admin'); ?>">
            </div>
            <div class="info">
                <a class="d-block"><?= $this->session->userdata('nama_admin'); ?></a>
            </div>
        </div>


        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column nav-child-indent nav-compact" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                <?php

                    nav_item('Dashboard', 'tachometer-alt', $halaman, 'dashboard', 'dashboard/');
                    // nav_item('Performance', 'tachometer-alt', $halaman, 'performance', 'performance/');

                ?>
                <li class="nav-header">MASTER DATA</li>
                <?php

                    nav_item('WILAYAH', 'university', $halaman, 'wilayah', 'wilayah/');

                    nav_treeview('USERS', 'users', $halaman, [

                        [
                            'kd' => 'Administrator',
                            'url' => 'admin/',
                            'halaman' => 'admin',
                        ],
                        [
                            'kd' => 'Koordinator Wilayah',
                            'url' => 'koordinator/',
                            'halaman' => 'koordinator',
                        ],
                        [
                            'kd' => 'Client',
                            'url' => 'client/',
                            'halaman' => 'client',
                        ],
                        [
                            'kd' => 'Manager Area',
                            'url' => 'manager_area/',
                            'halaman' => 'manager_area',
                        ],
                        [
                            'kd' => 'Cabang',
                            'url' => 'cabang/',
                            'halaman' => 'cabang',
                        ],
                        [
                            'kd' => 'Pengelola',
                            'url' => 'pengelola/',
                            'halaman' => 'pengelola',
                        ],
                        [
                            'kd' => 'Teknisi',
                            'url' => 'teknisi/',
                            'halaman' => 'teknisi',
                        ],

                    ]);

                    nav_treeview('ATM', 'book', $halaman, [

                        [
                            'kd' => 'Cabang ATM',
                            'url' => 'cabang_atm/',
                            'halaman' => 'cabang_atm',
                        ],
                        [
                            'kd' => 'Pengelola ATM',
                            'url' => 'pengelola_atm/',
                            'halaman' => 'pengelola_atm',
                        ],
                        [
                            'kd' => 'ATM',
                            'url' => 'atm/',
                            'halaman' => 'atm',
                            'array' => $array_atm,
                        ],

                    ]);

                    nav_item('PART', 'toolbox', $halaman, 'part', 'part/');

                    nav_treeview('HARGA', 'dollar-sign', $halaman, $array_harga);

                ?>

                <li class="nav-header">CORRECTIVE MAINTENANCE</li>

                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-paper-plane"></i>
                        <p>New</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-wrench"></i>
                        <p>On Process</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-clock"></i>
                        <p>Pending</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-times-circle"></i>
                        <p>Cancel</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-hourglass-half"></i>
                        <p>Waiting Close</p>
                    </a>
                </li>
                <li class="nav-header">PREVENTIVE MAINTENANCE</li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-paper-plane"></i>
                        <p>New</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-wrench"></i>
                        <p>On Process</p>
                    </a>
                </li>

                <li class="nav-header">HISTORY TICKET</li>

                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-check-circle"></i>
                        <p>Finish</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?= base_url('project/HISTORY') ?>" class="nav-link">
                        <i class="nav-icon fas fa-history"></i>
                        <p>History</p>
                    </a>
                </li>
                <li class="nav-header">REPORT</li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-circle"></i>
                        <p>Monitoring Problem</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-circle"></i>
                        <p>Rekapitulasi Tagihan</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-circle"></i>
                        <p>Penggunaan Part</p>
                    </a>
                </li>

                <li class="nav-header">SETTINGS</li>

                <?php

                    nav_item('Log Tracking', 'map-marked-alt', $halaman, 'tracking', 'tracking/');
                    nav_item('Log Activity', 'cogs', $halaman, 'log', 'log/');

                    nav_treeview('FCM Token', 'cogs', $halaman, [

                        [
                            'kd' => 'Koordinator Wilayah',
                            'url' => 'fcm_token/koordinator/',
                            'halaman' => 'fcm_token_koordinator',
                        ],
                        [
                            'kd' => 'Client',
                            'url' => 'fcm_token/client/',
                            'halaman' => 'fcm_token_client',
                        ],
                        [
                            'kd' => 'Manager Area',
                            'url' => 'fcm_token/manager_area/',
                            'halaman' => 'fcm_token_manager_area',
                        ],
                        [
                            'kd' => 'Cabang',
                            'url' => 'fcm_token/cabang/',
                            'halaman' => 'fcm_token_cabang',
                        ],
                        [
                            'kd' => 'Pengelola',
                            'url' => 'fcm_token/pengelola/',
                            'halaman' => 'fcm_token_pengelola',
                        ],
                        [
                            'kd' => 'Teknisi',
                            'url' => 'fcm_token/teknisi/',
                            'halaman' => 'fcm_token_teknisi',
                        ],

                    ]);

                    nav_treeview('Telegram Token', 'paper-plane', $halaman, [

                        [
                            'kd' => 'Monitoring',
                            'url' => 'telegram_token/monitoring/',
                            'halaman' => 'telegram_token_monitoring',
                        ],
                        [
                            'kd' => 'Wilayah',
                            'url' => 'telegram_token/wilayah/',
                            'halaman' => 'telegram_token_wilayah',
                        ],

                    ]);

                ?>

            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>