<?php

if ($this->session->userdata('email_user')) {

  if ($this->session->userdata('level_user')=="1") {

    include 'admin/templates/header.php';
    include 'admin/templates/sidebar.php';
    if (isset($halaman)) {
      include 'admin/content/' . $halaman . '.php';
    } else {
      include 'admin/content/dashboard.php';
    }
    include 'admin/templates/footer.php';
  
  } elseif ($this->session->userdata('level_user')=="2") {

    include 'koordinator/templates/header.php';
    include 'koordinator/templates/sidebar.php';
    if (isset($halaman)) {
      include 'koordinator/content/' . $halaman . '.php';
    } else {
      include 'koordinator/content/dashboard.php';
    }
    include 'koordinator/templates/footer.php';

  } elseif ($this->session->userdata('level_user')=="3") {

    include 'client/templates/header.php';
    include 'client/templates/sidebar.php';
    if (isset($halaman)) {
      include 'client/content/' . $halaman . '.php';
    } else {
      include 'client/content/dashboard.php';
    }
    include 'client/templates/footer.php';

  } elseif ($this->session->userdata('level_user')=="4") {

    include 'manager_area/templates/header.php';
    include 'manager_area/templates/sidebar.php';
    if (isset($halaman)) {
      include 'manager_area/content/' . $halaman . '.php';
    } else {
      include 'manager_area/content/dashboard.php';
    }
    include 'manager_area/templates/footer.php';

  } elseif ($this->session->userdata('level_user')=="5") {

    include 'cabang/templates/header.php';
    include 'cabang/templates/sidebar.php';
    if (isset($halaman)) {
      include 'cabang/content/' . $halaman . '.php';
    } else {
      include 'cabang/content/dashboard.php';
    }
    include 'cabang/templates/footer.php';

  } elseif ($this->session->userdata('level_user')=="6") {

    include 'pengelola/templates/header.php';
    include 'pengelola/templates/sidebar.php';
    if (isset($halaman)) {
      include 'pengelola/content/' . $halaman . '.php';
    } else {
      include 'pengelola/content/dashboard.php';
    }
    include 'pengelola/templates/footer.php';

  } elseif ($this->session->userdata('level_user')=="7") {

    include 'teknisi/templates/header.php';
    include 'teknisi/templates/sidebar.php';
    if (isset($halaman)) {
      include 'teknisi/content/' . $halaman . '.php';
    } else {
      include 'teknisi/content/dashboard.php';
    }
    include 'teknisi/templates/footer.php';

  } else {

    $this->session->set_flashdata('error', 'Anda harus login dahulu ');
    redirect();
    die();
  
  }

} else {

  $this->session->set_flashdata('error', 'Anda harus login dahulu ');
  redirect();
  die();

}
