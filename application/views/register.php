<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="icon" type="image/png" href="<?= base_url() ?>/img/logo/cctv-kecil.png">
  <title>Register | Pasti CCTV v.2.0.</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url('assets/') ?>plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="<?= base_url('assets/') ?>plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?= base_url('assets/') ?>plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="<?= base_url('assets/') ?>plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url('assets/') ?>dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?= base_url('assets/') ?>plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?= base_url('assets/') ?>plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="<?= base_url('assets/') ?>plugins/summernote/summernote-bs4.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- pace-progress -->
  <link rel="stylesheet" href="<?= base_url('assets/') ?>plugins/pace-progress/themes/black/pace-theme-flat-top.css">
  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="<?= base_url('assets/') ?>plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
</head>

<body class="hold-transition login-page pace-primary" style="background-size: cover;
                                                            background-repeat: no-repeat;
                                                            background-attachment: fixed;
                                                            background-image: url(<?= base_url('img/bg/bg1.jpg') ?>);
                                                            transition: 1.5s linear;
                                                            -moz-transition: 1.5s linear;
                                                            background-position: center;">
  <div class="login-box">
    <div class="login-logo">
      <!-- <h1 class="text-danger"><b>Pasti</b>Kinclong</h1> -->
      <!-- <img src="<?= base_url('img/logo/kinclong.png') ?>" style="max-width: 300px;"><br> -->
    </div>
    <!-- /.login-logo -->
    <div class="card" style="background-color:rgba(255,255,255,0.7); border-radius: 10px;">
      <div class="card-body login-card-body" style="background: transparent;">
        <img src="<?= base_url('img/logo/cctv.png') ?>" style="max-width: 300px;">
        <p class="login-box-msg">Versi 2.0.</p>

        <form action="<?= base_url('register') ?>" method="post">
          <div class="mt-3">
            <?= form_error('nama', '<small class="text-danger">', '</small>'); ?>
            <div class="input-group">
              <input type="text" name="nama" class="form-control" placeholder="Full name" value="<?= set_value('nama') ?>" required>
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-user"></span>
                </div>
              </div>
            </div>
          </div>
          <div class="mt-3">
            <?= form_error('email', '<small class="text-danger">', '</small>'); ?>
            <div class="input-group">
              <input type="email" name="email" class="form-control" placeholder="Email" value="<?= set_value('email') ?>" required>
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-envelope"></span>
                </div>
              </div>
            </div>
          </div>
          <div class="mt-3">
            <?= form_error('password1', '<small class="text-danger">', '</small>'); ?>
            <div class="input-group">
              <input type="password" name="password1" class="form-control" placeholder="Password" required>
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-lock"></span>
                </div>
              </div>
            </div>
          </div>
          <div class="mt-3">
            <?= form_error('password2', '<small class="text-danger">', '</small>'); ?>
            <div class="input-group">
              <input type="password" name="password2" class="form-control" placeholder="Ulangi password" required>
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-lock"></span>
                </div>
              </div>
            </div>
          </div>
          <div class="mt-3">
            <?= form_error('akses', '<small class="text-danger">', '</small>'); ?>
            <div class="input-group">
              <select class="form-control" name="akses" required>
                <option value="">-- Pilih Hak Akses --</option>
                <option value="1">Administrator</option>
                <option value="2">Koordinator Wilayah</option>
                <option value="3">Client</option>
                <option value="4">Manager Area</option>
                <option value="5">Cabang</option>
                <option value="6">Pengelola</option>
                <option value="7">Teknisi</option>
              </select>
            </div>
          </div>
          <div class="mt-3">
            <?= form_error('wilayah', '<small class="text-danger">', '</small>'); ?>
            <div class="input-group">
              <select class="form-control" name="wilayah">
                <option value="">-- Pilih Wilayah --</option>
                <?php foreach ($wilayah as $key) : ?>
                  <option value="<?= $key['kd_wilayah'] ?>"><?= $key['nama_wilayah'] ?></option>
                <?php endforeach; ?>
              </select>
            </div>
            <p class="text-xs">* Hanya untuk Koordinator Wilayah, Client, Manager Area</p>
          </div>
          <div class="mt-3">
            <?= form_error('cabang', '<small class="text-danger">', '</small>'); ?>
            <div class="input-group">
              <select class="form-control" name="cabang">
                <option value="">-- Pilih Cabang --</option>
                <?php foreach ($cabang as $key) : ?>
                  <option value="<?= $key['kd_atm_cabang'] ?>"><?= $key['nama_atm_cabang'] ?></option>
                <?php endforeach; ?>
              </select>
            </div>
            <p class="text-xs">* Hanya untuk Cabang</p>
          </div>
          <div class="mt-3">
            <?= form_error('pengelola', '<small class="text-danger">', '</small>'); ?>
            <div class="input-group">
              <select class="form-control" name="pengelola">
                <option value="">-- Pilih Pengelola --</option>
                <?php foreach ($pengelola as $key) : ?>
                  <option value="<?= $key['kd_atm_pengelola'] ?>"><?= $key['nama_atm_pengelola'] ?></option>
                <?php endforeach; ?>
              </select>
            </div>
            <p class="text-xs">* Hanya untuk Pengelola</p>
          </div>
          <div class="row mt-3">
            <div class="col-8">
              <div class="icheck-primary">
                Sudah punya akun? <a href="<?= base_url() ?>" class="text-center">Masuk</a>
              </div>
            </div>
            <!-- /.col -->
            <div class="col-4">
              <input type="submit" name="submit" class="btn btn-primary btn-block" value="Daftar">
            </div>
            <!-- /.col -->
          </div>
        </form>

      </div>
      <!-- /.form-box -->
    </div><!-- /.card -->
  </div>
  <!-- /.register-box -->
  <!-- jQuery -->
  <script src="<?= base_url('assets/') ?>plugins/jquery/jquery.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="<?= base_url('assets/') ?>plugins/jquery-ui/jquery-ui.min.js"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
    $.widget.bridge('uibutton', $.ui.button)
  </script>
  <!-- Bootstrap 4 -->
  <script src="<?= base_url('assets/') ?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- ChartJS -->
  <script src="<?= base_url('assets/') ?>plugins/chart.js/Chart.min.js"></script>
  <!-- Sparkline -->
  <script src="<?= base_url('assets/') ?>plugins/sparklines/sparkline.js"></script>
  <!-- JQVMap -->
  <script src="<?= base_url('assets/') ?>plugins/jqvmap/jquery.vmap.min.js"></script>
  <script src="<?= base_url('assets/') ?>plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
  <!-- jQuery Knob Chart -->
  <script src="<?= base_url('assets/') ?>plugins/jquery-knob/jquery.knob.min.js"></script>
  <!-- daterangepicker -->
  <script src="<?= base_url('assets/') ?>plugins/moment/moment.min.js"></script>
  <script src="<?= base_url('assets/') ?>plugins/daterangepicker/daterangepicker.js"></script>
  <!-- Tempusdominus Bootstrap 4 -->
  <script src="<?= base_url('assets/') ?>plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
  <!-- Summernote -->
  <script src="<?= base_url('assets/') ?>plugins/summernote/summernote-bs4.min.js"></script>
  <!-- overlayScrollbars -->
  <script src="<?= base_url('assets/') ?>plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
  <!-- AdminLTE App -->
  <script src="<?= base_url('assets/') ?>dist/js/adminlte.js"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <script src="<?= base_url('assets/') ?>dist/js/pages/dashboard.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="<?= base_url('assets/') ?>dist/js/demo.js"></script>
  <!-- pace-progress -->
  <script src="<?= base_url('assets/') ?>plugins/pace-progress/pace.min.js"></script>
  <!-- SweetAlert2 -->
  <script src="<?= base_url('assets/') ?>plugins/sweetalert2/sweetalert2.min.js"></script>
  <?php if ($this->session->flashdata('success')) { ?>

    <script>
      Swal.fire(
        'Berhasil!',
        '<?= $this->session->flashdata('success') ?>',
        'success'
      )
    </script>

  <?php } elseif ($this->session->flashdata('error')) { ?>

    <script>
      Swal.fire(
        'Gagal!',
        '<?= $this->session->flashdata('error') ?>',
        'error'
      )
    </script>

  <?php } ?>
</body>

</html>