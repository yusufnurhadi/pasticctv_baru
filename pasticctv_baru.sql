-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 15, 2020 at 05:46 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pasticctv_baru`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `email_user` varchar(125) NOT NULL,
  `nama_admin` varchar(125) NOT NULL,
  `foto_admin` varchar(125) NOT NULL,
  `notif_admin` tinyint(1) NOT NULL,
  `created_admin` datetime NOT NULL,
  `updated_admin` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `email_user`, `nama_admin`, `foto_admin`, `notif_admin`, `created_admin`, `updated_admin`) VALUES
(1, 'yusufh4d1@gmail.com', 'Yusuf Nurhadi', 'default.jpg', 1, '2020-11-12 23:24:03', '0000-00-00 00:00:00'),
(2, 'admin@gmail.com', 'Admin', 'default.jpg', 1, '2020-11-13 17:32:10', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `atm`
--

CREATE TABLE `atm` (
  `id_atm` varchar(125) NOT NULL,
  `lokasi_atm` varchar(125) NOT NULL,
  `alamat_atm` text NOT NULL,
  `kd_atm_cabang` varchar(125) NOT NULL,
  `kd_atm_pengelola` varchar(125) NOT NULL,
  `lat_atm` float(10,6) NOT NULL,
  `lng_atm` float(10,6) NOT NULL,
  `merk_dvr_atm` varchar(100) NOT NULL,
  `merk_dome_atm` varchar(100) NOT NULL,
  `sn_dvr_atm` varchar(100) NOT NULL,
  `sn_dome_atm` varchar(100) NOT NULL,
  `sn_hdd_atm` varchar(100) NOT NULL,
  `date_problem_atm` date NOT NULL,
  `created_atm` datetime NOT NULL,
  `updated_atm` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `atm`
--

INSERT INTO `atm` (`id_atm`, `lokasi_atm`, `alamat_atm`, `kd_atm_cabang`, `kd_atm_pengelola`, `lat_atm`, `lng_atm`, `merk_dvr_atm`, `merk_dome_atm`, `sn_dvr_atm`, `sn_dome_atm`, `sn_hdd_atm`, `date_problem_atm`, `created_atm`, `updated_atm`) VALUES
('S1BJKT12DD', 'Taman Fatahilah Kota Tua', 'Jl. Kalibesar Timur IV Blok B No. 2, Tamansari, Jakarta Barat', 'JKT', 'SSIHMN', 0.000000, 0.000000, '', '', '', '', '', '0000-00-00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('S1BJKTR005', 'PT. Docotel Teknologi', 'Jl. KH. Hasyim Ashari No. 26, Petojo Utara, Gambir, Jakarta Pusat', 'JKT', 'SSIHMN', 0.000000, 0.000000, '', '', '', '', '', '0000-00-00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('S1DJKTA042', 'Mediterania Palace KMY', 'Jl. Landas Pacu Komp. Kemayoran Blok A1 Kav. 02, Jakarta Pusat', 'JKT', 'SSIHMN', 0.000000, 0.000000, '', '', '', '', '', '0000-00-00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('S1DJKTR002', 'Pulau Pramuka (Kep. Seribu)', 'Jl. Dermaga (Pulau Pramuka) Kep. Seribu', 'JKT', 'SSIHMN', 0.000000, 0.000000, '', '', '', '', '', '0000-00-00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('S1FJKT08NU', 'Apartemen Med. Boulevard Kemayoran', 'Jl. Landasan Pacu Timur, Kemayoran, Jakarta Pusat', 'JKT', 'SSIHMN', 0.000000, 0.000000, '', '', '', '', '', '0000-00-00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('S1FJKTR006', 'Kantor Dishub Pulau Tidung', 'Pulau Tidung Kepulauan Seribu', 'JKT', 'SSIHMN', 0.000000, 0.000000, '', '', '', '', '', '0000-00-00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('S1GJKT11Q0', 'BNI Syariah KCP Mangga Dua', 'Komp. Pertokoan Harco Mangga Dua, Jl. Mangga Dua Raya, Jakarta Pusat', 'JKT', 'SSIHMN', 0.000000, 0.000000, '', '', '', '', '', '0000-00-00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('S1HJKT10WS', 'Shelter Hydro Coco', 'Komplek TIJA, Jl. Lodan, Ancol, Jakarta Utara', 'JKT', 'SSIHMN', 0.000000, 0.000000, '', '', '', '', '', '0000-00-00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('S1HJKT10XF', 'Pantai Indah Ancol', 'Komplek TIJA, Jl. Lodan, Ancol, Jakarta Utara', 'JKT', 'SSIHMN', 0.000000, 0.000000, '', '', '', '', '', '0000-00-00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('S1HRMA03VI', 'ITC Mangga Dua Blok D', 'ITC Mangga Dua, Jl.  Mangga Dua Raya Jakarta Utara', 'JKT', 'SSIHMN', 0.000000, 0.000000, '', '', '', '', '', '0000-00-00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('S1HRMA11T7', 'Dunia Fantasi Ancol', 'Jl. Lodan Timur No. 7, Jakarta Utara', 'JKT', 'SSIHMN', 0.000000, 0.000000, '', '', '', '', '', '0000-00-00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('S1JJKTA019', 'Hotel Plaza Jayakarta', 'Jl. Hayam Wuruk 126, Jakarta 11180', 'JKT', 'SSIHMN', 0.000000, 0.000000, '', '', '', '', '', '0000-00-00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `atm_cabang`
--

CREATE TABLE `atm_cabang` (
  `kd_atm_cabang` varchar(125) NOT NULL,
  `kd_wilayah` varchar(125) NOT NULL,
  `nama_atm_cabang` varchar(125) NOT NULL,
  `created_atm_cabang` datetime NOT NULL,
  `updated_atm_cabang` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `atm_cabang`
--

INSERT INTO `atm_cabang` (`kd_atm_cabang`, `kd_wilayah`, `nama_atm_cabang`, `created_atm_cabang`, `updated_atm_cabang`) VALUES
('JKT', 'WJK', 'JAKARTA KOTA', '2020-11-12 23:26:34', '0000-00-00 00:00:00'),
('RMA', 'WJK', 'ROA MALAKA', '2020-11-12 23:26:46', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `atm_pengelola`
--

CREATE TABLE `atm_pengelola` (
  `kd_atm_pengelola` varchar(125) NOT NULL,
  `kd_wilayah` varchar(125) NOT NULL,
  `nama_atm_pengelola` varchar(125) NOT NULL,
  `created_atm_pengelola` datetime NOT NULL,
  `updated_atm_pengelola` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `atm_pengelola`
--

INSERT INTO `atm_pengelola` (`kd_atm_pengelola`, `kd_wilayah`, `nama_atm_pengelola`, `created_atm_pengelola`, `updated_atm_pengelola`) VALUES
('SSIHMN', 'WJK', 'SSI HARMONI', '2020-11-12 23:27:15', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cabang`
--

CREATE TABLE `cabang` (
  `id_cabang` int(11) NOT NULL,
  `email_user` varchar(125) NOT NULL,
  `kd_atm_cabang` varchar(125) NOT NULL,
  `nama_cabang` varchar(125) NOT NULL,
  `foto_cabang` varchar(125) NOT NULL,
  `notif_cabang` tinyint(1) NOT NULL,
  `created_cabang` datetime NOT NULL,
  `updated_cabang` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cabang`
--

INSERT INTO `cabang` (`id_cabang`, `email_user`, `kd_atm_cabang`, `nama_cabang`, `foto_cabang`, `notif_cabang`, `created_cabang`, `updated_cabang`) VALUES
(1, 'jkt@bni.co.id', 'JKT', 'BNI CABANG JAKARTA KOTA', 'default.jpg', 1, '2020-11-13 14:22:51', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `id_client` int(11) NOT NULL,
  `email_user` varchar(125) NOT NULL,
  `kd_wilayah` varchar(125) NOT NULL,
  `nama_client` varchar(125) NOT NULL,
  `foto_client` varchar(125) NOT NULL,
  `notif_client` tinyint(1) NOT NULL,
  `created_client` datetime NOT NULL,
  `updated_client` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`id_client`, `email_user`, `kd_wilayah`, `nama_client`, `foto_client`, `notif_client`, `created_client`, `updated_client`) VALUES
(1, 'wjk@bni.co.id', 'WJK', 'BNI WILAYAH JAKARTA KOTA', 'default.jpg', 1, '2020-11-12 23:25:06', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `koordinator`
--

CREATE TABLE `koordinator` (
  `id_koordinator` int(11) NOT NULL,
  `email_user` varchar(125) NOT NULL,
  `nama_koordinator` varchar(125) NOT NULL,
  `foto_koordinator` varchar(125) NOT NULL,
  `notif_koordinator` tinyint(1) NOT NULL,
  `created_koordinator` datetime NOT NULL,
  `updated_koordinator` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `koordinator`
--

INSERT INTO `koordinator` (`id_koordinator`, `email_user`, `nama_koordinator`, `foto_koordinator`, `notif_koordinator`, `created_koordinator`, `updated_koordinator`) VALUES
(1, 'koordinator@gmail.com', 'Koordinator Wilayah', 'profile-koordinator-20201128231226.jpeg', 1, '2020-11-28 10:23:45', '2020-11-28 23:12:26'),
(2, 'koordinator1@gmail.com', 'Koordinator Wilayah 1', 'default.jpg', 1, '2020-11-28 10:26:43', '2020-11-28 23:11:01'),
(3, 'koordinator2@gmail.com', 'Koordinator Wilayah 2', 'default.jpg', 1, '2020-11-28 22:55:01', '2020-11-28 23:11:24');

-- --------------------------------------------------------

--
-- Table structure for table `koordinator_wilayah`
--

CREATE TABLE `koordinator_wilayah` (
  `id_koordinator_wilayah` int(11) NOT NULL,
  `id_koordinator` int(11) NOT NULL,
  `kd_wilayah` varchar(125) NOT NULL,
  `created_koordinator_wilayah` datetime NOT NULL,
  `updated_koordinator_wilayah` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `koordinator_wilayah`
--

INSERT INTO `koordinator_wilayah` (`id_koordinator_wilayah`, `id_koordinator`, `kd_wilayah`, `created_koordinator_wilayah`, `updated_koordinator_wilayah`) VALUES
(40, 2, 'WJB', '2020-11-28 23:11:01', '0000-00-00 00:00:00'),
(41, 2, 'WJK', '2020-11-28 23:11:01', '0000-00-00 00:00:00'),
(42, 2, 'WJS', '2020-11-28 23:11:01', '0000-00-00 00:00:00'),
(43, 2, 'WJY', '2020-11-28 23:11:01', '0000-00-00 00:00:00'),
(44, 3, 'WSM', '2020-11-28 23:11:24', '0000-00-00 00:00:00'),
(45, 3, 'WYK', '2020-11-28 23:11:24', '0000-00-00 00:00:00'),
(46, 1, 'WBN', '2020-11-28 23:12:26', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `id_log` int(11) NOT NULL,
  `nama_user` varchar(100) NOT NULL,
  `activity_log` text NOT NULL,
  `lat_log` float(10,6) NOT NULL,
  `lng_log` float(10,6) NOT NULL,
  `created_log` datetime NOT NULL,
  `updated_log` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `log`
--

INSERT INTO `log` (`id_log`, `nama_user`, `activity_log`, `lat_log`, `lng_log`, `created_log`, `updated_log`) VALUES
(1, 'Unknow', 'Login Failed ; Tidak Terdaftar', 0.000000, 0.000000, '2020-11-27 15:47:10', '0000-00-00 00:00:00'),
(2, 'Unknow', 'Login Failed ; Akun Tidak Aktif', 0.000000, 0.000000, '2020-11-27 15:47:30', '0000-00-00 00:00:00'),
(3, 'Unknow', 'Login Failed ; Password Salah', 0.000000, 0.000000, '2020-11-27 15:47:38', '0000-00-00 00:00:00'),
(4, 'Yusuf Nurhadi', 'Login Success', 0.000000, 0.000000, '2020-11-27 15:47:47', '0000-00-00 00:00:00'),
(5, 'Yusuf Nurhadi', 'Login Success', 0.000000, 0.000000, '2020-11-27 20:08:44', '0000-00-00 00:00:00'),
(6, 'Yusuf Nurhadi', 'Page Dashboard', 0.000000, 0.000000, '2020-11-27 20:31:00', '0000-00-00 00:00:00'),
(7, 'Yusuf Nurhadi', 'Page Dashboard', 0.000000, 0.000000, '2020-11-27 20:31:22', '0000-00-00 00:00:00'),
(8, 'yusufh4d1@gmail.com', 'Page Wilayah', 0.000000, 0.000000, '2020-11-27 20:36:41', '0000-00-00 00:00:00'),
(9, 'yusufh4d1@gmail.com', 'Tambah Wilayah AA', 0.000000, 0.000000, '2020-11-27 20:36:53', '0000-00-00 00:00:00'),
(10, 'yusufh4d1@gmail.com', 'Page Wilayah', 0.000000, 0.000000, '2020-11-27 20:36:53', '0000-00-00 00:00:00'),
(11, 'yusufh4d1@gmail.com', 'Ubah Wilayah AA', 0.000000, 0.000000, '2020-11-27 20:37:02', '0000-00-00 00:00:00'),
(12, 'yusufh4d1@gmail.com', 'Page Wilayah', 0.000000, 0.000000, '2020-11-27 20:37:02', '0000-00-00 00:00:00'),
(13, 'yusufh4d1@gmail.com', 'Refresh Wilayah', 0.000000, 0.000000, '2020-11-27 20:37:16', '0000-00-00 00:00:00'),
(14, 'yusufh4d1@gmail.com', 'Page Wilayah', 0.000000, 0.000000, '2020-11-27 20:37:16', '0000-00-00 00:00:00'),
(15, 'yusufh4d1@gmail.com', 'Cetak Wilayah', 0.000000, 0.000000, '2020-11-27 20:37:18', '0000-00-00 00:00:00'),
(16, 'yusufh4d1@gmail.com', 'Unduh Wilayah', 0.000000, 0.000000, '2020-11-27 20:37:23', '0000-00-00 00:00:00'),
(17, 'yusufh4d1@gmail.com', 'Hapus Wilayah AA', 0.000000, 0.000000, '2020-11-27 20:37:28', '0000-00-00 00:00:00'),
(18, 'yusufh4d1@gmail.com', 'Page Wilayah', 0.000000, 0.000000, '2020-11-27 20:37:28', '0000-00-00 00:00:00'),
(19, 'yusufh4d1@gmail.com', 'Page Wilayah', 0.000000, 0.000000, '2020-11-27 20:38:08', '0000-00-00 00:00:00'),
(20, 'yusufh4d1@gmail.com', 'Page Admin', 0.000000, 0.000000, '2020-11-27 20:47:37', '0000-00-00 00:00:00'),
(21, 'yusufh4d1@gmail.com', 'Page Admin', 0.000000, 0.000000, '2020-11-27 20:47:40', '0000-00-00 00:00:00'),
(22, 'yusufh4d1@gmail.com', 'Refresh Admin', 0.000000, 0.000000, '2020-11-27 20:47:42', '0000-00-00 00:00:00'),
(23, 'yusufh4d1@gmail.com', 'Page Admin', 0.000000, 0.000000, '2020-11-27 20:47:42', '0000-00-00 00:00:00'),
(24, 'yusufh4d1@gmail.com', 'Tambah Admin aaaaa', 0.000000, 0.000000, '2020-11-27 20:47:54', '0000-00-00 00:00:00'),
(25, 'yusufh4d1@gmail.com', 'Page Admin', 0.000000, 0.000000, '2020-11-27 20:47:54', '0000-00-00 00:00:00'),
(26, 'yusufh4d1@gmail.com', 'Cetak Admin', 0.000000, 0.000000, '2020-11-27 20:47:58', '0000-00-00 00:00:00'),
(27, 'yusufh4d1@gmail.com', 'Unduh Admin', 0.000000, 0.000000, '2020-11-27 20:48:02', '0000-00-00 00:00:00'),
(28, 'yusufh4d1@gmail.com', 'Ubah Admin aaaaabbbbbbb', 0.000000, 0.000000, '2020-11-27 20:48:15', '0000-00-00 00:00:00'),
(29, 'yusufh4d1@gmail.com', 'Page Admin', 0.000000, 0.000000, '2020-11-27 20:48:15', '0000-00-00 00:00:00'),
(30, 'yusufh4d1@gmail.com', 'Hapus Admin 3', 0.000000, 0.000000, '2020-11-27 20:48:24', '0000-00-00 00:00:00'),
(31, 'yusufh4d1@gmail.com', 'Page Admin', 0.000000, 0.000000, '2020-11-27 20:48:25', '0000-00-00 00:00:00'),
(32, 'yusufh4d1@gmail.com', 'Page Wilayah', 0.000000, 0.000000, '2020-11-27 20:50:43', '0000-00-00 00:00:00'),
(33, 'yusufh4d1@gmail.com', 'Page Admin', 0.000000, 0.000000, '2020-11-27 20:50:46', '0000-00-00 00:00:00'),
(34, 'yusufh4d1@gmail.com', 'Page Admin', 0.000000, 0.000000, '2020-11-27 20:53:20', '0000-00-00 00:00:00'),
(35, 'yusufh4d1@gmail.com', 'Page Admin', 0.000000, 0.000000, '2020-11-27 20:53:40', '0000-00-00 00:00:00'),
(36, 'yusufh4d1@gmail.com', 'Page Admin', 0.000000, 0.000000, '2020-11-27 20:56:41', '0000-00-00 00:00:00'),
(37, 'yusufh4d1@gmail.com', 'Page Admin', 0.000000, 0.000000, '2020-11-27 20:56:49', '0000-00-00 00:00:00'),
(38, 'yusufh4d1@gmail.com', 'Page Admin', 0.000000, 0.000000, '2020-11-27 21:00:59', '0000-00-00 00:00:00'),
(39, 'yusufh4d1@gmail.com', 'Reset Admin Yusuf Nurhadi', 0.000000, 0.000000, '2020-11-27 21:03:38', '0000-00-00 00:00:00'),
(40, 'yusufh4d1@gmail.com', 'Page Admin', 0.000000, 0.000000, '2020-11-27 21:03:39', '0000-00-00 00:00:00'),
(41, 'yusufh4d1@gmail.com', 'Page Admin', 0.000000, 0.000000, '2020-11-27 21:05:10', '0000-00-00 00:00:00'),
(42, 'yusufh4d1@gmail.com', 'Blokir Admin Yusuf Nurhadi', 0.000000, 0.000000, '2020-11-27 21:05:31', '0000-00-00 00:00:00'),
(43, 'yusufh4d1@gmail.com', 'Page Admin', 0.000000, 0.000000, '2020-11-27 21:05:31', '0000-00-00 00:00:00'),
(44, 'yusufh4d1@gmail.com', 'Aktif Admin Yusuf Nurhadi', 0.000000, 0.000000, '2020-11-27 21:05:36', '0000-00-00 00:00:00'),
(45, 'yusufh4d1@gmail.com', 'Page Admin', 0.000000, 0.000000, '2020-11-27 21:05:37', '0000-00-00 00:00:00'),
(46, 'yusufh4d1@gmail.com', 'Aktif Admin Admin', 0.000000, 0.000000, '2020-11-27 21:05:41', '0000-00-00 00:00:00'),
(47, 'yusufh4d1@gmail.com', 'Page Admin', 0.000000, 0.000000, '2020-11-27 21:05:41', '0000-00-00 00:00:00'),
(48, 'Yusuf Nurhadi', 'Page Dashboard', 0.000000, 0.000000, '2020-11-27 21:06:17', '0000-00-00 00:00:00'),
(49, 'yusufh4d1@gmail.com', 'Page Admin', 0.000000, 0.000000, '2020-11-27 21:10:05', '0000-00-00 00:00:00'),
(50, 'yusufh4d1@gmail.com', 'Page Admin', 0.000000, 0.000000, '2020-11-27 21:16:09', '0000-00-00 00:00:00'),
(51, 'yusufh4d1@gmail.com', 'Page Admin', 0.000000, 0.000000, '2020-11-27 21:16:17', '0000-00-00 00:00:00'),
(52, 'yusufh4d1@gmail.com', 'Page Admin', 0.000000, 0.000000, '2020-11-27 21:16:48', '0000-00-00 00:00:00'),
(53, 'yusufh4d1@gmail.com', 'Page Admin', 0.000000, 0.000000, '2020-11-27 21:23:32', '0000-00-00 00:00:00'),
(54, 'Yusuf Nurhadi', 'Login Success', 0.000000, 0.000000, '2020-11-28 09:18:54', '0000-00-00 00:00:00'),
(55, 'Yusuf Nurhadi', 'Page Dashboard', 0.000000, 0.000000, '2020-11-28 09:18:54', '0000-00-00 00:00:00'),
(56, 'yusufh4d1@gmail.com', 'Page Admin', 0.000000, 0.000000, '2020-11-28 09:22:47', '0000-00-00 00:00:00'),
(57, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 09:53:32', '0000-00-00 00:00:00'),
(58, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 09:53:52', '0000-00-00 00:00:00'),
(59, 'Yusuf Nurhadi', 'Login Success', 0.000000, 0.000000, '2020-11-28 09:55:13', '0000-00-00 00:00:00'),
(60, 'Yusuf Nurhadi', 'Page Dashboard', 0.000000, 0.000000, '2020-11-28 09:55:13', '0000-00-00 00:00:00'),
(61, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 09:55:17', '0000-00-00 00:00:00'),
(62, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 09:55:53', '0000-00-00 00:00:00'),
(63, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 09:59:45', '0000-00-00 00:00:00'),
(64, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 09:59:57', '0000-00-00 00:00:00'),
(65, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 10:02:34', '0000-00-00 00:00:00'),
(66, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 10:03:26', '0000-00-00 00:00:00'),
(67, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 10:03:59', '0000-00-00 00:00:00'),
(68, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 10:21:38', '0000-00-00 00:00:00'),
(69, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 10:22:15', '0000-00-00 00:00:00'),
(70, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 10:22:22', '0000-00-00 00:00:00'),
(71, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 10:22:57', '0000-00-00 00:00:00'),
(72, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 10:23:20', '0000-00-00 00:00:00'),
(73, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 10:23:22', '0000-00-00 00:00:00'),
(74, 'yusufh4d1@gmail.com', 'Tambah Koordinator Wilayah Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 10:23:45', '0000-00-00 00:00:00'),
(75, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 10:23:45', '0000-00-00 00:00:00'),
(76, 'yusufh4d1@gmail.com', 'Refresh Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 10:24:01', '0000-00-00 00:00:00'),
(77, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 10:24:01', '0000-00-00 00:00:00'),
(78, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 10:24:18', '0000-00-00 00:00:00'),
(79, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 10:24:54', '0000-00-00 00:00:00'),
(80, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 10:25:28', '0000-00-00 00:00:00'),
(81, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 10:26:14', '0000-00-00 00:00:00'),
(82, 'yusufh4d1@gmail.com', 'Tambah Koordinator Wilayah Koordinator Wilayah 1', 0.000000, 0.000000, '2020-11-28 10:26:43', '0000-00-00 00:00:00'),
(83, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 10:26:43', '0000-00-00 00:00:00'),
(84, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 10:43:00', '0000-00-00 00:00:00'),
(85, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 11:00:36', '0000-00-00 00:00:00'),
(86, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 11:02:05', '0000-00-00 00:00:00'),
(87, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 11:02:27', '0000-00-00 00:00:00'),
(88, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 11:02:49', '0000-00-00 00:00:00'),
(89, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 11:05:02', '0000-00-00 00:00:00'),
(90, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 11:05:39', '0000-00-00 00:00:00'),
(91, 'Yusuf Nurhadi', 'Login Success', 0.000000, 0.000000, '2020-11-28 16:22:36', '0000-00-00 00:00:00'),
(92, 'Yusuf Nurhadi', 'Page Dashboard', 0.000000, 0.000000, '2020-11-28 16:22:36', '0000-00-00 00:00:00'),
(93, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 16:22:48', '0000-00-00 00:00:00'),
(94, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 16:23:42', '0000-00-00 00:00:00'),
(95, 'yusufh4d1@gmail.com', 'Ubah Koordinator Wilayah Koordinator Wilayah 1', 0.000000, 0.000000, '2020-11-28 16:26:57', '0000-00-00 00:00:00'),
(96, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 16:26:57', '0000-00-00 00:00:00'),
(97, 'yusufh4d1@gmail.com', 'Ubah Koordinator Wilayah Koordinator Wilayah 1', 0.000000, 0.000000, '2020-11-28 16:30:59', '0000-00-00 00:00:00'),
(98, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 16:30:59', '0000-00-00 00:00:00'),
(99, 'yusufh4d1@gmail.com', 'Ubah Koordinator Wilayah Koordinator Wilayah 1', 0.000000, 0.000000, '2020-11-28 16:31:14', '0000-00-00 00:00:00'),
(100, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 16:31:14', '0000-00-00 00:00:00'),
(101, 'yusufh4d1@gmail.com', 'Ubah Koordinator Wilayah Koordinator Wilayah 1', 0.000000, 0.000000, '2020-11-28 16:31:22', '0000-00-00 00:00:00'),
(102, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 16:31:22', '0000-00-00 00:00:00'),
(103, 'yusufh4d1@gmail.com', 'Ubah Koordinator Wilayah Koordinator Wilayah 1', 0.000000, 0.000000, '2020-11-28 16:31:45', '0000-00-00 00:00:00'),
(104, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 16:31:45', '0000-00-00 00:00:00'),
(105, 'yusufh4d1@gmail.com', 'Ubah Koordinator Wilayah Koordinator Wilayah 2', 0.000000, 0.000000, '2020-11-28 16:32:37', '0000-00-00 00:00:00'),
(106, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 16:32:37', '0000-00-00 00:00:00'),
(107, 'Yusuf Nurhadi', 'Login Success', 0.000000, 0.000000, '2020-11-28 22:54:00', '0000-00-00 00:00:00'),
(108, 'Yusuf Nurhadi', 'Page Dashboard', 0.000000, 0.000000, '2020-11-28 22:54:00', '0000-00-00 00:00:00'),
(109, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 22:54:05', '0000-00-00 00:00:00'),
(110, 'yusufh4d1@gmail.com', 'Tambah Koordinator Wilayah Koordinator Wilayah 2', 0.000000, 0.000000, '2020-11-28 22:55:01', '0000-00-00 00:00:00'),
(111, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 22:55:01', '0000-00-00 00:00:00'),
(112, 'yusufh4d1@gmail.com', 'Ubah Koordinator Wilayah Koordinator Wilayah 2', 0.000000, 0.000000, '2020-11-28 22:55:14', '0000-00-00 00:00:00'),
(113, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 22:55:14', '0000-00-00 00:00:00'),
(114, 'yusufh4d1@gmail.com', 'Ubah Koordinator Wilayah Koordinator Wilayah 2', 0.000000, 0.000000, '2020-11-28 22:57:36', '0000-00-00 00:00:00'),
(115, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 22:57:37', '0000-00-00 00:00:00'),
(116, 'yusufh4d1@gmail.com', 'Ubah Koordinator Wilayah Koordinator Wilayah 1', 0.000000, 0.000000, '2020-11-28 22:57:55', '0000-00-00 00:00:00'),
(117, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 22:57:55', '0000-00-00 00:00:00'),
(118, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 23:01:51', '0000-00-00 00:00:00'),
(119, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 23:01:53', '0000-00-00 00:00:00'),
(120, 'yusufh4d1@gmail.com', 'Ubah Koordinator Wilayah Koordinator Wilayah 1', 0.000000, 0.000000, '2020-11-28 23:04:07', '0000-00-00 00:00:00'),
(121, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 23:04:07', '0000-00-00 00:00:00'),
(122, 'yusufh4d1@gmail.com', 'Ubah Koordinator Wilayah Koordinator Wilayah 1', 0.000000, 0.000000, '2020-11-28 23:04:18', '0000-00-00 00:00:00'),
(123, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 23:04:18', '0000-00-00 00:00:00'),
(124, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 23:10:20', '0000-00-00 00:00:00'),
(125, 'yusufh4d1@gmail.com', 'Ubah Koordinator Wilayah Koordinator Wilayah 111', 0.000000, 0.000000, '2020-11-28 23:10:34', '0000-00-00 00:00:00'),
(126, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 23:10:34', '0000-00-00 00:00:00'),
(127, 'yusufh4d1@gmail.com', 'Ubah Koordinator Wilayah Koordinator Wilayah 2', 0.000000, 0.000000, '2020-11-28 23:10:42', '0000-00-00 00:00:00'),
(128, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 23:10:42', '0000-00-00 00:00:00'),
(129, 'yusufh4d1@gmail.com', 'Ubah Koordinator Wilayah Koordinator Wilayah 1', 0.000000, 0.000000, '2020-11-28 23:10:50', '0000-00-00 00:00:00'),
(130, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 23:10:50', '0000-00-00 00:00:00'),
(131, 'yusufh4d1@gmail.com', 'Ubah Koordinator Wilayah Koordinator Wilayah 1', 0.000000, 0.000000, '2020-11-28 23:11:01', '0000-00-00 00:00:00'),
(132, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 23:11:02', '0000-00-00 00:00:00'),
(133, 'yusufh4d1@gmail.com', 'Ubah Koordinator Wilayah Koordinator Wilayah 2', 0.000000, 0.000000, '2020-11-28 23:11:24', '0000-00-00 00:00:00'),
(134, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 23:11:24', '0000-00-00 00:00:00'),
(135, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 23:11:42', '0000-00-00 00:00:00'),
(136, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 23:12:01', '0000-00-00 00:00:00'),
(137, 'yusufh4d1@gmail.com', 'Ubah Koordinator Wilayah Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 23:12:26', '0000-00-00 00:00:00'),
(138, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-11-28 23:12:26', '0000-00-00 00:00:00'),
(139, 'Unknow', 'Login Failed ; Password Salah', 0.000000, 0.000000, '2020-11-29 09:34:02', '0000-00-00 00:00:00'),
(140, 'Yusuf Nurhadi', 'Login Success', 0.000000, 0.000000, '2020-11-29 09:34:11', '0000-00-00 00:00:00'),
(141, 'Yusuf Nurhadi', 'Page Dashboard', 0.000000, 0.000000, '2020-11-29 09:34:11', '0000-00-00 00:00:00'),
(142, 'yusufh4d1@gmail.com', 'Page Wilayah', 0.000000, 0.000000, '2020-11-29 10:37:31', '0000-00-00 00:00:00'),
(143, 'Unknow', 'Login Failed ; Password Salah', 0.000000, 0.000000, '2020-12-08 09:10:59', '0000-00-00 00:00:00'),
(144, 'Yusuf Nurhadi', 'Login Success', 0.000000, 0.000000, '2020-12-08 09:11:07', '0000-00-00 00:00:00'),
(145, 'Yusuf Nurhadi', 'Page Dashboard', 0.000000, 0.000000, '2020-12-08 09:11:07', '0000-00-00 00:00:00'),
(146, 'yusufh4d1@gmail.com', 'Page Wilayah', 0.000000, 0.000000, '2020-12-08 09:11:26', '0000-00-00 00:00:00'),
(147, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-12-08 09:11:32', '0000-00-00 00:00:00'),
(148, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-12-08 09:11:45', '0000-00-00 00:00:00'),
(149, 'yusufh4d1@gmail.com', 'Reset Koordinator Koordinator Wilayah', 0.000000, 0.000000, '2020-12-08 09:13:32', '0000-00-00 00:00:00'),
(150, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-12-08 09:13:32', '0000-00-00 00:00:00'),
(151, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-12-08 09:16:02', '0000-00-00 00:00:00'),
(152, 'yusufh4d1@gmail.com', 'Blokir Koordinator Koordinator Wilayah', 0.000000, 0.000000, '2020-12-08 09:16:08', '0000-00-00 00:00:00'),
(153, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-12-08 09:16:08', '0000-00-00 00:00:00'),
(154, 'yusufh4d1@gmail.com', 'Aktif Koordinator Koordinator Wilayah', 0.000000, 0.000000, '2020-12-08 09:16:14', '0000-00-00 00:00:00'),
(155, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-12-08 09:16:14', '0000-00-00 00:00:00'),
(156, 'yusufh4d1@gmail.com', 'Page Wilayah', 0.000000, 0.000000, '2020-12-08 09:16:25', '0000-00-00 00:00:00'),
(157, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-12-08 09:16:27', '0000-00-00 00:00:00'),
(158, 'yusufh4d1@gmail.com', 'Cetak Kordinator', 0.000000, 0.000000, '2020-12-08 09:18:36', '0000-00-00 00:00:00'),
(159, 'yusufh4d1@gmail.com', 'Unduh Koordinator', 0.000000, 0.000000, '2020-12-08 09:19:36', '0000-00-00 00:00:00'),
(160, 'yusufh4d1@gmail.com', 'Page Admin', 0.000000, 0.000000, '2020-12-08 09:20:21', '0000-00-00 00:00:00'),
(161, 'yusufh4d1@gmail.com', 'Page Admin', 0.000000, 0.000000, '2020-12-08 09:24:58', '0000-00-00 00:00:00'),
(162, 'yusufh4d1@gmail.com', 'Page Client', 0.000000, 0.000000, '2020-12-08 10:55:47', '0000-00-00 00:00:00'),
(163, 'yusufh4d1@gmail.com', 'Reset Client BNI WILAYAH JAKARTA KOTA', 0.000000, 0.000000, '2020-12-08 10:55:53', '0000-00-00 00:00:00'),
(164, 'yusufh4d1@gmail.com', 'Page Client', 0.000000, 0.000000, '2020-12-08 10:55:54', '0000-00-00 00:00:00'),
(165, 'yusufh4d1@gmail.com', 'Page Client', 0.000000, 0.000000, '2020-12-08 10:56:10', '0000-00-00 00:00:00'),
(166, 'yusufh4d1@gmail.com', 'Blokir Client aaaaaaaaaaaaaaaaaaaaaaaaaa', 0.000000, 0.000000, '2020-12-08 10:56:43', '0000-00-00 00:00:00'),
(167, 'yusufh4d1@gmail.com', 'Page Client', 0.000000, 0.000000, '2020-12-08 10:56:44', '0000-00-00 00:00:00'),
(168, 'yusufh4d1@gmail.com', 'Aktif Client aaaaaaaaaaaaaaaaaaaaaaaaaa', 0.000000, 0.000000, '2020-12-08 10:56:48', '0000-00-00 00:00:00'),
(169, 'yusufh4d1@gmail.com', 'Page Client', 0.000000, 0.000000, '2020-12-08 10:56:48', '0000-00-00 00:00:00'),
(170, 'yusufh4d1@gmail.com', 'Hapus Client', 0.000000, 0.000000, '2020-12-08 10:56:53', '0000-00-00 00:00:00'),
(171, 'yusufh4d1@gmail.com', 'Page Client', 0.000000, 0.000000, '2020-12-08 10:56:53', '0000-00-00 00:00:00'),
(172, 'yusufh4d1@gmail.com', 'Page Manager Area', 0.000000, 0.000000, '2020-12-08 11:14:02', '0000-00-00 00:00:00'),
(173, 'yusufh4d1@gmail.com', 'Page Manager Area', 0.000000, 0.000000, '2020-12-08 11:15:10', '0000-00-00 00:00:00'),
(174, 'yusufh4d1@gmail.com', 'Reset Manager Area Manager Area WJK', 0.000000, 0.000000, '2020-12-08 11:15:14', '0000-00-00 00:00:00'),
(175, 'yusufh4d1@gmail.com', 'Page Manager Area', 0.000000, 0.000000, '2020-12-08 11:15:15', '0000-00-00 00:00:00'),
(176, 'yusufh4d1@gmail.com', 'Blokir Manager Area Manager Area WJK', 0.000000, 0.000000, '2020-12-08 11:15:20', '0000-00-00 00:00:00'),
(177, 'yusufh4d1@gmail.com', 'Page Manager Area', 0.000000, 0.000000, '2020-12-08 11:15:20', '0000-00-00 00:00:00'),
(178, 'yusufh4d1@gmail.com', 'Aktif Manager Area Manager Area WJK', 0.000000, 0.000000, '2020-12-08 11:15:25', '0000-00-00 00:00:00'),
(179, 'yusufh4d1@gmail.com', 'Page Manager Area', 0.000000, 0.000000, '2020-12-08 11:15:25', '0000-00-00 00:00:00'),
(180, 'yusufh4d1@gmail.com', 'Page Manager Area', 0.000000, 0.000000, '2020-12-08 11:19:16', '0000-00-00 00:00:00'),
(181, 'yusufh4d1@gmail.com', 'Page Client', 0.000000, 0.000000, '2020-12-08 11:19:22', '0000-00-00 00:00:00'),
(182, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-12-08 11:19:25', '0000-00-00 00:00:00'),
(183, 'yusufh4d1@gmail.com', 'Page Client', 0.000000, 0.000000, '2020-12-08 11:19:28', '0000-00-00 00:00:00'),
(184, 'yusufh4d1@gmail.com', 'Page Manager Area', 0.000000, 0.000000, '2020-12-08 11:20:14', '0000-00-00 00:00:00'),
(185, 'yusufh4d1@gmail.com', 'Page Client', 0.000000, 0.000000, '2020-12-08 11:20:17', '0000-00-00 00:00:00'),
(186, 'yusufh4d1@gmail.com', 'Page Manager Area', 0.000000, 0.000000, '2020-12-08 11:31:26', '0000-00-00 00:00:00'),
(187, 'yusufh4d1@gmail.com', 'Page Cabang', 0.000000, 0.000000, '2020-12-08 11:46:09', '0000-00-00 00:00:00'),
(188, 'yusufh4d1@gmail.com', 'Reset Cabang BNI CABANG JAKARTA KOTA', 0.000000, 0.000000, '2020-12-08 11:46:13', '0000-00-00 00:00:00'),
(189, 'yusufh4d1@gmail.com', 'Page Cabang', 0.000000, 0.000000, '2020-12-08 11:46:13', '0000-00-00 00:00:00'),
(190, 'yusufh4d1@gmail.com', 'Blokir Cabang BNI CABANG JAKARTA KOTA', 0.000000, 0.000000, '2020-12-08 11:46:19', '0000-00-00 00:00:00'),
(191, 'yusufh4d1@gmail.com', 'Page Cabang', 0.000000, 0.000000, '2020-12-08 11:46:19', '0000-00-00 00:00:00'),
(192, 'yusufh4d1@gmail.com', 'Aktif Cabang BNI CABANG JAKARTA KOTA', 0.000000, 0.000000, '2020-12-08 11:46:24', '0000-00-00 00:00:00'),
(193, 'yusufh4d1@gmail.com', 'Page Cabang', 0.000000, 0.000000, '2020-12-08 11:46:24', '0000-00-00 00:00:00'),
(194, 'yusufh4d1@gmail.com', 'Page Cabang', 0.000000, 0.000000, '2020-12-08 11:46:55', '0000-00-00 00:00:00'),
(195, 'yusufh4d1@gmail.com', 'Page Manager Area', 0.000000, 0.000000, '2020-12-08 11:46:58', '0000-00-00 00:00:00'),
(196, 'yusufh4d1@gmail.com', 'Page Client', 0.000000, 0.000000, '2020-12-08 11:47:00', '0000-00-00 00:00:00'),
(197, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-12-08 11:47:02', '0000-00-00 00:00:00'),
(198, 'yusufh4d1@gmail.com', 'Page Admin', 0.000000, 0.000000, '2020-12-08 11:47:04', '0000-00-00 00:00:00'),
(199, 'Yusuf Nurhadi', 'Login Success', 0.000000, 0.000000, '2020-12-08 14:02:09', '0000-00-00 00:00:00'),
(200, 'Yusuf Nurhadi', 'Page Dashboard', 0.000000, 0.000000, '2020-12-08 14:02:09', '0000-00-00 00:00:00'),
(201, 'yusufh4d1@gmail.com', 'Page Wilayah', 0.000000, 0.000000, '2020-12-08 14:02:44', '0000-00-00 00:00:00'),
(202, 'yusufh4d1@gmail.com', 'Page Cabang', 0.000000, 0.000000, '2020-12-08 14:02:51', '0000-00-00 00:00:00'),
(203, 'yusufh4d1@gmail.com', 'Page Pengelola', 0.000000, 0.000000, '2020-12-08 14:33:15', '0000-00-00 00:00:00'),
(204, 'yusufh4d1@gmail.com', 'Reset Pengelola SSI HARMONI', 0.000000, 0.000000, '2020-12-08 14:33:20', '0000-00-00 00:00:00'),
(205, 'yusufh4d1@gmail.com', 'Page Pengelola', 0.000000, 0.000000, '2020-12-08 14:33:20', '0000-00-00 00:00:00'),
(206, 'yusufh4d1@gmail.com', 'Blokir Pengelola SSI HARMONI', 0.000000, 0.000000, '2020-12-08 14:33:25', '0000-00-00 00:00:00'),
(207, 'yusufh4d1@gmail.com', 'Page Pengelola', 0.000000, 0.000000, '2020-12-08 14:33:26', '0000-00-00 00:00:00'),
(208, 'yusufh4d1@gmail.com', 'Aktif Pengelola SSI HARMONI', 0.000000, 0.000000, '2020-12-08 14:33:30', '0000-00-00 00:00:00'),
(209, 'yusufh4d1@gmail.com', 'Page Pengelola', 0.000000, 0.000000, '2020-12-08 14:33:30', '0000-00-00 00:00:00'),
(210, 'yusufh4d1@gmail.com', 'Page Teknisi', 0.000000, 0.000000, '2020-12-08 15:00:55', '0000-00-00 00:00:00'),
(211, 'yusufh4d1@gmail.com', 'Reset Teknisi TEKNISI 1', 0.000000, 0.000000, '2020-12-08 15:01:01', '0000-00-00 00:00:00'),
(212, 'yusufh4d1@gmail.com', 'Page Teknisi', 0.000000, 0.000000, '2020-12-08 15:01:01', '0000-00-00 00:00:00'),
(213, 'yusufh4d1@gmail.com', 'Blokir Teknisi TEKNISI 1', 0.000000, 0.000000, '2020-12-08 15:01:06', '0000-00-00 00:00:00'),
(214, 'yusufh4d1@gmail.com', 'Page Teknisi', 0.000000, 0.000000, '2020-12-08 15:01:06', '0000-00-00 00:00:00'),
(215, 'yusufh4d1@gmail.com', 'Aktif Teknisi TEKNISI 1', 0.000000, 0.000000, '2020-12-08 15:01:10', '0000-00-00 00:00:00'),
(216, 'yusufh4d1@gmail.com', 'Page Teknisi', 0.000000, 0.000000, '2020-12-08 15:01:10', '0000-00-00 00:00:00'),
(217, 'yusufh4d1@gmail.com', 'Page Cabang ATM', 0.000000, 0.000000, '2020-12-08 15:19:38', '0000-00-00 00:00:00'),
(218, 'yusufh4d1@gmail.com', 'Page Pengelola ATM', 0.000000, 0.000000, '2020-12-08 15:22:20', '0000-00-00 00:00:00'),
(219, 'yusufh4d1@gmail.com', 'Page ATM WBN', 0.000000, 0.000000, '2020-12-08 15:23:27', '0000-00-00 00:00:00'),
(220, 'Yusuf Nurhadi', 'Page Dashboard', 0.000000, 0.000000, '2020-12-08 15:28:06', '0000-00-00 00:00:00'),
(221, 'yusufh4d1@gmail.com', 'Page ATM WJK', 0.000000, 0.000000, '2020-12-08 15:28:14', '0000-00-00 00:00:00'),
(222, 'yusufh4d1@gmail.com', 'Page Pengelola ATM', 0.000000, 0.000000, '2020-12-08 15:41:31', '0000-00-00 00:00:00'),
(223, 'yusufh4d1@gmail.com', 'Page Cabang ATM', 0.000000, 0.000000, '2020-12-08 15:41:33', '0000-00-00 00:00:00'),
(224, 'yusufh4d1@gmail.com', 'Page Admin', 0.000000, 0.000000, '2020-12-08 15:41:35', '0000-00-00 00:00:00'),
(225, 'yusufh4d1@gmail.com', 'Page Koordinator Wilayah', 0.000000, 0.000000, '2020-12-08 15:41:37', '0000-00-00 00:00:00'),
(226, 'yusufh4d1@gmail.com', 'Page ATM WBN', 0.000000, 0.000000, '2020-12-08 15:41:43', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `manager_area`
--

CREATE TABLE `manager_area` (
  `id_manager_area` int(11) NOT NULL,
  `email_user` varchar(125) NOT NULL,
  `kd_wilayah` varchar(125) NOT NULL,
  `nama_manager_area` varchar(125) NOT NULL,
  `foto_manager_area` varchar(125) NOT NULL,
  `notif_manager_area` tinyint(1) NOT NULL,
  `created_manager_area` datetime NOT NULL,
  `updated_manager_area` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `manager_area`
--

INSERT INTO `manager_area` (`id_manager_area`, `email_user`, `kd_wilayah`, `nama_manager_area`, `foto_manager_area`, `notif_manager_area`, `created_manager_area`, `updated_manager_area`) VALUES
(1, 'manager.wjk@bni.co.id', 'WJK', 'Manager Area WJK', 'default.jpg', 1, '2020-11-12 23:25:57', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `part`
--

CREATE TABLE `part` (
  `kd_part` varchar(125) NOT NULL,
  `nama_part` varchar(125) NOT NULL,
  `created_part` datetime NOT NULL,
  `updated_part` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `part_harga`
--

CREATE TABLE `part_harga` (
  `id_part_harga` int(11) NOT NULL,
  `kd_part` varchar(125) NOT NULL,
  `kd_wilayah` varchar(125) NOT NULL,
  `harga_part_harga` int(11) NOT NULL,
  `warranty_part_harga` tinyint(2) NOT NULL,
  `created_part_harga` datetime NOT NULL,
  `updated_part_harga` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pengelola`
--

CREATE TABLE `pengelola` (
  `id_pengelola` int(11) NOT NULL,
  `email_user` varchar(125) NOT NULL,
  `kd_atm_pengelola` varchar(125) NOT NULL,
  `nama_pengelola` varchar(125) NOT NULL,
  `foto_pengelola` varchar(125) NOT NULL,
  `notif_pengelola` tinyint(1) NOT NULL,
  `created_pengelola` datetime NOT NULL,
  `updated_pengelola` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pengelola`
--

INSERT INTO `pengelola` (`id_pengelola`, `email_user`, `kd_atm_pengelola`, `nama_pengelola`, `foto_pengelola`, `notif_pengelola`, `created_pengelola`, `updated_pengelola`) VALUES
(1, 'ssihmn@ssi.co.id', 'SSIHMN', 'SSI HARMONI', 'default.jpg', 1, '2020-11-13 14:23:20', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `snapshoot_cm`
--

CREATE TABLE `snapshoot_cm` (
  `id_snapshoot_cm` int(11) NOT NULL,
  `id_ticket_header` int(11) NOT NULL,
  `status_snapshoot_cm` tinyint(1) NOT NULL,
  `foto_id_snapshoot_cm` varchar(125) NOT NULL,
  `foto_lokasi_snapshoot_cm` varchar(125) NOT NULL,
  `foto_good_snapshoot_cm` varchar(125) NOT NULL,
  `foto_bad_snapshoot_cm` varchar(125) NOT NULL,
  `foto_sebelum_snapshoot_cm` varchar(125) NOT NULL,
  `foto_sesudah_snapshoot_cm` varchar(125) NOT NULL,
  `merk_dome_snapshoot_cm` varchar(125) NOT NULL,
  `merk_dvr_snapshoot_cm` varchar(125) NOT NULL,
  `created_snapshoot_cm` datetime NOT NULL,
  `updated_snapshoot_cm` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `snapshoot_pm`
--

CREATE TABLE `snapshoot_pm` (
  `id_snapshoot_pm` int(11) NOT NULL,
  `id_ticket_header` int(11) NOT NULL,
  `status_snapshoot_pm` tinyint(1) NOT NULL,
  `foto_id_snapshoot_pm` varchar(125) NOT NULL,
  `foto_lokasi_snapshoot_pm` varchar(125) NOT NULL,
  `foto_good_snapshoot_pm` varchar(125) NOT NULL,
  `foto_bad_snapshoot_pm` varchar(125) NOT NULL,
  `foto_sebelum_snapshoot_pm` varchar(125) NOT NULL,
  `foto_sesudah_snapshoot_pm` varchar(125) NOT NULL,
  `foto_sn_dvr_snapshoot_pm` varchar(125) NOT NULL,
  `foto_sn_camera_snapshoot_pm` varchar(125) NOT NULL,
  `merk_hdd_snapshoot_pm` varchar(125) NOT NULL,
  `merk_dome_snapshoot_pm` varchar(125) NOT NULL,
  `merk_dvr_snapshoot_pm` varchar(125) NOT NULL,
  `created_snapshoot_pm` datetime NOT NULL,
  `updated_snapshoot_pm` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `teknisi`
--

CREATE TABLE `teknisi` (
  `id_teknisi` int(11) NOT NULL,
  `email_user` varchar(125) NOT NULL,
  `nama_teknisi` varchar(125) NOT NULL,
  `foto_teknisi` varchar(125) NOT NULL,
  `notif_teknisi` tinyint(1) NOT NULL,
  `created_teknisi` datetime NOT NULL,
  `updated_teknisi` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `teknisi`
--

INSERT INTO `teknisi` (`id_teknisi`, `email_user`, `nama_teknisi`, `foto_teknisi`, `notif_teknisi`, `created_teknisi`, `updated_teknisi`) VALUES
(1, 'teknisi1@ptmdm.co.id', 'TEKNISI 1', 'default.jpg', 1, '2020-11-13 14:23:50', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `ticket_cancel`
--

CREATE TABLE `ticket_cancel` (
  `id_ticket_cancel` int(11) NOT NULL,
  `id_ticket_header` int(11) NOT NULL,
  `id_pengelola` int(11) NOT NULL,
  `alasan_ticket_cancel` text NOT NULL,
  `created_ticket_cancel` datetime NOT NULL,
  `updated_ticket_cancel` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ticket_detail`
--

CREATE TABLE `ticket_detail` (
  `id_ticket_detail` int(11) NOT NULL,
  `id_ticket_header` int(11) NOT NULL,
  `id_koordinator` int(11) NOT NULL,
  `id_teknisi` int(11) NOT NULL,
  `sla_ticket_detail` varchar(125) NOT NULL,
  `created_ticket_detail` datetime NOT NULL,
  `update_ticket_detail` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ticket_header`
--

CREATE TABLE `ticket_header` (
  `id_ticket_header` int(11) NOT NULL,
  `id_atm` varchar(125) NOT NULL,
  `id_pengelola` int(11) NOT NULL,
  `no_ticket_header` varchar(125) NOT NULL,
  `tgl_ticket_header` varchar(125) NOT NULL,
  `problem_ticket_header` varchar(125) NOT NULL,
  `type_ticket_header` enum('CM','PM','','') NOT NULL,
  `status_ticket_header` varchar(125) NOT NULL,
  `pelapor_ticket_header` varchar(125) NOT NULL,
  `no_pelapor_ticket_header` varchar(125) NOT NULL,
  `flm_ticket_header` varchar(125) NOT NULL,
  `no_flm_ticket_header` varchar(125) NOT NULL,
  `created_ticket_header` datetime NOT NULL,
  `updated_ticket_header` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ticket_part`
--

CREATE TABLE `ticket_part` (
  `id_ticket_part` int(11) NOT NULL,
  `id_ticket_header` int(11) NOT NULL,
  `kd_part` varchar(125) NOT NULL,
  `harga_ticket_part` int(11) NOT NULL,
  `warranty_ticket_part` tinyint(2) NOT NULL,
  `created_ticket_part` datetime NOT NULL,
  `updated_ticket_part` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ticket_pending`
--

CREATE TABLE `ticket_pending` (
  `id_ticket_pending` int(11) NOT NULL,
  `id_ticket_header` int(11) NOT NULL,
  `id_teknisi` int(11) NOT NULL,
  `alasan_ticket_pending` text NOT NULL,
  `created_ticket_pending` datetime NOT NULL,
  `updated_ticket_pending` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ticket_pinalty`
--

CREATE TABLE `ticket_pinalty` (
  `id_ticket_pinalty` int(11) NOT NULL,
  `id_ticket_header` int(11) NOT NULL,
  `harga_ticket_pinalty` int(11) NOT NULL,
  `jml_ticket_pinalty` int(11) NOT NULL,
  `created_ticket_pinalty` datetime NOT NULL,
  `updated_ticket_pinalty` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ticket_responded`
--

CREATE TABLE `ticket_responded` (
  `id_ticket_responded` int(11) NOT NULL,
  `id_ticket_header` int(11) NOT NULL,
  `id_teknisi` int(11) NOT NULL,
  `created_ticket_responded` datetime NOT NULL,
  `updated_ticket_responded` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ticket_sn`
--

CREATE TABLE `ticket_sn` (
  `id_ticket_sn` int(11) NOT NULL,
  `id_ticket_header` int(11) NOT NULL,
  `dvr_ticket_sn` varchar(125) NOT NULL,
  `camera_ticket_sn` varchar(125) NOT NULL,
  `hdd_ticket_sn` varchar(125) NOT NULL,
  `created_ticket_sn` datetime NOT NULL,
  `updated_ticket_sn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ticket_started`
--

CREATE TABLE `ticket_started` (
  `id_ticket_started` int(11) NOT NULL,
  `id_ticket_header` int(11) NOT NULL,
  `id_teknisi` int(11) NOT NULL,
  `foto_ticket_started` varchar(125) NOT NULL,
  `created_ticket_started` datetime NOT NULL,
  `updated_ticket_started` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `token_cabang`
--

CREATE TABLE `token_cabang` (
  `token_cabang` varchar(255) NOT NULL,
  `id_cabang` int(11) NOT NULL,
  `created_token_cabang` datetime NOT NULL,
  `updated_token_cabang` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `token_client`
--

CREATE TABLE `token_client` (
  `token_client` varchar(255) NOT NULL,
  `id_client` int(11) NOT NULL,
  `created_token_client` datetime NOT NULL,
  `updated_token_client` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `token_koordinator`
--

CREATE TABLE `token_koordinator` (
  `token_koordinator` varchar(255) NOT NULL,
  `id_koordinator` int(11) NOT NULL,
  `created_token_koordinator` datetime NOT NULL,
  `updated_token_koordinator` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `token_manager_area`
--

CREATE TABLE `token_manager_area` (
  `token_manager_area` varchar(255) NOT NULL,
  `id_manager_area` int(11) NOT NULL,
  `created_token_manager_area` datetime NOT NULL,
  `updated_token_manager_area` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `token_pengelola`
--

CREATE TABLE `token_pengelola` (
  `token_pengelola` varchar(255) NOT NULL,
  `id_pengelola` int(11) NOT NULL,
  `created_token_pengelola` datetime NOT NULL,
  `updated_token_pengelola` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `token_teknisi`
--

CREATE TABLE `token_teknisi` (
  `token_teknisi` int(11) NOT NULL,
  `id_teknisi` int(11) NOT NULL,
  `created_token_teknisi` datetime NOT NULL,
  `updated_token_teknisi` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `email_user` varchar(125) NOT NULL,
  `password_user` varchar(125) NOT NULL,
  `level_user` tinyint(1) NOT NULL,
  `is_active_user` tinyint(1) NOT NULL,
  `created_user` datetime NOT NULL,
  `updated_user` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`email_user`, `password_user`, `level_user`, `is_active_user`, `created_user`, `updated_user`) VALUES
('admin@gmail.com', '$2y$10$mMM09d3HSPWaHSAlbkfWeeSNm5KckQrljZEeNs.WxswtoQ1Sf4.SC', 1, 1, '2020-11-13 17:32:10', '0000-00-00 00:00:00'),
('jkt@bni.co.id', '$2y$10$BkZ2Bvf2Yjacjmbfrq6llO7h2y/fzfxqrfB0nv43jwjCx2AVisGs2', 5, 1, '2020-11-13 14:22:51', '0000-00-00 00:00:00'),
('koordinator1@gmail.com', '$2y$10$UQf5zeDaFo22KoSLMOwn4.rur2X/m9VYIMlZ7/A6C3o601OfQ5dxC', 2, 1, '2020-11-28 10:26:43', '0000-00-00 00:00:00'),
('koordinator2@gmail.com', '$2y$10$YJUMVde3Ef3K2JBfbexscek7nGvv0xwcHrsdc016T2FgdoWAsIPOG', 2, 1, '2020-11-28 22:55:01', '0000-00-00 00:00:00'),
('koordinator@gmail.com', '$2y$10$w6kwjAMpAmme4dIxuCf15ucEyZWnrYBb1T66.WeI2Cwo03e9QakcO', 2, 1, '2020-11-28 10:23:45', '0000-00-00 00:00:00'),
('manager.wjk@bni.co.id', '$2y$10$VPfVrqAnEi6lONg02qD1TeBMRJFcisDgw7DYG3X2qvvUE4qthk0Kq', 4, 1, '2020-11-12 23:25:57', '0000-00-00 00:00:00'),
('ssihmn@ssi.co.id', '$2y$10$/AMmQwFsIY6N4Y5.G3jII.Ho4g22mOmBzwVsV.fmOTlQslWB.XWXe', 6, 1, '2020-11-13 14:23:20', '0000-00-00 00:00:00'),
('teknisi1@ptmdm.co.id', '$2y$10$vI79aSuGf1UShEjN0PNuqOiowFuM/Rb0o4KtwjwdHKcurox5pTKbS', 7, 1, '2020-11-13 14:23:50', '0000-00-00 00:00:00'),
('wjk@bni.co.id', '$2y$10$f2Csy4OqopkuNhE3SLmM3OoMS5LbJQKZxlBaV5g5nvJ9L.yCMqj16', 3, 1, '2020-11-12 23:25:06', '0000-00-00 00:00:00'),
('yusufh4d1@gmail.com', '$2y$10$3issqasjcSjY1XDTTS4DZec.KF89lJTCP.tqPuhYkQgXGu0j.5h2K', 1, 1, '2020-11-12 23:24:03', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `wilayah`
--

CREATE TABLE `wilayah` (
  `kd_wilayah` varchar(125) NOT NULL,
  `nama_wilayah` varchar(125) NOT NULL,
  `created_wilayah` datetime NOT NULL,
  `updated_wilayah` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `wilayah`
--

INSERT INTO `wilayah` (`kd_wilayah`, `nama_wilayah`, `created_wilayah`, `updated_wilayah`) VALUES
('WBN', 'BNI WILAYAH BANDUNG', '2020-11-12 23:20:41', '0000-00-00 00:00:00'),
('WJB', 'BNI WILAYAH JAKARTA BSD', '2020-11-12 23:20:28', '0000-00-00 00:00:00'),
('WJK', 'BNI WILAYAH JAKARTA KOTA', '2020-11-12 23:19:04', '2020-11-12 23:19:24'),
('WJS', 'BNI WILAYAH JAKARTA SENAYAN', '2020-11-12 23:20:03', '0000-00-00 00:00:00'),
('WJY', 'BNI WILAYAH JAKARTA KEMAYORAN', '2020-11-12 23:19:44', '0000-00-00 00:00:00'),
('WSM', 'BNI WILAYAH SEMARANG', '2020-11-12 23:20:55', '0000-00-00 00:00:00'),
('WYK', 'BNI WILAYAH YOGYAKARTA', '2020-11-12 23:21:08', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`),
  ADD KEY `fkadmin1` (`email_user`);

--
-- Indexes for table `atm`
--
ALTER TABLE `atm`
  ADD PRIMARY KEY (`id_atm`),
  ADD KEY `fkatm1` (`kd_atm_cabang`),
  ADD KEY `fkatm2` (`kd_atm_pengelola`);

--
-- Indexes for table `atm_cabang`
--
ALTER TABLE `atm_cabang`
  ADD PRIMARY KEY (`kd_atm_cabang`),
  ADD KEY `fkatm_cabang1` (`kd_wilayah`);

--
-- Indexes for table `atm_pengelola`
--
ALTER TABLE `atm_pengelola`
  ADD PRIMARY KEY (`kd_atm_pengelola`),
  ADD KEY `fkatm_pengelola1` (`kd_wilayah`);

--
-- Indexes for table `cabang`
--
ALTER TABLE `cabang`
  ADD PRIMARY KEY (`id_cabang`),
  ADD KEY `fkcabang1` (`email_user`),
  ADD KEY `fkcabang2` (`kd_atm_cabang`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id_client`),
  ADD KEY `fkclient1` (`email_user`),
  ADD KEY `fkclient2` (`kd_wilayah`);

--
-- Indexes for table `koordinator`
--
ALTER TABLE `koordinator`
  ADD PRIMARY KEY (`id_koordinator`),
  ADD KEY `fkkoordinator1` (`email_user`);

--
-- Indexes for table `koordinator_wilayah`
--
ALTER TABLE `koordinator_wilayah`
  ADD PRIMARY KEY (`id_koordinator_wilayah`),
  ADD KEY `fkkoordinator_wilayah1` (`id_koordinator`),
  ADD KEY `fkkoordinator_wilayah2` (`kd_wilayah`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id_log`);

--
-- Indexes for table `manager_area`
--
ALTER TABLE `manager_area`
  ADD PRIMARY KEY (`id_manager_area`),
  ADD KEY `fkmanager_are1` (`email_user`),
  ADD KEY `fkmanager_area2` (`kd_wilayah`);

--
-- Indexes for table `part`
--
ALTER TABLE `part`
  ADD PRIMARY KEY (`kd_part`);

--
-- Indexes for table `part_harga`
--
ALTER TABLE `part_harga`
  ADD PRIMARY KEY (`id_part_harga`),
  ADD KEY `fkpart_harga1` (`kd_part`),
  ADD KEY `fkpart_harga2` (`kd_wilayah`);

--
-- Indexes for table `pengelola`
--
ALTER TABLE `pengelola`
  ADD PRIMARY KEY (`id_pengelola`),
  ADD KEY `fkpengelola1` (`email_user`),
  ADD KEY `fkpengelola2` (`kd_atm_pengelola`);

--
-- Indexes for table `snapshoot_cm`
--
ALTER TABLE `snapshoot_cm`
  ADD PRIMARY KEY (`id_snapshoot_cm`),
  ADD KEY `fksnapshoot_cm` (`id_ticket_header`);

--
-- Indexes for table `snapshoot_pm`
--
ALTER TABLE `snapshoot_pm`
  ADD PRIMARY KEY (`id_snapshoot_pm`),
  ADD KEY `fksnapshoot_pm1` (`id_ticket_header`);

--
-- Indexes for table `teknisi`
--
ALTER TABLE `teknisi`
  ADD PRIMARY KEY (`id_teknisi`),
  ADD KEY `fkteknisi1` (`email_user`);

--
-- Indexes for table `ticket_cancel`
--
ALTER TABLE `ticket_cancel`
  ADD PRIMARY KEY (`id_ticket_cancel`),
  ADD KEY `fkticket_cancel2` (`id_pengelola`),
  ADD KEY `fkticket_cancel1` (`id_ticket_header`) USING BTREE;

--
-- Indexes for table `ticket_detail`
--
ALTER TABLE `ticket_detail`
  ADD PRIMARY KEY (`id_ticket_detail`),
  ADD KEY `fkticket_detail1` (`id_ticket_header`),
  ADD KEY `fkticket_detail2` (`id_koordinator`),
  ADD KEY `fkticket_detail3` (`id_teknisi`);

--
-- Indexes for table `ticket_header`
--
ALTER TABLE `ticket_header`
  ADD PRIMARY KEY (`id_ticket_header`),
  ADD KEY `fkticket_header1` (`id_atm`),
  ADD KEY `fkticket_header2` (`id_pengelola`);

--
-- Indexes for table `ticket_part`
--
ALTER TABLE `ticket_part`
  ADD PRIMARY KEY (`id_ticket_part`),
  ADD KEY `fkticket_part1` (`id_ticket_header`),
  ADD KEY `fkticket_part` (`kd_part`);

--
-- Indexes for table `ticket_pending`
--
ALTER TABLE `ticket_pending`
  ADD PRIMARY KEY (`id_ticket_pending`),
  ADD KEY `fkticket_pending1` (`id_ticket_header`),
  ADD KEY `fkticket_pending2` (`id_teknisi`);

--
-- Indexes for table `ticket_pinalty`
--
ALTER TABLE `ticket_pinalty`
  ADD PRIMARY KEY (`id_ticket_pinalty`),
  ADD KEY `fkticket_pinalty1` (`id_ticket_header`);

--
-- Indexes for table `ticket_responded`
--
ALTER TABLE `ticket_responded`
  ADD PRIMARY KEY (`id_ticket_responded`),
  ADD KEY `fkticket_responded1` (`id_ticket_header`),
  ADD KEY `fkticket_responded2` (`id_teknisi`);

--
-- Indexes for table `ticket_sn`
--
ALTER TABLE `ticket_sn`
  ADD PRIMARY KEY (`id_ticket_sn`),
  ADD KEY `fkticket_sn1` (`id_ticket_header`) USING BTREE;

--
-- Indexes for table `ticket_started`
--
ALTER TABLE `ticket_started`
  ADD PRIMARY KEY (`id_ticket_started`),
  ADD KEY `fkticket_started1` (`id_ticket_header`),
  ADD KEY `fkticket_started2` (`id_teknisi`);

--
-- Indexes for table `token_cabang`
--
ALTER TABLE `token_cabang`
  ADD PRIMARY KEY (`token_cabang`),
  ADD KEY `fktoken_cabang` (`id_cabang`);

--
-- Indexes for table `token_client`
--
ALTER TABLE `token_client`
  ADD PRIMARY KEY (`token_client`),
  ADD KEY `fktoken_client1` (`id_client`) USING BTREE;

--
-- Indexes for table `token_koordinator`
--
ALTER TABLE `token_koordinator`
  ADD PRIMARY KEY (`token_koordinator`),
  ADD KEY `fktoken_koordinator1` (`id_koordinator`);

--
-- Indexes for table `token_manager_area`
--
ALTER TABLE `token_manager_area`
  ADD PRIMARY KEY (`token_manager_area`),
  ADD KEY `fktoken_manager_area1` (`id_manager_area`);

--
-- Indexes for table `token_pengelola`
--
ALTER TABLE `token_pengelola`
  ADD PRIMARY KEY (`token_pengelola`),
  ADD KEY `fktoken_pegelola` (`id_pengelola`);

--
-- Indexes for table `token_teknisi`
--
ALTER TABLE `token_teknisi`
  ADD PRIMARY KEY (`token_teknisi`),
  ADD KEY `fktoken_teknisi1` (`id_teknisi`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`email_user`);

--
-- Indexes for table `wilayah`
--
ALTER TABLE `wilayah`
  ADD PRIMARY KEY (`kd_wilayah`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `cabang`
--
ALTER TABLE `cabang`
  MODIFY `id_cabang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `id_client` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `koordinator`
--
ALTER TABLE `koordinator`
  MODIFY `id_koordinator` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `koordinator_wilayah`
--
ALTER TABLE `koordinator_wilayah`
  MODIFY `id_koordinator_wilayah` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `id_log` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=227;

--
-- AUTO_INCREMENT for table `manager_area`
--
ALTER TABLE `manager_area`
  MODIFY `id_manager_area` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `part_harga`
--
ALTER TABLE `part_harga`
  MODIFY `id_part_harga` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pengelola`
--
ALTER TABLE `pengelola`
  MODIFY `id_pengelola` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `snapshoot_cm`
--
ALTER TABLE `snapshoot_cm`
  MODIFY `id_snapshoot_cm` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `snapshoot_pm`
--
ALTER TABLE `snapshoot_pm`
  MODIFY `id_snapshoot_pm` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `teknisi`
--
ALTER TABLE `teknisi`
  MODIFY `id_teknisi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ticket_cancel`
--
ALTER TABLE `ticket_cancel`
  MODIFY `id_ticket_cancel` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ticket_detail`
--
ALTER TABLE `ticket_detail`
  MODIFY `id_ticket_detail` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ticket_header`
--
ALTER TABLE `ticket_header`
  MODIFY `id_ticket_header` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ticket_part`
--
ALTER TABLE `ticket_part`
  MODIFY `id_ticket_part` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ticket_pending`
--
ALTER TABLE `ticket_pending`
  MODIFY `id_ticket_pending` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ticket_pinalty`
--
ALTER TABLE `ticket_pinalty`
  MODIFY `id_ticket_pinalty` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ticket_responded`
--
ALTER TABLE `ticket_responded`
  MODIFY `id_ticket_responded` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ticket_sn`
--
ALTER TABLE `ticket_sn`
  MODIFY `id_ticket_sn` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ticket_started`
--
ALTER TABLE `ticket_started`
  MODIFY `id_ticket_started` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin`
--
ALTER TABLE `admin`
  ADD CONSTRAINT `fkadmin1` FOREIGN KEY (`email_user`) REFERENCES `user` (`email_user`);

--
-- Constraints for table `atm`
--
ALTER TABLE `atm`
  ADD CONSTRAINT `fkatm1` FOREIGN KEY (`kd_atm_cabang`) REFERENCES `atm_cabang` (`kd_atm_cabang`),
  ADD CONSTRAINT `fkatm2` FOREIGN KEY (`kd_atm_pengelola`) REFERENCES `atm_pengelola` (`kd_atm_pengelola`);

--
-- Constraints for table `atm_cabang`
--
ALTER TABLE `atm_cabang`
  ADD CONSTRAINT `fkatm_cabang1` FOREIGN KEY (`kd_wilayah`) REFERENCES `wilayah` (`kd_wilayah`);

--
-- Constraints for table `atm_pengelola`
--
ALTER TABLE `atm_pengelola`
  ADD CONSTRAINT `fkatm_pengelola1` FOREIGN KEY (`kd_wilayah`) REFERENCES `wilayah` (`kd_wilayah`);

--
-- Constraints for table `cabang`
--
ALTER TABLE `cabang`
  ADD CONSTRAINT `fkcabang1` FOREIGN KEY (`email_user`) REFERENCES `user` (`email_user`),
  ADD CONSTRAINT `fkcabang2` FOREIGN KEY (`kd_atm_cabang`) REFERENCES `atm_cabang` (`kd_atm_cabang`);

--
-- Constraints for table `client`
--
ALTER TABLE `client`
  ADD CONSTRAINT `fkclient1` FOREIGN KEY (`email_user`) REFERENCES `user` (`email_user`),
  ADD CONSTRAINT `fkclient2` FOREIGN KEY (`kd_wilayah`) REFERENCES `wilayah` (`kd_wilayah`);

--
-- Constraints for table `koordinator`
--
ALTER TABLE `koordinator`
  ADD CONSTRAINT `fkkoordinator1` FOREIGN KEY (`email_user`) REFERENCES `user` (`email_user`);

--
-- Constraints for table `koordinator_wilayah`
--
ALTER TABLE `koordinator_wilayah`
  ADD CONSTRAINT `fkkoordinator_wilayah1` FOREIGN KEY (`id_koordinator`) REFERENCES `koordinator` (`id_koordinator`),
  ADD CONSTRAINT `fkkoordinator_wilayah2` FOREIGN KEY (`kd_wilayah`) REFERENCES `wilayah` (`kd_wilayah`);

--
-- Constraints for table `manager_area`
--
ALTER TABLE `manager_area`
  ADD CONSTRAINT `fkmanager_are1` FOREIGN KEY (`email_user`) REFERENCES `user` (`email_user`),
  ADD CONSTRAINT `fkmanager_area2` FOREIGN KEY (`kd_wilayah`) REFERENCES `wilayah` (`kd_wilayah`);

--
-- Constraints for table `part_harga`
--
ALTER TABLE `part_harga`
  ADD CONSTRAINT `fkpart_harga1` FOREIGN KEY (`kd_part`) REFERENCES `part` (`kd_part`),
  ADD CONSTRAINT `fkpart_harga2` FOREIGN KEY (`kd_wilayah`) REFERENCES `wilayah` (`kd_wilayah`);

--
-- Constraints for table `pengelola`
--
ALTER TABLE `pengelola`
  ADD CONSTRAINT `fkpengelola1` FOREIGN KEY (`email_user`) REFERENCES `user` (`email_user`),
  ADD CONSTRAINT `fkpengelola2` FOREIGN KEY (`kd_atm_pengelola`) REFERENCES `atm_pengelola` (`kd_atm_pengelola`);

--
-- Constraints for table `snapshoot_cm`
--
ALTER TABLE `snapshoot_cm`
  ADD CONSTRAINT `fksnapshoot_cm` FOREIGN KEY (`id_ticket_header`) REFERENCES `ticket_header` (`id_ticket_header`);

--
-- Constraints for table `snapshoot_pm`
--
ALTER TABLE `snapshoot_pm`
  ADD CONSTRAINT `fksnapshoot_pm1` FOREIGN KEY (`id_ticket_header`) REFERENCES `ticket_header` (`id_ticket_header`);

--
-- Constraints for table `teknisi`
--
ALTER TABLE `teknisi`
  ADD CONSTRAINT `fkteknisi1` FOREIGN KEY (`email_user`) REFERENCES `user` (`email_user`);

--
-- Constraints for table `ticket_cancel`
--
ALTER TABLE `ticket_cancel`
  ADD CONSTRAINT `fkticket_cancel` FOREIGN KEY (`id_ticket_header`) REFERENCES `ticket_header` (`id_ticket_header`),
  ADD CONSTRAINT `fkticket_cancel2` FOREIGN KEY (`id_pengelola`) REFERENCES `pengelola` (`id_pengelola`);

--
-- Constraints for table `ticket_detail`
--
ALTER TABLE `ticket_detail`
  ADD CONSTRAINT `fkticket_detail1` FOREIGN KEY (`id_ticket_header`) REFERENCES `ticket_header` (`id_ticket_header`),
  ADD CONSTRAINT `fkticket_detail2` FOREIGN KEY (`id_koordinator`) REFERENCES `koordinator` (`id_koordinator`),
  ADD CONSTRAINT `fkticket_detail3` FOREIGN KEY (`id_teknisi`) REFERENCES `teknisi` (`id_teknisi`);

--
-- Constraints for table `ticket_header`
--
ALTER TABLE `ticket_header`
  ADD CONSTRAINT `fkticket_header1` FOREIGN KEY (`id_atm`) REFERENCES `atm` (`id_atm`),
  ADD CONSTRAINT `fkticket_header2` FOREIGN KEY (`id_pengelola`) REFERENCES `pengelola` (`id_pengelola`);

--
-- Constraints for table `ticket_part`
--
ALTER TABLE `ticket_part`
  ADD CONSTRAINT `fkticket_part` FOREIGN KEY (`kd_part`) REFERENCES `part` (`kd_part`),
  ADD CONSTRAINT `fkticket_part1` FOREIGN KEY (`id_ticket_header`) REFERENCES `ticket_header` (`id_ticket_header`);

--
-- Constraints for table `ticket_pending`
--
ALTER TABLE `ticket_pending`
  ADD CONSTRAINT `fkticket_pending1` FOREIGN KEY (`id_ticket_header`) REFERENCES `ticket_header` (`id_ticket_header`),
  ADD CONSTRAINT `fkticket_pending2` FOREIGN KEY (`id_teknisi`) REFERENCES `teknisi` (`id_teknisi`);

--
-- Constraints for table `ticket_pinalty`
--
ALTER TABLE `ticket_pinalty`
  ADD CONSTRAINT `fkticket_pinalty1` FOREIGN KEY (`id_ticket_header`) REFERENCES `ticket_header` (`id_ticket_header`);

--
-- Constraints for table `ticket_responded`
--
ALTER TABLE `ticket_responded`
  ADD CONSTRAINT `fkticket_responded1` FOREIGN KEY (`id_ticket_header`) REFERENCES `ticket_header` (`id_ticket_header`),
  ADD CONSTRAINT `fkticket_responded2` FOREIGN KEY (`id_teknisi`) REFERENCES `teknisi` (`id_teknisi`);

--
-- Constraints for table `ticket_sn`
--
ALTER TABLE `ticket_sn`
  ADD CONSTRAINT `fkticket_sn` FOREIGN KEY (`id_ticket_header`) REFERENCES `ticket_header` (`id_ticket_header`);

--
-- Constraints for table `ticket_started`
--
ALTER TABLE `ticket_started`
  ADD CONSTRAINT `fkticket_started` FOREIGN KEY (`id_ticket_header`) REFERENCES `ticket_header` (`id_ticket_header`),
  ADD CONSTRAINT `fkticket_started1` FOREIGN KEY (`id_ticket_header`) REFERENCES `ticket_header` (`id_ticket_header`),
  ADD CONSTRAINT `fkticket_started2` FOREIGN KEY (`id_teknisi`) REFERENCES `teknisi` (`id_teknisi`);

--
-- Constraints for table `token_cabang`
--
ALTER TABLE `token_cabang`
  ADD CONSTRAINT `fktoken_cabang` FOREIGN KEY (`id_cabang`) REFERENCES `cabang` (`id_cabang`);

--
-- Constraints for table `token_client`
--
ALTER TABLE `token_client`
  ADD CONSTRAINT `fktokem_client1` FOREIGN KEY (`id_client`) REFERENCES `client` (`id_client`);

--
-- Constraints for table `token_koordinator`
--
ALTER TABLE `token_koordinator`
  ADD CONSTRAINT `fktoken_koordinator` FOREIGN KEY (`id_koordinator`) REFERENCES `koordinator` (`id_koordinator`),
  ADD CONSTRAINT `fktoken_koordinator1` FOREIGN KEY (`id_koordinator`) REFERENCES `koordinator` (`id_koordinator`);

--
-- Constraints for table `token_manager_area`
--
ALTER TABLE `token_manager_area`
  ADD CONSTRAINT `fktoken_manager_area1` FOREIGN KEY (`id_manager_area`) REFERENCES `manager_area` (`id_manager_area`);

--
-- Constraints for table `token_pengelola`
--
ALTER TABLE `token_pengelola`
  ADD CONSTRAINT `fktoken_pegelola` FOREIGN KEY (`id_pengelola`) REFERENCES `pengelola` (`id_pengelola`);

--
-- Constraints for table `token_teknisi`
--
ALTER TABLE `token_teknisi`
  ADD CONSTRAINT `fktoken_teknisi1` FOREIGN KEY (`id_teknisi`) REFERENCES `teknisi` (`id_teknisi`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
